#!/bin/bash
xhost +
sudo docker run -it -d -P --name t1-ui \
--cap-add NET_ADMIN --cap-add NET_RAW --device=/dev/net/tun \
--mount type=bind,source=/home/cyanide/workbench/firmdep_eval_new,target=/share \
--mount type=bind,source=/home/cyanide/workbench/panda_0606,target=/panda \
-e DISPLAY=unix$DISPLAY \
-e GDK_SCALE \
-e GDK_DPI_SCALE \
-v /tmp/.X11-unix:/tmp/.X11-unix \
panda_eval \
/bin/bash
