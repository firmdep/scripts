#!/usr/bin/env python3
import sqlite3
from io import StringIO
from os import path
from base_class import *
from api_identity import hex_to_int, pid_from_stackid, tid_from_stackid
from colorama import Fore, Style

def progress(msg):
    print(Fore.CYAN + '[+] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_warn(msg):
    print(Fore.YELLOW + '[!] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_err(msg):
    print(Fore.RED + '[-] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)


class TaskSummary(object):
    def __init__(self, stackid, start_pos, end_pos, parent):
        self.stackid = stackid
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.parent = parent
    def get_self_execution(self):
        return (self.start_pos, self.end_pos)
    def get_whole_execution(self):
        if (self.parent == None):
            return self.get_self_execution()
        elif (self.parent.stackid == self.stackid):
            return self.get_self_execution()
        else:
            return (min(self.start_pos, self.parent.get_whole_execution()[0]), self.end_pos)

class Database(object):
    def __init__(self, workdir, target, db_source = None):
        # self.conn = self.init_db(workdir, target)
        self.save_file = path.join(workdir, target) + '.db'
        self.conn = self.init_db(db_source)

    def init_db(self, db_source):
        conn = sqlite3.connect(':memory:',check_same_thread=False)
        if (db_source != None):
            progress("Loading existing database...")
            # source = sqlite3.connect(db_source)
            # str_buffer = StringIO()
            # for line in source.iterdump():
            #     str_buffer.write('%s\n' % line)
            # source.close()
            conn = sqlite3.connect(db_source, check_same_thread = False)
            # cursor = conn.cursor()
            # cursor.executescript(str_buffer.getvalue())
        else:
            cursor = conn.cursor()
            # build event table
            cursor.execute('''CREATE TABLE \"events\" (
            \"event_id\"	INTEGER NOT NULL UNIQUE,
            \"pos\"	INTEGER NOT NULL,
            \"type\"	TEXT NOT NULL,
            \"stackid\"	TEXT NOT NULL,
            \"desc"	TEXT,
            PRIMARY KEY(\"event_id\")
            );
            ''')
                # build trace table
            cursor.execute('''CREATE TABLE \"states\" (
                \"event_id\"	INTEGER NOT NULL UNIQUE,
                \"pos\"	UNSIGNED BIG INT NOT NULL,
                \"is_after\"	INTEGER NOT NULL,
                \"stackid\"	TEXT NOT NULL,
                \"size\"	INTEGER NOT NULL,
                \"pc\"	INTEGER NOT NULL,
                \"sp\"	INTEGER NOT NULL,
                \"lr\"	INTEGER NOT NULL,
                \"r0\"	INTEGER NOT NULL,
                \"r1\"	INTEGER NOT NULL,
                \"r2\"	INTEGER NOT NULL,
                \"r3\"	INTEGER NOT NULL,
                \"r4\"	INTEGER NOT NULL,
                \"r5\"	INTEGER NOT NULL,
                \"r6\"	INTEGER NOT NULL,
                \"r7\"	INTEGER NOT NULL,
                \"r8\"	INTEGER NOT NULL,
                \"r9\"	INTEGER NOT NULL,
                \"r10\"	INTEGER NOT NULL,
                \"r11\"	INTEGER NOT NULL,
                \"r12\"	INTEGER NOT NULL,
                \"r13\"	INTEGER NOT NULL,
                \"r14\"	INTEGER NOT NULL,
                \"r15\"	INTEGER NOT NULL,
                \"exitcode\" INTEGER,
                PRIMARY KEY(\"event_id\",\"pos\")
            );
            ''')
                # build file descriptor operation table
            cursor.execute('''CREATE TABLE \"fd_ops\" (
                \"event_id\"	INTEGER NOT NULL UNIQUE,
                \"pos\"	INTEGER NOT NULL,
                \"pid\"	INTEGER NOT NULL,
                \"op\"	TEXT NOT NULL,
                \"src\"	TEXT,
                \"dest\" INTEGER,
                \"ret\"	INTEGER,
                \"desc\"	TEXT,
                PRIMARY KEY(\"event_id\")
            )
            ''')
                # build errno record table
            cursor.execute('''CREATE TABLE \"errnos\"(
                \"event_id\"    INTEGER NOT NULL UNIQUE,
                \"pos\" INTEGER NOT NULL,
                \"stackid\" TEXT NOT NULL,
                \"errno\" INTEGER NOT NULL,
                PRIMARY KEY(\"event_id\",\"pos\")
            )
            ''')
            cursor.execute('''CREATE TABLE \"tasks\"(
                \"event_id\" TEXT NOT NULL UNIQUE,
                \"pos\" INTEGER NOT NULL,
                \"stackid\" TEXT NOT NULL,
                \"is_main_thread\" INTEGER NOT NULL,
                \"parent\" INTEGER NOT NULL,
                PRIMARY KEY(\"event_id\")
            )
            ''')

        return conn

    def new_event_id(self):
        cursor = self.conn.cursor()
        cursor.execute('select max(event_id) from events')
        result = cursor.fetchall()
        if (len(result) == 0):
            return 1
        elif (result[0][0] == None):
            return 1
        else:
            latest = result[0][0]
            return latest + 1

    def get_fds(self, rr_instr, stackid):
        cursor = self.conn.cursor()
        pid = pid_from_stackid(stackid)
        cursor.execute('select * from fd_ops where pid=? and pos<=? order by pos asc;',(pid, rr_instr))
        result = cursor.fetchall()
        fds = dict()
        # format of r:(eventid, pos(rr_instr), stackid, operation, src, dest, result, desc)
        for r in result:
            op = r[3]
            src = r[4]
            dest = r[5]
            ret = r[6]
            if (ret != 0xffffffff):
                if (op == 'open'):
                    fds[dest] = src
                elif (op == 'openat'):
                    dirfd = src[0]
                    file = src[1]
                    if dirfd in fds:
                        fds[dest] = fds[dirfd] + '/' + file
                    else:
                        fds[dest] = "<unknown>/" + file
                elif (op == 'creat'):
                    fds[dest] = src
                elif (op == 'dup'):
                    fds[dest] = fds[int(src)]
                elif (op == 'dup2'):
                    fds[dest] = fds[int(src)]
                elif (op == 'dup3'):
                    fds[dest] = fds[int(src)]
                elif (op == 'close'):
                    if int(src) in fds:
                        fds.pop(int(src))
                elif (op == 'socket'):
                    fds[dest] = src
                elif (op == 'bind'):
                    fds[dest] = src
                elif (op == 'connect'):
                    fds[dest] = src
        return fds

    def get_fd(self, rr_instr, stackid, fd):
        fds = self.get_fds(rr_instr, stackid)
        return fds[fd]

    def get_last_fd_op(self, rr_instr, stackid, include_fail = False):
        pid = pid_from_stackid(stackid)
        cursor = self.conn.cursor()
        if (include_fail == True):
            cursor.execute('select * from fd_ops where pid=? and pos<=? order by pos desc limit 1;',(pid, rr_instr))
        else:
            cursor.execute('select * from fd_ops where pid=? and pos<=? and ret>=0 order by pos desc limit 1;',(pid, rr_instr))
        result = cursor.fetchall()[0]
        return {'op':result[3],'src':result[4],'dest':result[5],'ret':result[6]}

    def update_fd(self, rr_instr, stackid, op, src, dest, ret, desc):
        cursor = self.conn.cursor()
        event_id = self.new_event_id()
        ret_int = hex_to_int(ret)
        pid = pid_from_stackid(stackid)
        cursor.execute('insert into events values (?,?,?,?,?)',(event_id, rr_instr, 'fd', stackid, ''))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')
        cursor.execute('insert into fd_ops values (?,?,?,?,?,?,?,?)',(event_id, rr_instr, pid, op, src, dest, ret_int, desc))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')

    def commit_errno(self, rr_instr, stackid, errno):
        cursor = self.conn.cursor()
        event_id = self.new_event_id()
        cursor.execute('insert into events values (?,?,?,?,?)',(event_id, rr_instr, 'errno', stackid, ''))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')
        cursor.execute('insert into errnos values (?,?,?,?)',(event_id, rr_instr, stackid, errno))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')

    def get_last_errno(self, pos, stackid):
        cursor = self.conn.cursor()
        cursor.execute('select * from errnos where stackid=? and pos<=? order by pos desc limit 1;',(stackid, pos))
        result = cursor.fetchall()
        if len(result) > 0 :
            return result[0]
        else:
            return None

    def update_task(self, rr_instr, stackid, parent):
        cursor = self.conn.cursor()
        event_id = self.new_event_id()
        pid = pid_from_stackid(stackid)
        tid = pid_from_stackid(stackid)
        is_main_thread = 1 if (pid == tid) else 0
        cursor.execute('insert into events values (?,?,?,?,?)',(event_id, rr_instr, 'new_task', stackid, ''))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')
        cursor.execute('insert into tasks values (?,?,?,?,?)',(event_id, rr_instr, stackid, is_main_thread, parent))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')

    def commit_state_raw(self, rr_instr, is_after, stackid, regs, size, exitcode):
        cursor = self.conn.cursor()
        event_id = self.new_event_id()
        cursor.execute('insert into events values (?,?,?,?,?)',(event_id, rr_instr, 'state', stackid, ''))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')
        try:
            cursor.execute('insert into states values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            (event_id, rr_instr, is_after, stackid, size,
                regs[15], regs[13], regs[14],
                regs[0],regs[1],regs[2],regs[3],regs[4],regs[5],regs[6],regs[7],regs[8],regs[9],regs[10],regs[11],regs[12],regs[13],regs[14],regs[15],exitcode))
        except:
            print("rr_instr:",repr(rr_instr)) 
            print("stackid:", repr(stackid))
            print("size:", repr(size))
            for i in range(16):
                print("reg[%d] = %s" %(i, repr(regs[i])))
        if (cursor.rowcount == 0):
            raise IOError('update db failed')
        # self.conn.commit()

    def commit_state(self, state):
        if (state.type == 'before'):
            self.commit_state_raw(state.pos, 0, format(state.stackid), state.regs, state.size, 0)
        else:
            self.commit_state_raw(state.pos, 1, format(state.stackid), state.regs, state.size, state.exitcode)

    def state_from_db(self, rr_instr):
    	cursor = self.conn.cursor()
    	cursor.execute('select * from states where pos<=? order by desc limit 1;', (rr_instr))
    	result = cursor.fetchall()
    	if (len(result) == 0):
    		raise IOError('fetch from db failed')
    	r = result[0]
    	state = State(('before' if r[2]==0 else 'after'), r[1], r[2], r[5], r[8:-1], r[4], r[-1])
    	return state

    def save(self):
        progress("Saving trace to database file...")
        if path.exists(self.save_file):
            os.remove(self.save_file)
        conn_file = sqlite3.connect(self.save_file)
        str_buffer = StringIO()
        for line in self.conn.iterdump():
            str_buffer.write('%s\n' % line)
        self.conn.close()
        cursor = conn_file.cursor()
        cursor.executescript(str_buffer.getvalue())
        conn_file.close()

    def summary(self, pp=False):
        summaries = dict()
        cursor = self.conn.cursor()
        cursor.execute('select stackid, parent from tasks')
        tasks = cursor.fetchall()
        for t in tasks:
            cmd = 'select min(pos),max(pos) from states where stackid=%s' %repr(t[0])
            cursor.execute(cmd)
            start,end = cursor.fetchall()[0]
            if (t[0] == t[1]):
                new_summary = TaskSummary(t[0], start, end, None)
            else:
                new_summary = TaskSummary(t[0], start, end, summaries[t[1]])
            summaries[t[0]] = (new_summary)
        if(pp):
            for s in summaries.values():
                overall_start, overall_end = s.get_whole_execution()
                print("%s\t%d\t%d\t%d\t%d" %(s.stackid, s.start_pos, s.end_pos, overall_start, overall_end))
        return summaries

    def fetch_events_by_stackid(self, stackid, type='all'):
        summaries = self.summary()
        if stackid not in summaries.keys():
            progress_err("invalid stackid")
            return None
        else:
            cursor = self.conn.cursor()
            overall_start, overall_end = summaries[stackid].get_whole_execution()
            if (type == 'all'):
                cursor.execute('select event_id, stackid, type from events where (stackid=? or stackid in (SELECT parent from tasks where stackid=?)) and pos>=? and pos<=? order by event_id asc',(stackid, stackid, overall_start, overall_end))
            elif (type == 'state'):
                cursor.execute('select * from states where event_id in (select event_id from events where (stackid=? or stackid in (SELECT parent from tasks where stackid=?)) and pos>=? and pos<=? and type=?) order by event_id asc',(stackid, stackid, overall_start, overall_end, 'state'))                
            result = cursor.fetchall()
            return result


