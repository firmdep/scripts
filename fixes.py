#!/usr/bin/env python3
from api_identity import errno_dict
from colorama import Fore, Style
import os

def progress(msg):
    print(Fore.CYAN + '[+] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_warn(msg):
    print(Fore.YELLOW + '[!] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_err(msg):
    print(Fore.RED + '[-] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)
'''
this dict comes from an old version of this framework. may be replaced by some database in the future.
'''
blame_dict = {
    # 'funcname' : { ERRNO: {arg_index:type, arg_index:type....}, ...}
    # from libc 
    # int access(const char *path, int amode);
    'access' : {'EACCES':{1:'concrete'},
                'ELOOP':{0:'string'},
                'ENAMETOOLONG':{0:'string'},
                'ENOENT':{0:'string'},
                'ENOTDIR':{0:'string'},
                'EROFS':{}
                },
    'calloc' : {'ENOMEM':{}},
    # int fclose(FILE *stream);
    'fclose' : {'EBADF':{0:'fd'},'ENOTOPEN':{0:'fd'},'ESCANFAILURE':{0:'fd'},'EIOERROR':{},'EIORECERR':{}},
    # int fflush(FILE *stream);
    'fflush' : {'EBADF':{0:'file'},'ENOTOPEN':{0:'file'},'ESCANFAILURE':{0:'file'},'EIOERROR':{},'EIORECERR':{}},
    # int fgetc(FILE *stream);
    'fgetc' : {'EBADF':{0:'file'},'ECONVERT':{},'ENOTREAD':{0:'file'},'EGETANDPUT':{},'ERECIO':{0:'file'},'ESTDIN':{},'EIOERROR':{},'EIORECERR':{}},
    # char *fgets (char *string, int n, FILE *stream);
    'fgets' : {'EBADF':{2:'file'},'ECONVERT':{},'ENOTREAD':{2:'file'},'EGETANDPUT':{},'ERECIO':{2:'file'},'ESTDIN':{},'EIOERROR':{},'EIORECERR':{}},
    # FILE *fopen(const char *pathname, const char *mode);
    'fopen' : {'EINVAL':{0:'string',1:'string'},
               'EACCES':{0:'string'},
               'EDQUOT':{0:'string'},
               'EEXIST':{0:'string'},
               'EFAULT':{0:'string'},
               'EFBIG':{0:'string'},
               'EINTR':{},
               'EISDIR':{0:'string'},
               'ELOOP':{0:'string'},
               'EMFILE':{},
               'ENAMETOOLONG':{0:'string'},
               'ENFILE':{},
               'ENODEV':{0:'string'},
               'ENOENT':{0:'string'},
               'ENOMEM':{},
               'ENOSPC':{0:'string'},
               'ENOTDIR':{0:'string'},
               'ENXIO':{0:'string'},
               'EOPENOTSUPP':{0:'string'},
               'EOVERFLOW':{0:'string'},
               'EPERM':{1:'string'},
               'EROFS':{0:'string'},
               'ETXTBSY':{0:'string'},
               'EWOULDBLOCK':{1:'string'},
               # EBADF,ENOTDIR
               },
    # int fputc(int c, FILE *stream);
    'fputc' : {'ECONVERT':{0:'concrete'},'ENOTWRITE':{1:'file'},'EPUTANDGET':{},"ERECIO":{1:'file'},'ESTDERR':{1:'file'},
               'ESTDOUT':{1:'file'},'EIOERROR':{},'EIORECERR':{}},
    # int fputs(const char *s, FILE *stream);
    'fputs' : {'ECONVERT':{0:'concrete'},'ENOTWRITE':{1:'file'},'EPUTANDGET':{},"ERECIO":{1:'file'},'ESTDERR':{1:'file'},
               'ESTDOUT':{1:'file'},'EIOERROR':{},'EIORECERR':{}},
    # size_t fread(void *buffer, size_t size, size_t count, FILE *stream);
    'fread' : {'EBADF':{3:'file'},'ECONVERT':{},'ENOTREAD':{3:'file'},'EGETANDPUT':{},'ERECIO':{3:'file'},'ESTDIN':{},'EIOERROR':{},'EIORECERR':{}},
    # int fscanf(FILE *stream, const char *format, ...);
    'fscanf' : {'EAGAIN':{0:'file'},'EBADF':{0:'file'},'EILSEQ':{},'EINTR':{},'EINVAL':{1:'string'},'ENOMEM':{},'ERANGE':{1:'string'}},
    # int fseek(FILE *stream, long offset, int whence);
    'fseek' : {'EINVAL':{2:'concrete'},'ESPIPE':{0:'file'},'EBADF':{0:'file'},'ENOTOPEN':{0:'file'},'ESCANFAILURE':{0:'file'},'EIOERROR':{},'EIORECERR':{}},
    # long ftell(FILE *stream);
    'ftell' : {'ESPIPE':{0:'file'},'EBADF':{0:'file'},'ENOTOPEN':{0:'file'},'ESCANFAILURE':{0:'file'},'EIOERROR':{},'EIORECERR':{}},
    # int fprintf(FILE *restrict stream, const char *restrict format, ...);
    # 'fprintf' : {'ECONVERT':{},'ENOTWRITE':{0:'fd'},'EPUTANDGET':{},"ERECIO":{0:'fd'},'ESTDERR':{0:'fd'},
            #    'ESTDOUT':{0:'fd'},'EIOERROR':{},'EIORECERR':{},'EILSEQ':{},'EINVAL':{},'ENOMEM':{},'EOVERFLOW':{}},
    # size_t fwrite(const void *ptr, size_t size, size_t nitems,FILE *stream);
    'fwrite' : {'ECONVERT':{0:'ptr'},'ENOTWRITE':{3:'file'},'EPUTANDGET':{},"ERECIO":{3:'file'},'ESTDERR':{3:'file'},
               'ESTDOUT':{3:'file'},'EIOERROR':{},'EIORECERR':{}},
    # int getchar(void);   
    'getchar' : {'EBADF':{},'ECONVERT':{},'EGETANDPUT':{},'EIOERROR':{},'EIORECERR':{}},
    # malloc: no
    # memcmp: no
    # memcpy: no
    # memset: no
    # openlog: no
    # perror: no
    # int printf(const char *restrict format, ...);
    # 'printf' : {'ECONVERT':{},'ENOTWRITE':{},'EPUTANDGET':{},"ERECIO":{},'ESTDERR':{},
            # 'ESTDOUT':{},'EIOERROR':{},'EIORECERR':{},'ENOMEM':{},'EOVERFLOW':{}},
    # int putc(int c, FILE *stream);
    'putc' : {'ECONVERT':{0:'concrete'},'ENOTWRITE':{1:'file'},'EPUTANDGET':{},"ERECIO":{1:'file'},'ESTDERR':{1:'file'},
               'ESTDOUT':{1:'file'},'EIOERROR':{},'EIORECERR':{}},
    # int putchar(int c);
    # 'putchar' : {'ECONVERT':{0:'concrete'},'ENOTWRITE':{},'EPUTANDGET':{},"ERECIO":{},'ESTDERR':{},
            #    'ESTDOUT':{},'EIOERROR':{},'EIORECERR':{}},
    # rand: no
    # realloc: no
    # void rewind(FILE *stream);
    'rewind' : {'ESPIPE':{0:'file'},'EBADF':{0:'file'},'ENOTOPEN':{0:'file'},'ESCANFAILURE':{0:'file'},'EIOERROR':{},'EIORECERR':{}},
    # scanf: no
    # int setvbuf(FILE *restrict stream, char *restrict buf, int type, size_t size);
    'setvbuf' : {'EBADF':{0:'file'}},
    # int snprintf(char *restrict s, size_t n, const char *restrict format, ...);
    'snprintf' : {'ECONVERT':{},'ENOTWRITE':{},'EPUTANDGET':{},"ERECIO":{},'ESTDERR':{},
            'ESTDOUT':{},'EIOERROR':{},'EIORECERR':{},'ENOMEM':{},'EOVERFLOW':{}},
    # srand: no
    # sscanf: no
    # strcat: no
    # strchr: no
    # strcmp: no
    # strcpy: no
    # strlen: no
    # strncmp: no
    # strncpy: no
    # strstr: no
    # long strtol(const char *restrict str, char **restrict endptr, int base);
    'strtol' : {'ERANGE':{0:'string'},'EINVAL':{2:'concrete'}},
    # int system(const char *command);
    'system' : {'ECHILD':{},'EAGAIN':{},'ENOMEM':{}},
    # tmpnam: no
    # tolower: no
    # toupper: no
    # ungetc: no
    # int vsprintf(char *restrict s, const char *restrict format, va_list ap);
    'vsprintf' : {'ECONVERT':{0:'string'},'ENOTWRITE':{},'EPUTANDGET':{},"ERECIO":{},'ESTDERR':{},
               'ESTDOUT':{},'EIOERROR':{},'EIORECERR':{},'EILSEQ':{},'EINVAL':{},'ENOMEM':{},'EOVERFLOW':{}},
    # wchar

    # from linux kernel
    # arch_prctl
    # arm_user_helpers
    # int brk(void *addr);
    'brk' : {'ENOMEM':{}},
    # cwd
    # futex: not implemented yet
    # getpit: no
    # int getrlimit(int resource, struct rlimit *rlim);
    # 'getrlimit' : {'EFAULT':{1:'ptr'},'EINVAL':{0:'concrete'},'EPERM':{},},
    # gettid: no
    # ssize_t readv(int fd, const struct iovec *iov, int iovcnt);
    'readv' : {'EAGAIN':{0:'fd'},'EWOULDBLOCK':{0:'fd'},'EBADF':{0:'fd'},'EFAULT':{1:'ptr'},'EINTR':{},'EINVAL':{0:'fd',1:'ptr',2:'concrete'},'EIO':{0:'fd'},'EISDIR':{0:'fd'}},
    # ssize_t writev(int fd, const struct iovec *iov, int iovcnt);
    'writev' : {'EAGAIN':{0:'fd'},'EWOULDBLOCK':{0:'fd'},'EBADF':{0:'fd'},'EFAULT':{1:'ptr'},'EINTR':{},'EINVAL':{0:'fd',1:'ptr',2:'concrete'},'EIO':{0:'fd'},'EISDIR':{0:'fd'}},
    # off_t lseek(int fd, off_t offset, int whence);
    'lseek' : {'EBADF':{0:'fd'},'EINVAL':{2:"concrete"},'ENXIO':{2:"concrete"},'EOVERFLOW':{0:'fd'},'ESPIPE':{0:'fd'}},
    # void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
    'mmap' : {'EACCES':{4:'fd',3:'int'},'EAGAIN':{4:'fd'},'EBADF':{4:'fd'},'EEXIST':{0:'concrete',1:'concrete'},'EINVAL':{0:'concrete',1:'concrete',3:'concrete'},
              'ENFILE':{},'ENODEV':{},'ENOMEM':{},'EOVERFLOW':{1:'concrete'},'EPERM':{2:'concrete'},'ETXTBSY':{3:'concrete',4:'fd'},'SIGSEGV':{0:'concrete'},'SIGBUS':{5:'concrete'}},
    # int mprotect(void *addr, size_t len, int prot);
    'mprotect': {'EACCESS':{0:'concrete'},'EINVAL':{0:'concrete',2:'concrete'},'ENOMEM':{0:'concrete',1:'concrete'}},
    # int munmap(void *addr, size_t length);
    'munmap' : {'EINVAL':{0:'concrete',1:'concrete'}},
    # set_tid_address: no
    # int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
    'sigaction' : {'EFAULT':{1:'ptr',2:'ptr'},'EINVAL':{}},
    # int sigprocmask(int how, const sigset_t *set, sigset_t *oldset);
    'sigprocmask' : {'EFAULT':{1:'ptr',2:'ptr'},'EINVAL':{}},
    # int stat(const char *path, struct stat *buf);
    'stat' : {'EACCES':{0:'string'},'ELOOP':{0:'string'},'ENAMETOOLONG':{0:'string'},'ENOENT':{0:'string'},'ENOMEM':{},'ENOTDIR':{0:'string'},'EOVERFLOW':{0:'string'}},
    # int sysinfo(struct sysinfo *info);操作
    'sysinfo' : {'EFAULT':{0:'ptr'}},
    # int tgkill(int tgid, int tid, int sig);
    'tgkill' : {'EAGAIN':{2:'concrete'},'EINVAL':{0:'concrete',1:'concrete'},'EPERM':{},'ESRCH':{0:'concrete',1:'concrete'}},
    # time_t time(time_t *t);
    'time' : {'EFAULT':{0:'ptr'}},
    # getuid: no
    # getgid: no
    # uname: no
    # int unlink(const char *pathname);
    'unlink' : {
        'EACCES':{0:'string'},
        'EBUSY':{0:'string'},
        'EFAULT':{0:'string'},
        'EIO':{0:'string'},
        'EISDIR':{0:'string'},
        'ELOOP':{0:'string'},
        'ENAMETOOLONG':{0:'string'},
        'ENOENT':{0:'string'},
        'ENOMEM':{},
        'ENOTDIR':{0:'string'},
        'EPERM':{0:'string'},
        'EROFS':{0:'string'},
    },
    
    # posix
    # int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
    'accept' : {'EAGAIN':{0:'fd'},'EWOULDBLOCK':{0:'fd'},'EBADF':{0:'fd'},'ECONNABORTED':{},'EFAULT':{1:'ptr'},'EINTR':{},
                'EINVAL':{0:'fd',2:'ptr'},'EMFILE':{},'ENFILE':{},'ENOBUFS':{},'ENOMEM':{},'ENOTSOCK':{0:'fd'},'EOPNOTSUPP':{0:'fd'},
                'EPROTO':{0:'fd'},'EPERM':{}},
    # int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
    'bind' : {'EACCES':{1:'ptr'},'EADDRINUSE':{1:'ptr'},'EBADF':{0:'fd'},'EINVAL':{0:'fd',1:'ptr'},'ENOTSOCK':{0:'fd'},'EADDRNOTAVAIL':{1:'ptr'},
                'EFAULT':{1:'ptr'},'ELOOP':{1:'ptr'},'ENAMETOOLONG':{1:'ptr'},'ENOENT':{1:'ptr'},'ENOMEM':{},'ENOTDIR':{1:'ptr'},'EROFS':{}},
    # int close(int fd);
    'close' : {'EBADF':{0:'fd'},'EINTR':{},'EIO':{},'ENOSPC':{},'EDQUOT':{}},
    # int closedir(DIR *dirp);
    'closedir' : {'EBADR':{0:'ptr'}},
    # int dup(int oldfd);
    'dup' : {'EBADF':{0:'fd'},'EMFILE':{}},
    # int dup2(int oldfd, int newfd);
    'dup2' : {'EBADF':{0:'fd',1:'fd'},'EBUSY':{},'EINTR':{},'EMFILE':{}},
    # int dup3(int oldfd, int newfd, int flags);
    'dup3' : {'EBADF':{0:'fd',1:'fd'},'EBUSY':{},'EINTR':{},'EMFILE':{},'EINVAL':{0:'fd',1:'fd',2:''}},
    # int fcntl(int fd, int cmd, ... /* arg */ );
    'fcntl' : {'EACCES':{},'EAGAIN':{},'EBADF':{0:'fd'},'EBUSY':{1:'concrete'},'EDEADLK':{1:'concrete'},'EFAULT':{},
                'EINTR':{1:'concrete'},'EINVAL':{1:'concrete'},'EMFILE':{1:'concrete'},'ENOLCK':{},
                'ENOTDIR':{0:'fd'},'EPERM':{}},
    # FILE *fdopen(int fd, const char *mode);
    'fdopen' : {'EACCES':{},'EAGAIN':{},'EBADF':{0:'fd'},'EBUSY':{1:'string'},'EDEADLK':{1:'string'},'EFAULT':{},
                'EINTR':{1:'string'},'EINVAL':{1:'string'},'EMFILE':{1:'string'},'ENOLCK':{},
                'ENOTDIR':{0:'fd'},'EPERM':{}},
    # int fileno(FILE *stream);
    'fileno' : {'EBADF':{0:'fd'}},
    # fork: no
    # gethostbyname saves at h_errno
    # char *getpass( const char *prompt);
    'getpass' : {0:'string'},
    # int getsockopt(int sockfd, int level, int optname, void *optval, socklen_t *optlen);
    'getsockopt' : {'EBADF':{0:'fd'},'EFAULT':{3:'ptr',4:'ptr'},'EINVAL':{4:'ptr'},'ENOPROTOOPT':{},'ENOTSOCK':{0:'fd'}},
    # htonl: no
    # htons: no
    # inet_ntoa: no
    # int listen(int sockfd, int backlog);
    'listen' : {'EADDRINUSE':{0:'fd'},'EBADF':{0:'fd'},'ENOTSOCK':{0:'fd'},'EOPNOTSUPP':{}},
    # int open(const char *pathname, int flags);
    'open' : {'EACCES':{0:'string'},'EDQOT':{},'EEXIST':{0:'string',1:'concrete'},'EFBIG':{0:'string'},'EINTR':{},'EINVAL':{0:'string',1:'concrete'},
                'EISDIR':{0:'string'},'ELOOP':{0:'string'},'EMFILE':{},'ENAMETOOLONG':{0:'string'},'ENFILE':{},'ENODEV':{0:'string'},
                'ENOENT':{0:'string',1:'concrete'},'ENOMEM':{},'ENOSPC':{0:'string'},'ENOTDIR':{0:'string'},'ENXIO':{0:'string'},
                'EOPNOTSUPP':{},'EOVERFLOW':{0:'string'},'EPERM':{},'EROFS':{0:'string'},'ETXTBSY':{0:'string'},'EWOULDBLOCK':{},},
    'open64' : {'EACCES':{0:'string'},'EDQOT':{},'EEXIST':{0:'string',1:'concrete'},'EFBIG':{0:'string'},'EINTR':{},'EINVAL':{0:'string',1:'concrete'},
                'EISDIR':{0:'string'},'ELOOP':{0:'string'},'EMFILE':{},'ENAMETOOLONG':{0:'string'},'ENFILE':{},'ENODEV':{0:'string'},
                'ENOENT':{0:'string',1:'concrete'},'ENOMEM':{},'ENOSPC':{0:'string'},'ENOTDIR':{0:'string'},'ENXIO':{0:'string'},
                'EOPNOTSUPP':{},'EOVERFLOW':{0:'string'},'EPERM':{},'EROFS':{0:'string'},'ETXTBSY':{0:'string'},'EWOULDBLOCK':{},},
    # DIR *opendir(const char *name);
    'opendir' : {'EACCES':{0:'string'},'EMFILE':{},'ENFILE':{},'ENOENT':{0:'string'},'ENOTDIR':{0:'string'}},
    # ssize_t pread(int fd, void *buf, size_t count, off_t offset);
    'pread' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'EFAULT':{1:'ptr'},'EINVAL':{2:"concrete"},'ENXIO':{2:"concrete"},'EINTR':{},'EOVERFLOW':{0:'fd'},'ESPIPE':{0:'fd'},'EIO':{},'EISDIR':{0:'fd'}},
    'pread64' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'EFAULT':{1:'ptr'},'EINVAL':{2:"concrete"},'ENXIO':{2:"concrete"},'EINTR':{},'EOVERFLOW':{0:'fd'},'ESPIPE':{0:'fd'},'EIO':{},'EISDIR':{0:'fd'}},
    # pthread: no
    # ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset);
    'pwrite' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'EDESTADDRREQ':{0:'fd'},'EDQUOT':{0:'fd'},'EFBIG':{0:'fd'},'EFAULT':{1:'ptr'},'EINVAL':{2:"concrete"},'ENXIO':{2:"concrete"},'EOVERFLOW':{0:'fd'},'ESPIPE':{0:'fd'},'EIO':{},'EISDIR':{0:'fd'},'ENOSPC':{0:'fd'},'EPIPE':{0:'fd'}},
    'pwrite64' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'EDESTADDRREQ':{0:'fd'},'EDQUOT':{0:'fd'},'EFBIG':{0:'fd'},'EFAULT':{1:'ptr'},'EINVAL':{2:"concrete"},'ENXIO':{2:"concrete"},'EOVERFLOW':{0:'fd'},'ESPIPE':{0:'fd'},'EIO':{},'EISDIR':{0:'fd'},'ENOSPC':{0:'fd'},'EPIPE':{0:'fd'}},
    # ssize_t read(int fd, void *buf, size_t count);
    'read' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'EFAULT':{1:'ptr'},'EINTR':{},'EINVAL':{2:"concrete"},'EIO':{},'EISDIR':{0:'fd'}},
    # struct dirent *readdir(DIR *dirp);
    'readdir' : {'EBADF':{0:'ptr'}},
    # ssize_t recv(int sockfd, void *buf, size_t len, int flags);
    'recv' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'ECONNREFUSED':{},'EFAULT':{1:'ptr'},'EINTR':{},'EINVAL':{},'ENOTCONN':{0:'fd'},'ENOTSOCK':{0:'fd'}},
    # ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen);
    'recvfrom' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'ECONNREFUSED':{},'EFAULT':{1:'ptr'},'EINTR':{},'EINVAL':{},'ENOTCONN':{0:'fd'},'ENOTSOCK':{0:'fd'}},
    # int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);
    'select' : {'EBADF':{0:'ptr'},'EINTR':{},'EINVAL':{0:'concrete'},'ENOMEM':{}},
    # int poll(struct pollfd *fds, nfds_t nfds, int timeout);
    'poll' : {'EFAULT':{},'EINTR':{},'EINVAL':{},'ENOMEM':{}},
    # ssize_t send(int sockfd, const void *buf, size_t len, int flags);
    'send' : {'EACCES':{0:'fd'},'EAGAIN':{0:'fd'},'EWOULDBLOCK':{},'EALREADY':{},'EBADF':{0:'fd'},'ECONNRESET':{},'EDESTADDRREQ':{0:'fd'},'EFAULT':{1:'ptr'},'EINTR':{},
                'EINVAL':{},'EISCONN':{0:'fd'},'EMSGSIZE':{0:'fd'},'ENOBUFS':{},'ENOMEM':{},'ENOTCONN':{0:'fd'},'ENOTSOCK':{0:'fd'},'EOPNOTSUPP':{3:'concrete'},'EPIPE':{}},
    # int setsockopt(int sockfd, int level, int optname, const void *optval, socklen_t optlen);
    'setsockopt' : {'EBADF':{0:'fd'},'EFAULT':{3:'ptr',4:'ptr'},'EINVAL':{4:'ptr'},'ENOPROTOOPT':{},'ENOTSOCK':{0:'fd'}},
    # gettimeofday,clock_gettime: no
    # sleep: no
    # int socket(int domain, int type, int protocol);
    'socket' : {'EACCES':{1:'concrete',2:'concrete'},'EAFNOSUPPORT':{},'EINVAL':{1:'concrete',2:'concrete'},'EMFILE':{},'ENFILE':{},'ENOBUFS':{},'ENOMEM':{},'EPROTONOSUPPORT':{1:'concrete',2:'concrete'}},
    # strcasecmp:no
    # char *strdup(const char *s);
    'strdup' : {'ENOMEM':{}},
    # strtok_r : no
    # syslog : no
    # ssize_t write(int fd, const void *buf, size_t count);
    'write' : {'EAGAIN':{},'EWOULDBLOCK':{},'EBADF':{0:'fd'},'EDESTADDRREQ':{0:'fd'},'EINTR':{},'EDQUOT':{0:'fd'},'EFBIG':{0:'fd'},'EFAULT':{1:'ptr'},'EINVAL':{2:"concrete"},'EOVERFLOW':{0:'fd'},'EIO':{},'ENOSPC':{0:'fd'},'EPIPE':{0:'fd'}},
    # int stat(const char *pathname, struct stat *statbuf);
    # int fstat(int fd, struct stat *statbuf);
    # int lstat(const char *pathname, struct stat *statbuf);
    # int __xstat(int ver, const char * path, struct stat * stat_buf);
    # int __lxstat(int ver, const char * path, struct stat * stat_buf);
    # int __fxstat(int ver, int fildes, struct stat * stat_buf);
    'xstat' : {'EACCES':{0:'string'},'ELOOP':{0:'string'},'ENAMETOOLONG':{0:'string'},'ENOENT':{0:'string'},'ENOMEM':{},'ENOTDIR':{0:'string'},'EOVERFLOW':{0:'string'}},
    'fstat' : {'EBADF':{0:'fd'},'ELOOP':{0:'fd'},'ENOENT':{0:'fd'},'ENOMEM':{},'EOVERFLOW':{0:'fd'}},
    'lstat' : {'EACCES':{0:'string'},'ELOOP':{0:'string'},'ENAMETOOLONG':{0:'string'},'ENOENT':{0:'string'},'ENOMEM':{},'ENOTDIR':{0:'string'},'EOVERFLOW':{0:'string'}},
    '__xstat64' : {'EACCES':{1:'string'},'ELOOP':{1:'string'},'ENAMETOOLONG':{1:'string'},'ENOENT':{1:'string'},'ENOMEM':{},'ENOTDIR':{1:'string'},'EOVERFLOW':{1:'string'}},
    '__fstat64' : {'EBADF':{1:'fd'},'ELOOP':{1:'fd'},'ENOENT':{1:'fd'},'ENOMEM':{},'EOVERFLOW':{1:'fd'}},
    '__lstat64' : {'EACCES':{1:'string'},'ELOOP':{1:'string'},'ENAMETOOLONG':{1:'string'},'ENOENT':{1:'string'},'ENOMEM':{},'ENOTDIR':{1:'string'},'EOVERFLOW':{1:'string'}},
    '__xstat' : {'EACCES':{1:'string'},'ELOOP':{1:'string'},'ENAMETOOLONG':{1:'string'},'ENOENT':{1:'string'},'ENOMEM':{},'ENOTDIR':{1:'string'},'EOVERFLOW':{1:'string'}},
    '__fstat' : {'EBADF':{1:'fd'},'ELOOP':{1:'fd'},'ENOENT':{1:'fd'},'ENOMEM':{},'EOVERFLOW':{1:'fd'}},
    '__lstat' : {'EACCES':{1:'string'},'ELOOP':{1:'string'},'ENAMETOOLONG':{1:'string'},'ENOENT':{1:'string'},'ENOMEM':{},'ENOTDIR':{1:'string'},'EOVERFLOW':{1:'string'}},
}

class VmArg(object):
    def __init__(self, arg, value, desc):
        self.arg = arg
        self.value = value
        self.desc = desc
    def repr_data(self):
        return ('vmarg', self.arg, self.value, self.desc)

class ExecCmd(object):
    def __init__(self, cmd, desc):
        self.cmd = cmd
        self.desc = desc
    def repr_data(self):
        return ('exec_cmd', self.cmd, self.desc)

class ForceExecTask(object):
    def __init__(self, pc, lr, time, index, value, desc):
        self.pc = pc
        self.lr = lr
        # time: before or after the block
        self.time = time
        self.index = index
        self.value = value
        self.desc = desc
    def repr_data(self):
        return ('force_exec', self.pc, self.lr, self.time, self.index, self.value, self.desc)
    def repr_data_qmp(self):
        return 'plugin_cmd change_%d_%d_%d_%d_%d' %(self.pc, self.lr, 1 if self.time == 'before' else 2, self.index, self.value)
    def matches(self, pc, lr, time):
        if (time == self.time) and (pc == self.pc) and (lr == self.lr):
            return True
        else:
            return False
    def do_fix(self, panda):
        cpu = panda.get_cpu()
        cpu.env_ptr.regs[self.index] = self.value

class ExtraLib(object):
    def __init__(self, lib, desc):
        self.lib = lib
        self.desc = desc
    def repr_data(self):
        return ('extra_lib', self.lib, self.desc)

class ExtraRecord(object):
    def __init__(self, lib, start, end, desc):
        self.lib = lib
        self.start = start
        self.end = end
        self.desc = desc
    def repr_data(self):
        return ('extra_record', self.lib, self.start, self.end, self.desc)

class ExtraRecordRange(object):
    def __init__(self, proj, extra_record_task):
        self.start = extra_record_task.start
        self.end = extra_record_task.end
        if self.end == None:
            self.end = 1000000000000
        self.pos_range = range(self.start, self.end)
        self.addr_range = proj.determine_range(proj.extra_libs + [extra_record_task.lib])
    def in_range(self, pos, addr):
        if (pos in self.pos_range) and (addr in self.addr_range):
            return True
        else:
            return False

class FixDesc(object):
    def __init__(self, file = None):
        # custom args for vm initialization, for example, ip address
        self.custom_vm_args = []
        # commands to be executed before the target program being executed
        self.before_exec_cmds = []
        # extra recording range
        self.extra_libs = []
        # force execution operations to be performed
        self.force_exec_tasks = []
        # deeper look into some function
        self.extra_record_tasks = []
        self.file = file
        if file != None:
            file = open(file, "r")
            tasks_raw = []
            for l in file.readlines():
                tasks_raw.append(eval(l))
            for t in tasks_raw:
                if t[0] == 'vmarg':
                    new = VmArg(t[1],t[2],t[3])
                    self.custom_vm_args.append(new)
                elif t[0] == 'exec_cmd':
                    new = ExecCmd(t[1],t[2])
                    self.before_exec_cmds.append(new)
                elif t[0] == 'force_exec':
                    new = ForceExecTask(t[1],t[2],t[3],t[4],t[5],t[6])
                    self.force_exec_tasks.append(new)
                elif t[0] == 'extra_lib':
                    new = ExtraLib(t[1],t[2])
                    self.extra_libs.append(new)
                elif t[0] == 'extra_record':
                    new = ExtraRecord(t[1],t[2],t[3],t[4])
                    self.extra_record_tasks.append(new)
    def to_file(self, filename, overwrite = False):
        if overwrite:
            out = open(filename, 'w+')
        else:
            out = open(filename, 'a+')
        if out != None:
            for v in self.custom_vm_args:
                f.write(format(v.repr_data()) + '\n')
            for b in self.before_exec_cmds:
                f.write(format(b.repr_data()) + '\n')
            for e in self.extra_libs:
                f.write(format(e.repr_data()) + '\n')
            for f in self.force_exec_tasks:
                f.write(format(f.repr_data()) + '\n')
            for r in self.extra_record_tasks:
                f.write(format(f.repr_data()) + '\n')
    def hashable_string(self):
        repr = ""
        if self.custom_vm_args != []:
            repr = repr + repr(self.custom_vm_args)
        if self.before_exec_cmds != []:
            repr = repr + repr(self.before_exec_cmds)
        if self.extra_libs != []:
            repr = repr + repr(self.extra_libs)
        if self.force_exec_tasks != []:
            repr = repr + repr(self.force_exec_tasks)
        if self.extra_record_tasks != []:
            repr = repr + repr(self.extra_record_tasks)
        return repr

common_libs = [
    # TODO: more of this!
    'libc.so.0',
    'libc.so.6',
    'libc-2.15.so',
    'libc-2.11.1.so',
    'libc-2.5.so'
]

def ensure_path_available(workdir, filepath):
    commands = []
    dirs = []
    parent = filepath
    while True:
        parent = os.path.dirname(parent)
        if parent == '/' or parent == '':
            break
        else:
            dirs.insert(0,parent)
    for d in dirs:
        if d[0] == '/':
            d = d[1:]
        exists = os.path.exists(os.path.join(workdir,'rootfs',d))
        islink = os.path.islink(os.path.join(workdir,'rootfs',d))
        if (exists == False) and (islink == True):
            orig = os.readlink(os.path.join(workdir,'rootfs',d))
            if orig[0] == '/':
                orig = orig[1:]
            cmds = ensure_path_available(workdir, orig)
            if cmds != []:
                commands = cmds.extend(commands)
            commands.append('mkdir -p %s' %orig)
        elif (exists == False) and (islink == False):
            commands.append('mkdir -p %s' %d)
    return commands

def solver_bind(workdir, retinfo):
    progress("suggestion for bind(): alter the network configuration")
    new_ip = retinfo.callinfo.args[1].data[1]
    port = retinfo.callinfo.args[1].data[2]
    net = new_ip[:-1]
    value = 'user,net=%s0/24,dhcpstart=%s,hostfwd=tcp::10080-:%d' %(net,new_ip,port)
    return VmArg('-net',value,None)

def solver_fopen(workdir, retinfo):
    progress("suggestion for fopen(): ensure the path exists")
    file_path = format(retinfo.callinfo.args[0])
    dir_path = os.path.dirname(file_path)
    cmds = ensure_path_available(workdir, file_path)
    if cmds != []:
        str = ''
        for c in cmds[:-1]:
            str = str + c + ';'
        str = str + cmds[-1]
        return ExecCmd(str, 'solve fopen(%s)'%file_path)
    else:
        return None

def solver_open(workdir, retinfo):
    progress("suggestion for open(): ensure the path exists")
    file_path = format(retinfo.callinfo.args[0])
    dir_path = os.path.dirname(file_path)
    cmds = ensure_path_available(workdir, file_path)
    if cmds != []:
        str = ''
        for c in cmds[:-1]:
            str = str + c + ';'
        str = str + cmds[-1]
        return ExecCmd(str, 'solve open(%s)'%file_path)
    else:
        return None

def solver_enoent(args):
    progress("suggestion for ENOENT: ensure the path exists")
    filepath = args[0]
    dir_path = os.path.dirname(file_path)
    cmds = ensure_path_available(workdir, filepath)
    if cmds != []:
        str = ''
        for c in cmds[:-1]:
            str = str + cmd + ';'
        str = str + cmds[-1]
        return ExecCmd(str, 'solve ENOENT: %s'%filepath)
    else:
        return None

func_solver_dict = {
    # TODO: more solver function
    'bind': solver_bind,
    'fopen': solver_fopen,
    'open': solver_open,
    'open64': solver_open
}

errno_solver_dict = {
    'ENOENT': solver_enoent,
}

def gen_solution_by_errno(workdir, errno, retinfo):
    # TODO: more cases
    errno_repr = errno_dict[errno]
    func_name = retinfo.name
    if (func_name in blame_dict):
        arg_indexes = (blame_dict[func_name])[errno_repr]
        args = []
        for a in arg_indexes.keys():
            args.append(retinfo.callinfo.args[a])
        # progress("%s: %s" %(errno_repr, repr(args)))
        if errno_repr in errno_solver_dict:
            return errno_solver_dict[errno_repr](args)
        else:
            progress_warn("not implemented yet...")
            progress("%s:%s" %(errno_repr, repr(retinfo)))
            return None
    else:
        progress_warn("not implemented yet...")
        progress("%s:%s" %(errno_repr, repr(retinfo)))
        return None

def gen_solution_by_func(workdir, retinfo):
    name = retinfo.name
    if name in func_solver_dict:
        return func_solver_dict[name](workdir, retinfo)
    else:
        progress_warn("not implemented yet...")
        return None

force_exec_funcs = {
    'agApi_fwGetNextTriggerConf': (0,1), # netgear httpd
    'isLanSubnet': (0,1), # netgear httpd
    # 'MSG_RegModule': (range(1,0xffffffff),0) #netgear mini_httpd
    'CMM_Init': (range(2,0xffffffff),1), #netgear mini_httpd
    'mssl_init' :(0,0xffffffff) # asus httpd
}
