#!/usr/bin/env python3
from base_class import *
from query_sql import Database
import os,sys
from time import sleep
from pandare import blocking
import pyvex,claripy
from pandare.asyncthread import AsyncThread
import threading
import select
from tqdm import tqdm
import getopt
from fixes import *
from api_identity import parse_val_legacy, preprocess_skip_funcs

class MyThread(threading.Thread):
    def __init__(self, func, args):
        super(MyThread, self).__init__()
        self.func = func
        self.args = args
    def run(self):
        self.result = self.func(*self.args)
    def get_result(self):
        try:
            return self.result
        except Exception:
            return None

def get_input(caption='', timeout=10):
    progress_warn(caption)
    ch = None
    i, o, e = select.select([sys.stdin], [], [], timeout)
    if i:
        ch = sys.stdin.readline().strip()
    return ch

class AnalysisProject(BaseProject):

    def __init__(self, target_path, workdir, target_bin=None, arch='armel', load_fix = None, init_panda = True):
        BaseProject.__init__(self, target_path, workdir, target_bin=target_bin, arch = arch, load_symbol=True, init_db = False, load_fix = load_fix, init_panda = init_panda)
        self.db = Database(self.workdir, self.target_bin, db_source= (os.path.join(self.workdir, self.target_bin)+'.db'))
        self.vm_ready = False
        self.last_checkpoint = 0
        self.threading_event = None
        self.async_thread = None
        self.call_info_queue = []
        self.ret_info_queue = []
        self.cond = threading.Condition()
        self.stop_points = []
        self.program_state = ProgramState()
        if (init_panda):
            self.prepare_vm()
        self.candidate_solutions = []

    def prepare_vm(self):
        def register_callback():
            self.panda.disable_tb_chaining()
            self.panda.enable_precise_pc()
            @self.panda.cb_before_block_exec(name="before", procname = self.target_bin)
            def bbe(cpu, tb):
                '''
                bbe: the before-block-execute callback function.
                during the trace preprocess, it pauses the vm before each block to be analyzed being executed.
                '''
                self.cond.acquire()
                pos = self.get_rr_instr()
                if self.vm_ready == False:
                    self.vm_ready = True
                    self.panda.disable_callback("before")
                    self.panda.vm_stop()
                elif self.is_target(cpu):
                    if pos >= self.last_checkpoint + 100000:
                        progress("executed %d instructions..." %pos)
                        self.last_checkpoint = pos
                    if pos == self.stop_points[0]:
                        self.panda.disable_callback("before")
                        self.stop_points.remove(pos)
                        self.panda.vm_stop()
                        if pos in self.ret_info_queue:
                        # if (len(self.ret_info_queue) > 0) and (pos == min(self.ret_info_queue)):
                            self.ret_info_queue.remove(pos)
                            self.program_state.new_return(self, pos = pos)
                        self.cond.notify()
                self.cond.release()
            @self.panda.cb_after_block_exec(name="after", procname = self.target_bin)
            def abe(cpu, tb, exitcode):
                if self.is_target(cpu = cpu):
                    if exitcode > 1:
                        pass
                    else:
                        pos = self.get_rr_instr()
                        if pos in self.call_info_queue:
                        # if (len(self.call_info_queue) > 0) and (pos == min(self.call_info_queue)):
                            self.call_info_queue.remove(pos)
                            self.program_state.new_call(self, pos = pos)
            if (self.fixes.force_exec_tasks != []):
                self.panda.disable_tb_chaining()
                self.panda.enable_precise_pc()
                @self.panda.cb_before_block_exec(name = 'fix_before_exec')
                def fix_bbe(cpu, tb):
                    if (self.is_target(cpu = cpu) == False):
                        return
                    for t in self.fixes.force_exec_tasks:
                        if t.matches(cpu.env_ptr.regs[15], cpu.env_ptr.regs[14], 'before'):
                            progress("performing force execution...")
                            t.do_fix(self.panda)
                @self.panda.cb_after_block_exec(name = 'fix_after_exec')
                def fix_abe(cpu, tb, exitcode):
                    if (self.is_target(cpu = cpu) == False):
                        return
                    for t in self.fixes.force_exec_tasks:
                        if t.matches(cpu.env_ptr.regs[15], cpu.env_ptr.regs[14], 'after'):
                            progress("performing force execution...")
                            t.do_fix(self.panda)
        def start_vm():
            self.panda.run_replay(self.target_bin)
            # self.run()
        start_vm.__blocking__ = "placeholder"

        if (self.vm_ready == False):
            self.threading_event = threading.Event()
            self.async_thread = AsyncThread(self.threading_event)
            register_callback()
            self.async_thread.queue(start_vm)
            self.threading_event.set()
        else:
            if (self.async_thread != None):
                self.async_thread.stop()
            if (self.threading_event != None):
                self.threading_event.clear()
            self.async_thread = AsyncThread(self.threading_event)
            self.async_thread.queue(start_vm)
            self.threading_event.set()
        progress("Done starting virtual machine...")

    def vm_cont(self):
        def cont():
            self.panda.enable_callback("before")
            self.panda.cont()
        cont.__blocking__ = "placeholder"
        if (self.vm_ready == False):
            progress_warn("virtual machine is not running. Init...")
            self.prepare_vm()
            self.async_thread.queue(cont, internal = True)
        else:
            self.async_thread.queue(cont, internal = True)

    def get_block(self, addr, size=None):
        if size:
            b = self.angr_proj.factory.block(addr, size)
        else:
            b = self.angr_proj.factory.block(addr)
        return b

    def generate_trace(self):

        def get_raw_trace():
            summaries = list(self.db.summary().values())
            progress("The database contains execution traces of the following processes/threads:")
            preferred = None
            max_dur = 0
            for task in summaries:
                print("%d: %s\t start:%d end:%d" %(summaries.index(task), task.stackid, task.start_pos, task.end_pos))
                overall_start, overall_end = task.get_whole_execution()
                dur = overall_end - overall_start
                if dur > max_dur:
                    preferred = summaries.index(task)
                    max_dur = dur
            choice = get_input("task to analyze (suggested:%d) :" %preferred) or preferred
            return self.db.fetch_events_by_stackid(summaries[int(choice)].stackid, type='state')

        def parse_trace(raw_states):
            #  fetch vmstates from database 
            states = []
            for r in raw_states:
                new_s = State(('before' if r[2]==0 else 'after'), r[1], r[3],int(r[5]), r[8:-1], r[4], r[-1])
                if (states == []) and (new_s.type == 'after'):
                    pass
                else:
                    states.append(new_s)
                new_s = None
                del new_s
            if states[-1].type == 'before':
                states.pop()
            progress("done extracting states from database...")
            # fetch basic blocks according to vmstates
            bbs = []
            index = 0
            total = len(states) // 2
            while (index < total):
                state_bb = states[2 * index]
                state_ab = states[2 * index + 1]
                if (state_bb == state_ab):
                    index += 1
                    continue
                elif (state_ab.exitcode >1):
                    index += 1
                    continue
                block = self.get_block(state_bb.pc, size=state_bb.size)
                # block = self.get_block(state_bb.pc)
                # while(block.size > state_bb.size):
                #     bp = index
                #     if (index + 1) < total:
                #         # progress_warn("%x:Normalizing... " %state_bb.pc)
                #         index += 1
                #         tmp_state_bb = states[2 * index]
                #         if (tmp_state_bb.pc == state_ab.pc):
                #             state_ab = states[2 * index + 1]
                #             state_bb.size += tmp_state_bb.size
                #         else:
                #             progress_err("Block of index %d: Failed to normailze the block!" %index)
                #             progress_err("pc: %x, block_size:%d, state_bb.size:%d" %(state_bb.pc, block.size, state_bb.size))
                #             break
                #     else:
                #         break
                # if(state_bb.size > block.size):
                #     progress_warn("%x:Normalizing..." %state_bb.pc)
                #     block = self.get_block(state_bb.pc, size = state_bb.size)
                if state_ab.pc == 0xffff10a0: # the block did not done executing
                    progress_warn("%x: this block did not done executing..." %state_bb.pc)
                    if len(states) < 2 * (index + 1):
                        index += 1
                        state_bb_middle = states[2 * index]
                        state_bb.size = state_bb.size - 4 + state_bb_middle.size
                        block = self.get_block(state_bb.pc, size = state_bb.size)
                        state_ab = states[2 * index + 1]
                    else:
                        pass
                new_block = BasicBlock(state_bb, state_ab, block)
                bbs.append(new_block)
                index += 1
            progress("done generating basic blocks according to vmstates...")
            return bbs
        raw = get_raw_trace()
        bbs = parse_trace(raw)
        progress("The trace contains %d blocks." %len(bbs))
        return bbs

    def gen_record_suggestion_addr(self, addr, pos = None):
        owner = self.loader.find_object_containing(addr)
        if owner != None:
            progress("the object that owns this address:%s" %owner.binary_basename)
        else:
            return None
        if owner.binary_basename in common_libs:
            progress("this address comes from a widely-used library, no need for further recording.")
            return None
        elif owner.binary_basename == self.target_bin:
            return None
        else:
            progress("not familiar with this library: %s, maybe we should have a deeper look into it." %owner.binary_basename)
            if pos == None:
                return ExtraLib(owner.binary_basename, '')
            else:
                return ExtraRecord(owner.binary_basename, pos, 100000000000,'')

    def gen_record_suggestion_sym(self, sym, pos = None):
        owner = sym.owner
        progress("the object that owns this address:%s" %owner.binary_basename)
        if owner.binary_basename in common_libs:
            progress("this address comes from a widely-used library, no need for further recording.")
            return None
        elif owner.binary_basename == self.target_bin:
            return None
        else:
            progress("not familiar with this library: %s, maybe we should have a deeper look into it." %owner.binary_basename)
            if pos == None:
                return ExtraLib(owner.binary_basename, '')
            else:
                return ExtraRecord(owner.binary_basename, pos, 100000000000,'')

    def pass_1(self, bbs, start_pos=None, main_func = None, skip_funcs = None, return_callstack = False):
        def prepare_skip_funcs(skip_funcs):
            skip_func_addrs = []
            skip_func_list = preprocess_skip_funcs
            for f in skip_func_list:
                sym = self.loader.find_symbol(f)
                if sym != None:
                    progress("The execution of %s() will be skipped." %sym.name)
                    skip_func_addrs.append(sym.rebased_addr)
            if skip_funcs != None:
                skip_func_addrs = skip_func_addrs + skip_funcs
            progress("skip funcs: %s" %repr(skip_func_addrs))
            return skip_func_addrs

        def find_start(bbs, start_pos):
            if (start_pos == None):
                return bbs
            else:
                for bb in bbs:
                    if (bb.pos >= start_pos):
                        new_bbs = bbs[bbs.index(bb):]
                        return new_bbs
                return []

        def remove_excess(bbs, main_func = None, skip_func_addrs = None, return_callstack = False):
            '''
            remove blocks of exported subroutines called by not-recorded functions.
            '''
            index = 0
            # begin_addr = 0x0
            new_bbs = []
            functions = []
            callstack = []
            last_plt_entry = None
            skip_end = None
            # while(index <= len(bbs) - 1):
            for index in tqdm(range(len(bbs))):
                bb = bbs[index]
                # if bb.exitcode > 1:
                #   index += 1
                #   continue
                if skip_end != None and bb.state_bb.pc != skip_end:
                    index += 1
                    continue
                else:
                    skip_end = None
                if len(callstack) == 0:
                    new_bbs.append(bb)
                elif (main_func != None) and (bb.state_bb.pc == main_func):
                    new_bbs.append(bb)
                elif (bb.state_bb.pc == new_bbs[-1].state_ab.pc):
                    new_bbs.append(bb)
                elif (new_bbs[-1].state_ab.pc == 0xffff10a0):
                    if bb.state_bb.pc in new_bbs[-1].block.instruction_addrs:
                        new_bbs[-1].state_ab = bb.state_ab
                        index += 1
                        continue
                    else:
                        new_bbs.append(bb)
                else:
                    if bb.state_bb.pc == callstack[-1]:
                        new_bbs.append(bb)
                    elif bb.state_bb.pc in self.symbol_dict:
                        if (last_plt_entry != None) and (self.symbol_dict[bb.state_bb.pc] == last_plt_entry):
                            new_bbs.append(bb)
                    else:
                        index += 1
                        continue
                if skip_func_addrs != None and bb.state_ab.pc in skip_func_addrs:
                    skip_end = bb.state_ab.lr
                # handle callstack
                if len(callstack) == 0:
                    pass
                elif bb.state_bb.pc == callstack[-1]:
                    self.ret_info_queue.append(bb.pos)
                    functions.pop()
                    callstack.pop()
                # check vex
                vex = bbs[index].block.vex
                if vex.jumpkind == "Ijk_Call":
                    callstack.append(bbs[index].state_ab.lr)
                    functions.append(bbs[index].state_ab.pc)
                    self.call_info_queue.append(bbs[index].state_ab.pos)
                elif vex.jumpkind == "Ijk_Sys_syscall":
                    callstack.append(bbs[index].state_ab.lr)
                    functions.append(bbs[index].state_ab.pc)
                    self.call_info_queue.append(bbs[index].state_ab.pos)
                elif bbs[index].state_ab.pc > 0xff000000:
                    if bbs[index].state_ab.pc in kuser:
                        callstack.append(bbs[index].state_ab.lr)
                        functions.append(bbs[index].state_ab.pc)
                        self.call_info_queue.append(bbs[index].state_ab.pos)
                    elif bbs[index].state_ab.pc == 0xffff10a0:
                        progress_warn("bb %d: have a look at this!" %index)
                out = bbs[index].state_ab.pc
                if self.loader.find_section_containing(out) != None:
                    if (self.loader.find_section_containing(out).name == '.plt'):
                        if (out in self.symbol_dict) and (self.symbol_dict[out].name != '_dl_linux_resolve') and (self.symbol_dict[out].name != '_dl_runtime_resolve') :
                            last_plt_entry = self.symbol_dict[out]
                index += 1
            if callstack != []:
                lastcall = functions[-1]
                if lastcall in self.symbol_dict:
                    sym = self.symbol_dict[lastcall]
                else:
                    sym = None
                if sym != None:
                    if sym.name not in ['exit','abort','exit_group','.fini','_exit']:
                        progress("last unreturned func: %s from %s" %(sym.name,repr(sym.owner)))
                        progress("the program died when executing this function. perhaps you need to hook that library and record again since the trace is probably not complete.")
                        record_suggestion = self.gen_record_suggestion_sym(sym, new_bbs[-1].state_ab.pos)
                        if record_suggestion != None:
                            self.candidate_solutions.append(record_suggestion)
                        else:
                            progress("the trace ends at addr %s, %s" %(self.loader.describe_addr(new_bbs[-1].state_ab.pc),repr(new_bbs[-1].state_ab)))
                    else:
                        progress("the program reached exit")
                else:
                    progress("last unreturned func:UNKNOWNFUNC_%X ,use gen_calltrace() to get more information." %(lastcall))
                    progress("the program died when executing this function. perhaps you need to hook that library and record again since the trace is probably not complete.")
                    record_suggestion = self.gen_record_suggestion_addr(lastcall, new_bbs[-1].state_ab.pos)
                    if record_suggestion != None:
                        self.candidate_solutions.append(record_suggestion)                        
                    op = get_input("if this is not good enough for you, the analysis would begin in 3s. otherwise, q to quit.")
                    if op == 'q':
                        return None
                    # else:
                    #     progress("the trace ends at addr %s, %s" %(self.loader.describe_addr(new_bbs[-1].state_ab.pc),repr(new_bbs[-1].state_ab)))              
            if (return_callstack): 
                return new_bbs, callstack
            else:
                return new_bbs
        bbs = find_start(bbs, start_pos)
        skip_func_addrs = prepare_skip_funcs(skip_funcs)
        if (return_callstack):
            bbs, callstack = remove_excess(bbs, main_func=main_func, skip_func_addrs=skip_func_addrs, return_callstack = True)
        else:
            bbs = remove_excess(bbs, main_func=main_func, skip_func_addrs=skip_func_addrs)
        if bbs != None:
            progress("The trace now has %d blocks left." %len(bbs))
        else:
            os._exit(0)
        if (return_callstack):
            return bbs, callstack
        else:
            return bbs

    def do_sim(self,bb,state = None):
        '''
        Infer memory operations within each basic block through concrete execution
        '''
        def need_sim(vex):
            '''
            decides whether the block needs simulation
            '''
            for stmt in vex.statements:
                if isinstance(stmt,pyvex.stmt.Store):
                    return True
                elif isinstance(stmt,pyvex.stmt.StoreG):
                    return True
                elif isinstance(stmt,pyvex.stmt.LLSC):
                    return True
                elif isinstance(stmt,pyvex.stmt.LoadG):
                    return True
                elif isinstance(stmt,pyvex.stmt.WrTmp):
                    if isinstance(stmt.data,pyvex.expr.Load):
                        return True
                    elif isinstance(stmt.data,pyvex.expr.ITE):
                        return True
            return False
        def record_mem_write(state):
            state.globals['sim_results'].append(MemOp(state.inspect.instruction,'w',state.inspect.mem_write_address.args[0],state.inspect.mem_write_condition))
            if state.inspect.mem_write_address.args[0] not in state.globals['visited_addrs']:
                state.globals['visited_addrs'].append(state.inspect.mem_write_address.args[0])
        def record_mem_read(state):
            state.globals['sim_results'].append(MemOp(state.inspect.instruction,'r',state.inspect.mem_read_address.args[0],state.inspect.mem_read_condition))
        def handle_mem_read(state):
            addr_tbr = state.inspect.mem_read_address.args[0]
            if (addr_tbr not in state.globals['visited_addrs']) and (addr_tbr not in self.addr_range):
                cpu = self.panda.get_cpu()
                try:
                    data = self.panda.virtual_memory_read(cpu, addr_tbr, 4)
                except:
                    progress_warn("failed to fetch memory data from PANDA...")
                else:
                    state.mem[addr_tbr].uint32_t = data
                    state.globals['visited_addrs'].append(addr_tbr)
        def record_tmp_value(state):
            if isinstance(state.inspect.expr, pyvex.expr.RdTmp):
                irsb = state.inspect.address
                index = state.inspect.statement
                value = state.inspect.expr_result.args[0]
                state.globals['tmp_values'][(state.inspect.expr.tmp,irsb)] = TmpOp(index, irsb, value)
        # wait until PANDA is ready.
        self.cond.acquire()
        if self.get_rr_instr() not in self.stop_points:
            self.vm_cont()
            self.cond.wait()
        cpu = self.panda.get_cpu()
        # progress("pc:%x" %cpu.env_ptr.regs[15])
        if state == None:       
            state = self.angr_proj.factory.blank_state(options=angr.options.common_options,add_options={angr.options.ZERO_FILL_UNCONSTRAINED_MEMORY})
            state.inspect.b('mem_write',when=angr.BP_AFTER,action=record_mem_write)
            state.inspect.b('mem_read',when=angr.BP_BEFORE,action=handle_mem_read)
            state.inspect.b('mem_read',when=angr.BP_AFTER,action=record_mem_read)
            state.inspect.b('expr',when=angr.BP_AFTER,action=record_tmp_value)
            state.globals['sim_results'] = []
            state.globals['tmp_values'] = dict()
            state.globals['visited_addrs'] = []
        else:
            state = state.copy()
            state.globals['sim_results'] = []
            state.globals['tmp_values'] = dict()
            state.globals['visited_addrs'] = []
        state.regs.pc = state.solver.BVV(bb.state_bb.pc,32)
        state.regs.lr = state.solver.BVV(bb.state_bb.lr,32)
        state.regs.sp = state.solver.BVV(bb.state_bb.regs[13],32)
        state.regs.r0 = state.solver.BVV(bb.state_bb.regs[0],32)
        state.regs.r1 = state.solver.BVV(bb.state_bb.regs[1],32)
        state.regs.r2 = state.solver.BVV(bb.state_bb.regs[2],32)
        state.regs.r3 = state.solver.BVV(bb.state_bb.regs[3],32)
        state.regs.r4 = state.solver.BVV(bb.state_bb.regs[4],32)
        state.regs.r5 = state.solver.BVV(bb.state_bb.regs[5],32)
        state.regs.r6 = state.solver.BVV(bb.state_bb.regs[6],32)
        state.regs.r7 = state.solver.BVV(bb.state_bb.regs[7],32)
        state.regs.r8 = state.solver.BVV(bb.state_bb.regs[8],32)
        state.regs.r9 = state.solver.BVV(bb.state_bb.regs[9],32)
        state.regs.r10 = state.solver.BVV(bb.state_bb.regs[10],32)
        state.regs.r11 = state.solver.BVV(bb.state_bb.regs[11],32)
        state.regs.r12 = state.solver.BVV(bb.state_bb.regs[12],32)
        simgr = self.angr_proj.factory.simulation_manager(state)
        block = self.get_block(bb.state_bb.pc)
        if (need_sim(block.vex)):
            if block.size == bb.state_bb.size:
                simgr.step()
                if len(simgr.active) > 0:
                    state = simgr.active[0]
                else:
                    progress_warn("alert! cannot do simulation for this block!(no active state found)")
                    progress_warn("debug information:")
                    progress_warn(repr(simgr.stashes))
                    # get_input(caption="enter to continue...")
            else:
                simgr.step(num_inst=bb.state_bb.size)
                if len(simgr.active) > 0:
                    state = simgr.active[0]
                else:
                    progress_warn("\n[info] alert! cannot do simulation for this block!(no active state found)")
                    progress_warn("[info] debug information:")
                    progress_warn(repr(simgr.stashes))  
                    # get_input(caption="enter to continue...")       
            if state == None:
                get_input(caption="alert! empty state!")
        bb.simstate = state
        bb.mem_ops = state.globals['sim_results']
        bb.tmp_values = state.globals['tmp_values']
        self.cond.release()
        return bb,state

    def slice_suggestion(self, bbs):
        '''
        determines how to slice the replay.
        '''
        slices = []
        curr_slice_start = 0
        last_bb_end_pos = 0
        for bb in bbs:
            bb_start_pos = bb.state_bb.pos
            bb_end_pos = bb.state_ab.pos
            if (curr_slice_start == 0):
                curr_slice_start = bb_start_pos
                last_bb_end_pos = bb_end_pos
            elif bb_start_pos - curr_slice_start > 100000000:
                slices.append((curr_slice_start, last_bb_end_pos))
                curr_slice_start = bb_start_pos
            elif bbs.index(bb) == len(bbs) - 1:
                slices.append((curr_slice_start,bb_end_pos))
            last_bb_end_pos = bb_end_pos
        progress("replay slice suggestion:" + repr(slices))

    def pass_2(self, bbs, max_sim_count = 'max', always_use_panda = False, incomplete_callstack = False):
        '''
        performs concrete execution for the basic block. 
        max_sim_count: maximum number of blocks to be simulated
        always_use_panda: if set this to false, we won't use panda to infer function call information if the block is not being executed.
        incomplete_calltrace: don't infer function call/return information for not-simulated blocks. this results in problematic call stack. 
        '''
        def wait_until_ready(self):
            self.cond.acquire()
            if self.get_rr_instr() not in self.stop_points:
                self.vm_cont()
                self.cond.wait()
            return
        def update_callstack(self, bb):
            '''
            a much rougher method to update function call/return information.
            '''
            if bb.state_bb.pos == min(self.ret_info_queue): 
                self.ret_info_queue.remove(bb.state_bb.pos)
                self.program_state.new_return_from_state(self, bb.state_bb)
            if bb.state_ab.pos == min(self.call_info_queue):
                self.call_info_queue.remove(bb.state_ab.pos)
                self.program_state.new_call_from_state(self, bb.state_ab)
        if (max_sim_count == 'max'):
            sim_block_count = len(bbs)
        else:
            sim_block_count = max_sim_count
        sim_start_index = len(bbs) - 1 - sim_block_count
        if (self.vm_ready == False):
            sleep(10)
        state = None
        for i in tqdm(range(len(bbs))):
            bb = bbs[i]
            if (i > sim_start_index):
                self.stop_points.append(bb.pos)
                thread_sim = MyThread(self.do_sim, args=(bb,state))
                thread_sim.start()
                thread_sim.join()
                bb,state = thread_sim.get_result()
                if bb.mem_ops == None:
                    get_input("\n[!] alert! no valid state for this block!")
                bbs[i] = bb
            else:
                if incomplete_callstack == False:
                    if always_use_panda == True:
                        self.stop_points.append(bb.pos)
                        thread_wait = MyThread(wait_until_ready, args=(self,))
                        thread_wait.start()
                        thread_wait.join()
                    else:
                        update_callstack(self, bb)
                    pass
        print("")
        progress("concrete execution is done. killing PANDA...")
        self.panda.end_analysis()
        return bbs

    def gen_callgraph_simp(self, bbs):
        # now only works after pass_1
        depth = 0
        last_op = None
        for bb in bbs:
            if bb.state_bb.pos in self.ret_info_queue:
                ret = '%x' %bb.state_bb.regs[0]
                if last_op != 'call':
                    print("| "*(depth-1) + "+-- = %s" %ret)
                    depth -= 1
                else:
                    print(" = %s" %ret)
                    depth -= 1
                last_op = 'ret'
            if bb.state_ab.pos in self.call_info_queue:
                if last_op == 'call':
                    print('')
                last_op = 'call'
                state = bb.state_ab
                if state.pc in self.symbol_dict:
                    name = self.symbol_dict[state.pc].name
                else:
                    name = "sub_%x" %state.pc
                info = "%s(%x,%x,%x,%x)" %(name, state.regs[0],state.regs[1],state.regs[2],state.regs[3])
                if depth != 0:
                    print("| "*depth + "+--%s" %repr(info), end='')
                    depth += 1
                else:
                    print(repr(info), end='')
                    depth += 1

    def check_loop(self, bbs, window = 100):
        latest_calls_states = []
        for bb in bbs:
            if bb.state_bb.pos in self.call_info_queue:
                if bb.state_bb in latest_calls_states:
                    progress("loop detected: bb %d" %bbs.index(bb))
                    break
                else:
                    if len(latest_calls_states) < window:
                        latest_calls_states.append(bb.state_bb)
                    else:
                        del(latest_calls_states[0])
                        latest_calls_states.append(bb.state_bb)

    def gen_callgraph(self):
        progress_warn("note: you should only use this after pass_2.")
        self.program_state.gen_callgraph()

    def get_entry_state(self):
        return self.angr_proj.factory.entry_state(add_options={angr.options.ZERO_FILL_UNCONSTRAINED_MEMORY,angr.options.ZERO_FILL_UNCONSTRAINED_REGISTERS})

    def slice_block(self, bb, t_mems, t_regs, do_exit=False, find_source=False, debug=False):
        tainted_mems = t_mems
        tainted_regs = t_regs
        mem_ops = bb.mem_ops
        # tainted_tmps = []
        tainted_tmps = set()
        vex = bb.block.vex
        pos = len(vex.statements) - 1
        memop_pos = len(mem_ops) - 1
        memop_addrs = []
        for o in mem_ops:
            memop_addrs.append(o.pc)
        vex_slice = []
        current_pc = bb.block.instruction_addrs[-1]
        while(pos >= 0):
            stmt = vex.statements[pos]
            if isinstance(stmt,pyvex.IRStmt.IMark):
                current_pc -= 4
            # struct {
            #    IRExpr*    guard;  /* Conditional expression */
            #    IRConst*   dst;      /* Jump target (constant only) */
            #    IRJumpKind jk;    /* Jump kind */
            #    Int        offsIP;   /* Guest state offset for IP */
            #  } Exit;
            elif isinstance(stmt,pyvex.IRStmt.Exit):
                if do_exit:
                    vex_slice.append(stmt)
                    tainted_tmps.add(stmt.guard.tmp)
            #  struct {
            #    IREndness end; /* Endianness of the store */
            #    IRExpr*   addr;   /* store address */
            #    IRExpr*   data;   /* value to write */
            #  } Store;     
            elif isinstance(stmt,pyvex.IRStmt.Store):
                if memop_pos > -1:
                    addr = mem_ops[memop_pos].addr
                    # print("[debug] current memop_pos = %d" %memop_pos)
                    if addr in tainted_mems:
                        vex_slice.append(stmt)
                        tainted_mems.discard(addr)
                        if isinstance(stmt.data,pyvex.expr.RdTmp):
                            tainted_tmps.add(stmt.data.tmp)
                            if isinstance(stmt.addr,pyvex.expr.RdTmp):
                                tainted_tmps.discard(stmt.addr.tmp)
                        elif isinstance(stmt.data,pyvex.expr.Const):
                            if isinstance(stmt.addr,pyvex.expr.RdTmp):
                                tainted_tmps.discard(stmt.addr.tmp)
                            if find_source:
                                progress("tainted data relates to const value %d/0x%x" %(stmt.data.con.value,stmt.data.con.value))
                        else:
                            get_input(caption = "Store data: unsupported type: %s" %repr(type(stmt.data)))
                else:
                    get_input("angr cannot emulate this block! pc=%x" %bb.state_bb.pc)
                memop_pos -= 1
            # struct {
            #    IRStoreG* details;
            # } StoreG;
            # struct {
            #    IREndness end; /* Endianness of the store */
            #    IRExpr*   addr;   /* store address */
            #    IRExpr*   data;   /* value to write */
            #    IRExpr*   guard;  /* Guarding value */
            # }
            # IRStoreG;
            elif isinstance(stmt,pyvex.IRStmt.StoreG):
                if memop_pos > -1:
                    if current_pc in memop_addrs:
                        addr = mem_ops[memop_pos].addr
                        cond = mem_ops[memop_pos].cond
                        if addr in tainted_mems:
                            tainted_mems.discard(addr)
                            if isinstance(stmt.addr,pyvex.expr.RdTmp):
                                tainted_tmps.discard(stmt.addr.tmp)
                            else:
                                input("[!] StoreG addr: unsupported type: %s" %repr(type(stmt.addr)))
                            if isinstance(stmt.guard,pyvex.expr.RdTmp):
                                tainted_tmps.add(stmt.guard.tmp)
                            else:
                                input("[!] StoreG guard: unsupported type: %s" %repr(type(stmt.guard)))
                            if cond != None:
                                if cond.is_true() == True:
                                    if isinstance(stmt.data,pyvex.expr.RdTmp):
                                        tainted_tmps.add(stmt.data.tmp)
                                    else:
                                        get_input(caption = "[!] StoreG data: unsupported type: %s" %repr(type(stmt.data)))
                            else:
                                get_input(caption = "[!] incomplete memory operation, please try do execution for this block again with opt_level=0")
                        memop_pos -= 1
                elif current_pc not in memop_addrs:
                    tainted_tmps.discard(stmt.addr.tmp)
                else:
                    get_input(caption = "\n[!] incomplete memory operation, please try do execution for this block again with opt_level=0")
            #  struct {
            #    IRLoadG* details;
            #  } LoadG;
            # struct {
            #    IREndness end; /* Endianness of the load */
            #    IRLoadGOp cvt; /* Conversion to apply to the loaded value */
            #    IRTemp dst;    /* Destination (LHS) of assignment */
            #    IRExpr*   addr;   /* Address being loaded from */
            #    IRExpr*   alt; /* Value if load is not done. */
            #    IRExpr*   guard;  /* Guarding value */
            # }
            # IRLoadG;
            elif isinstance(stmt,pyvex.IRStmt.LoadG):
                if memop_pos > -1:
                    if stmt.dst in tainted_tmps:
                        vex_slice.append(stmt)
                        if isinstance(stmt.guard,pyvex.expr.RdTmp):
                            tainted_tmps.add(stmt.guard.tmp)
                        else:
                            get_input(caption = "LoadG guard: unsupported type: %s" %repr(type(stmt.guard)))
                        # trace memory: both the address itself and its value
                        # we need to know whether it **actually** read something from memory.
                        addr = mem_ops[memop_pos].addr
                        cond = mem_ops[memop_pos].cond
                        if cond != None:
                            if cond.is_true() == True:
                                tainted_mems.add(addr)
                                # 11.4:added this
                                if isinstance(stmt.addr,pyvex.expr.RdTmp):
                                    tainted_tmps.add(stmt.addr.tmp)
                                elif isinstance(stmt.addr,pyvex.expr.Const):
                                    pass
                                else:
                                    get_input(caption = '[!] LoadG addr: unsupported type: %s' %repr(type(stmt.addr)))
                            else:
                                if isinstance(stmt.alt,pyvex.expr.RdTmp):
                                    tainted_tmps.add(stmt.alt.tmp)
                                elif isinstance(stmt.addr,pyvex.expr.Const):
                                    pass
                                else:
                                    get_input(caption = "LoadG alt: unsupported type: %s" %repr(type(stmt.alt)))
                        else:
                            get_input(caption = "the cond of LoadG was not recorded. both the addr and alt will be tainted. this may result in overtaint.")
                            tainted_mems.add(addr)
                            if isinstance(stmt.addr,pyvex.expr.RdTmp):
                                tainted_tmps.add(stmt.addr.tmp)
                            elif isinstance(stmt.addr,pyvex.expr.Const):
                                pass
                            else:
                                get_input(caption = 'LoadG addr: unsupported type: %s' %repr(type(stmt.addr)))
                            if isinstance(stmt.alt,pyvex.expr.RdTmp):
                                tainted_tmps.add(stmt.alt.tmp)
                            elif isinstance(stmt.addr,pyvex.expr.Const):
                                pass
                            else:
                                get_input(caption = 'LoadG alt: unsupported type: %s' %repr(type(stmt.alt)))
                    memop_pos -= 1
                elif current_pc not in memop_addrs:
                    tainted_tmps.discard(stmt.dst)
                else:
                    get_input("incomplete memory operation... please try do execution for this block again with opt_level=0")
            #  struct {
            #    Int     offset;   /* Offset into the guest state */
            #    IRExpr* data;   /* The value to write */
            #  } Put;       
            elif isinstance(stmt,pyvex.IRStmt.Put):
                if stmt.offset in tainted_regs:
                    vex_slice.append(stmt)
                    if isinstance(stmt.data,pyvex.expr.RdTmp):
                        tainted_tmps.add(stmt.data.tmp)
                        tainted_regs.discard(stmt.offset)
                    elif isinstance(stmt.data,pyvex.expr.Const):
                        tainted_regs.discard(stmt.offset)
                        if find_source:
                            progress("tainted data relates to const value %d/0x%x" %(stmt.data.con.value,stmt.data.con.value))
                    else:
                        get_input("Put data: unsupported type:%s" %repr(type(stmt.data)))       
            # struct {
            #    IRTemp  tmp;   /* Temporary  (LHS of assignment) */
            #    IRExpr* data;  /* Expression (RHS of assignment) */
            # } WrTmp;    
            elif isinstance(stmt,pyvex.IRStmt.WrTmp):
                if stmt.tmp in tainted_tmps:
                    vex_slice.append(stmt)
                    if isinstance(stmt.data,pyvex.expr.Get):
                        tainted_regs.add(stmt.data.offset)
                    elif isinstance(stmt.data,pyvex.expr.RdTmp):
                        tainted_tmps.add(stmt.data.tmp)
                    elif isinstance(stmt.data,pyvex.expr.Const):
                        tainted_tmps.discard(stmt.tmp)
                        if find_source:
                            progress("tainted data relates to const value %d/0x%x" %(stmt.data.con.value,stmt.data.con.value))
                    elif isinstance(stmt.data,pyvex.expr.Load):
                        if memop_pos > -1:
                            addr = mem_ops[memop_pos].addr
                            tainted_mems.add(addr)
                            if isinstance(stmt.data.addr,pyvex.expr.RdTmp):
                                tainted_tmps.add(stmt.data.addr.tmp)
                            elif isinstance(stmt.data.addr, pyvex.expr.Const):
                                pass
                            else:
                                get_input(caption = "WrTmp Load addr: unsupported type:%s" %repr(type(stmt.data.addr)))
                            memop_pos -= 1
                        else:
                            get_input(caption = "incomplete memory operation... please re-execute this block with opt_level=0")
                    # struct {
                    #    IRExpr* cond;   /* Condition */
                    #    IRExpr* iftrue;   /* True expression */
                    #    IRExpr* iffalse;  /* False expression */
                    # } ITE
                    elif isinstance(stmt.data,pyvex.expr.ITE):
                        if isinstance(stmt.data.cond,pyvex.expr.RdTmp):
                            tainted_tmps.add(stmt.data.cond.tmp)
                        else:
                            get_input(caption = "WrTmp ITE cond: unsupported type:%s" %repr(type(stmt.data.cond)))
                        if (stmt.data.cond.tmp,bb.block.instruction_addrs[0]) in bb.tmp_values:
                            if bb.tmp_values[(stmt.data.cond.tmp,bb.block.instruction_addrs[0])].value == 1:
                                if isinstance(stmt.data.iftrue, pyvex.expr.RdTmp):
                                    tainted_tmps.add(stmt.data.iftrue.tmp)
                                elif isinstance(stmt.data.iftrue, pyvex.expr.Const):
                                    if find_source:
                                        progress("tainted data relates to const value: %d/0x%x" %(stmt.data.iftrue.con.value,stmt.data.iftrue.con.value))
                                else:
                                    get_input(caption = "WrTmp ITE iftrue: unsupported type:%s" %repr(type(stmt.data.iftrue)))
                            elif bb.tmp_values[(stmt.data.cond.tmp,bb.block.instruction_addrs[0])].value == 0:
                                if isinstance(stmt.data.iffalse,pyvex.expr.RdTmp):
                                    tainted_tmps.add(stmt.data.iffalse.tmp)
                                elif isinstance(stmt.data.iffalse, pyvex.expr.Const):
                                    if find_source:
                                        print("[!] tainted data relates to const value: %d/0x%x" %(stmt.data.iffalse.con.value,stmt.data.iffalse.con.value))
                                else:
                                    get_input(caption = "WrTmp ITE iffalse: unsupported type:%s" %repr(type(stmt.data.iffalse)))
                            else:
                                progress_err("ITE: value of condition: %s" %format(bb.tmp_values[(stmt.data.cond.tmp,bb.block.instruction_addrs[0])].value))
                                get_input(caption = "ITE: unexpected value of condition")
                        else:
                            print("tmp:%s, irsb:%s" %(format(stmt.data.cond.tmp), hex(bb.block.instruction_addrs[0])))
                            print("tmp_values: %s" %format(bb.tmp_values))
                            progress_warn("ITE: the condition value of this operation is not recorded.")
                            progress_warn("We're going to taint all tmps. Overtaint is inevitable...")
                            if isinstance(stmt.data.iftrue, pyvex.expr.RdTmp):
                                tainted_tmps.add(stmt.data.iftrue.tmp)
                            if isinstance(stmt.data.iffalse, pyvex.expr.RdTmp):
                                tainted_tmps.add(stmt.data.iffalse.tmp)
                    # struct {
                    #    IRCallee* cee; /* Function to call. */
                    #    IRType retty;  /* Type of return value. */
                    #    IRExpr**  args;   /* Vector of argument expressions. */
                    # }  CCall;
                    elif isinstance(stmt.data,pyvex.expr.CCall):
                        for e in stmt.data.args:
                            if isinstance(e,pyvex.expr.Const):
                                pass
                            elif isinstance(e,pyvex.expr.RdTmp):
                                tainted_tmps.add(e.tmp)
                    else:
                        for e in stmt.data.child_expressions:
                            if isinstance(e,pyvex.expr.RdTmp):
                                tainted_tmps.add(e.tmp)
                else:
                    if isinstance(stmt.data,pyvex.expr.Load):
                        if memop_pos > -1:
                            # print("[debug] current memop_pos = %d" %memop_pos)
                            memop_pos -= 1

            elif isinstance(stmt,pyvex.IRStmt.NoOp):
                pass
            elif isinstance(stmt,pyvex.IRStmt.AbiHint):
                pass
            else:
                get_input(caption = "unsupported stmt:%s" %repr(type(stmt)))
            pos -= 1
        if debug:
            for s in vex_slice:
                s.pp()
        return tainted_regs,tainted_mems

    def get_alt_ret(self,bb):
        '''
        generate a new return value that may could lead the execution to an another path.
        '''
        vex = bb.block.vex
        extra_targets = []
        ct = list(vex.constant_jump_targets)
        for c in ct:
            if (c != bb.state_ab.pc):
                extra_targets.append(c)
            if(len(extra_targets)==0):
                return None
            else:
                r0 = claripy.BVS('r0',32)
                state = self.angr_proj.factory.blank_state(addr = bb.state_bb.pc)
                state.regs.r0 = r0
                simgr = self.angr_proj.factory.simgr(state)
                simgr.explore(avoid = bb.state_ab.pc, find = extra_targets)
                alts = []
                if len(simgr.found) > 0:
                    for f in simgr.found:
                        expected = f.solver.eval(r0)
                        alts.append(expected)
                    return alts
                else:
                    return None

    def get_owner_by_funcname(self, funcname):
        owner = None
        for o in self.loader.all_elf_objects:
            if funcname in o.symbols_by_name:
                owner = o.binary_basename
                break
        return owner

    def gen_extra_record_cmd(self, retinfo):
        libname = self.get_owner_by_funcname(retinfo.name)
        progress("extra record arguments('extra_record', %s, %d ,%d, 'extra record for %s')" %(libname, retinfo.callinfo.pos, retinfo.pos, retinfo.name))

    def process(self, bbs):
        def check_flat(bb1, bb2):
            obj1 = self.loader.find_object_containing(bb1.state_ab.pc)
            obj2 = self.loader.find_object_containing(bb2.state_bb.pc)
            if obj1 == obj2:
                return True
            return False
        def gen_suggestion(retinfo, bb):
            # if we have any knowledge about this function or errno, generate solution according to this function/errno.
            if (retinfo.name in common_apis):
                errno = self.db.get_last_errno(bb.pos, bb.stackid)
                if errno != None:
                    solution = gen_solution_by_errno(self.workdir, errno, retinfo)
                else:
                    solution = gen_solution_by_func(self.workdir, retinfo)
            else:
                # if we don't have any knowledge about this function, try to find a alternative return value that may lead to an another execution path.
                alt_ret = self.get_alt_ret(bb)
                if alt_ret != None:
                    for r in alt_ret:
                        progress("force execution arguments: ('force_exec', %d, %d, 'before', 0, %d)" %(bb.state_bb.pc, bb.state_bb.lr, r))
                        solution = ForceExecTask(bb.state_bb.pc, bb.state_bb.lr, 'before', 0, r, None)
                else:
                    progress("you may try force execution arguments: ('force_exec', %d, %d, 'before', 0, <new_val>, None)" %(bb.state_bb.pc, bb.state_bb.lr))
                    # if we cannot find any vaild alternative return value, and this function comes from an unfamiliar library, record its execution.
                    # libname = self.loader.find_object_containing(retinfo.func).binary_basename
                    libname = self.get_owner_by_funcname(retinfo.name)
                    if (libname not in common_libs) and (libname != None):
                        progress("extra record arguments('extra_record', %s, %d, %d, 'extra record for %s')" %(libname, retinfo.callinfo.pos, retinfo.pos, retinfo.name))
                        solution = ExtraRecord(libname, retinfo.callinfo.pos, retinfo.pos, 'extra record for %s' %retinfo.name)
                    else:
                        solution = None  
            return solution
        tainted_mems = set()
        tainted_regs = set()
        index = len(bbs) - 1
        while (index > 0):
            progress("processing bb %d, tainted_regs = %s" %(index, repr(tainted_regs)))
            tainted_regs, tainted_mems = self.slice_block(bbs[index],tainted_mems,tainted_regs,do_exit=True,debug = False)
            if check_flat(bbs[index-1],bbs[index]):
                index -= 1
            else:
                if tainted_regs & {8, 12} != set():
                    progress("possible root cause found, tainted_regs = %s" %repr(tainted_regs))
                    result = -1
                    info = None
                    if bbs[index].state_bb.pos in self.program_state.retinfos:
                        info = self.program_state.retinfos[bbs[index].state_bb.pos]
                        for arg in info.callinfo.args:
                            # if we failed to infer argument data during PANDA execution, try to infer it again from angr memory model
                            if (arg.type == 'string') and (arg.data == None) and (bbs[index].simstate != None):
                                data = parse_val_legacy(self, bbs[index].simstate, arg.val)
                                arg.data = data
                        result = check_call(info.name, info.val)
                    if info == None:
                        index -= 1
                        return
                    if result == 0:
                        progress("%s" %repr(info))
                        progress("the retval looks legit, probably a FP")
                    elif result == 1:
                        progress("%s" %repr(info))
                        progress("we think this is the root cause...")
                        solution = gen_suggestion(info, bbs[index])
                        if solution != None:
                            progress(repr(solution.repr_data()))
                            self.candidate_solutions.append(solution)
                    else:
                        progress("%s" %repr(info))
                        progress("not sure about this retval...")
                        solution = gen_suggestion(info, bbs[index])
                        if solution != None:
                            progress(repr(solution.repr_data()))
                            self.candidate_solutions.append(solution)
                    if result == 0: 
                        tainted_regs = tainted_regs - {8, 12, 16, 20}
                        index -= 1
                    else:
                        # op = get_input("enter to continue searching, c to dig, q to quit...", timeout = 5)
                        # if op == 'q':
                        #     index = 0
                        #     return
                        # elif op != "c":
                        #     tainted_regs = tainted_regs - {8, 12, 16, 20}
                        #     index -= 1
                        # else:
                        #     return
                        return
                else:
                    progress("breakpoint here, just a notice!")
                    index -= 1
        progress("halted")

    def summary(self):
        if self.candidate_solutions != []:
            progress("Summary: generated solutions:")
            for s in self.candidate_solutions:
                print(s.repr_data())
        else:
            progress_err("Failed to generate any suggestion for this trace...")

def main():
    p = AnalysisProject(target_path='/usr/sbin/minhttpd', workdir = '/share/rpi/3412', load_fix = None)
    progress("done loading the project, now extract the trace from database...")
    bbs = p.generate_trace()
    progress("done loading the trace, now perform the first preprocess pass on the trace...")
    bbs = p.pass_1(bbs)
    if bbs != None:
        progress("now perform the second preprocess pass on the trace...")
        bbs = p.pass_2(bbs)
        p.process(bbs)
    p.summary()

if __name__ == "__main__":
    # main()
    target_path = None
    workdir = None
    fix = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], '-h-p:-w:-f:',['help','target_path','workdir','fix',])
    except:
        progress_err("usage: analysis.py -p <target_path> -w <workdir> -f <fix_description_file>")
        progress_err("crucial arguments: -p, -w")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            progress_err("usage: analysis.py -p <target_path> -w <workdir> -f <fix_description_file>")
            progress_err("crucial arguments: -p, -w")
            sys.exit(2)
        elif opt in ('-p','--target_path'):
            target_path = arg
        elif opt in ('-w','--workdir'):
            workdir = arg
        elif opt in ('-f','--fix'):
            fix = arg
    p = AnalysisProject(target_path=target_path, workdir = workdir, load_fix = fix)
    progress("done loading the project, now extract the trace from database...")
    bbs = p.generate_trace()
    progress("done loading the trace, now perform the first preprocess pass on the trace...")
    bbs = p.pass_1(bbs)
    # if bbs != None:
    if (bbs != None) and len(p.candidate_solutions)==0:
        progress("now perform the second preprocess pass on the trace...")
        bbs = p.pass_2(bbs)
        p.process(bbs)  
    p.summary()  
