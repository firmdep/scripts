#!/usr/bin/env python3

from urllib.request import Request, urlopen
import urllib.error
import os,sys,getopt
import pymysql

def service_avail():
    req = Request("http://127.0.0.1:10080")
    res = None
    try:
        res = urlopen(req)
    except urllib.error.HTTPError as e:
        print("[!] Error code: %d" %(e.code))
    except urllib.error.URLError as e:
        print("[!] Error Info:", e.reason)
    except ConnectionResetError as e:
        print("[!] Connection reset")
    if res != None:
        print("[+] Web OK")
        return True
    else:
        return False

def commit(server_id, fix, desc, works):
    print("result: %s" %works)
    print("fix: %s" %fix)
    print("desc: %s" %desc)
    db = pymysql.connect('192.168.3.48','root','firmware','firmdep')
    cursor = db.cursor()
    cursor.execute('select * from fixes order by id desc limit 1;')
    result = cursor.fetchall()
    if len(result) > 0:
        new_id = int(result[0][0]) + 1
    else:
        new_id = 1
    get_server = cursor.execute("select sign_call_20,sign_call_30,sign_call_50,sign_call_100,sign_20,sign_30,sign_50,sign_100 from signatures where server_id='%s'" %(repr(server_id)))
    if get_server == 0:
        print("invaild server id")
        return None
    result = cursor.fetchall()
    sign_call_20,sign_call_30,sign_call_50,sign_call_100,sign_20,sign_30,sign_50,sign_100 = result[0]
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id, sign_call_20, 'sign_call_20', fix, desc, works))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+1, sign_call_30, 'sign_call_30', fix, desc, works))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+2, sign_call_50, 'sign_call_50', fix, desc, works))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+3, sign_call_100, 'sign_call_100', fix, desc, works))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+4, sign_call_20, 'sign_call_20', fix, desc, works))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+5, sign_call_30, 'sign_call_30', fix, desc, works))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+6, sign_call_50, 'sign_call_50', fix, desc, works))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+7, sign_call_100, 'sign_call_100', fix, desc, works))
    db.commit()
    db.close()
    return new_id

def main(server_id, fix, desc):
    # result = 'y' if service_avail() else 'n'
    result = 'y'
    commit(server_id, fix, desc, result)

if __name__ == "__main__":
    server_id = None
    fix = ''
    desc = ''
    try:
        opts, args = getopt.getopt(sys.argv[1:], '-h-i:-f:-d:',['help','server_id','fix','desc'])
    except:
        sys.exit(1)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            progress_err("usage: check_conn.py -i <server_id> -f <fix_file> -d <description>")  
            sys.exit(2)
        elif opt in ('-i','--server_id'):
            server_id = int(arg)
        elif opt in ('-f','--fix'):
            fix = arg
        elif opt in ('-d','--desc'):
            desc = arg
    if server_id != None:
        main(server_id, fix, desc)
