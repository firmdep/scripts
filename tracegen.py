#!/usr/bin/python3
# this script doesn't work with pypy3 well, don't know why yet.
from base_class import *
import getopt

class ExtraRecordRange(object):
    def __init__(self, proj, extra_record_task):
        self.start = extra_record_task.start
        self.end = extra_record_task.end
        if self.end == None:
            self.end = 1000000000000
        self.pos_range = range(self.start, self.end)
        self.addr_range = proj.determine_range(proj.extra_libs + [extra_record_task.lib])
    def in_range(self, pos, addr):
        if (pos in self.pos_range) and (addr in self.addr_range):
            return True
        else:
            return False

class TraceProject(BaseProject):

    def __init__(self, target_path, workdir, target_bin=None, arch='armel', load_fix = None):
        BaseProject.__init__(self, target_path, workdir, target_bin = target_bin, arch = arch, init_panda = True, init_cle = True, init_db = True, load_fix = load_fix)
        self.tasks = {}
        self.last_stackid = None
        self.last_bb_state = None
        self.target_stackids = []
        self.do_ab = False
        self.extra_record_tasks = []
        for t in self.fixes.extra_record_tasks:
            new_task = ExtraRecordRange(self, t)
            self.extra_record_tasks.append(new_task)
        self.last_checkpoint = 0

    def should_record_block(self, pc, cpu):
        # if (pc not in self.addr_range):
        #     return False
        # if self.panda.in_kernel(cpu):
        #     return False 
        # if self.extra_record_tasks != []:
        #     pos = self.get_rr_instr()
        #     for t in self.extra_record_tasks:
        #         if t.in_range(pos, pc):
        #             return True
        # return True
        if pc in self.addr_range:
            return True
        if self.panda.in_kernel(cpu):
            return False
        if self.extra_record_tasks != []:
            pos = self.get_rr_instr()
            for t in self.extra_record_tasks:
                if t.in_range(pos, pc):
                    return True
        return False

    def update_stackids(self, stackid, cpu):
        if (stackid in self.target_stackids):
            self.last_stackid = stackid
            return
        else:
            rr_instr = self.get_rr_instr()
            if (self.last_stackid == None):
                self.db.update_task(rr_instr, stackid, stackid)
            else:
                self.db.update_task(rr_instr, stackid, self.last_stackid)
            self.target_stackids.append(stackid)
            self.last_stackid = stackid
            # update fd
            proc = self.panda.plugins['osi'].get_current_process(cpu)
            fname_ptr_0 = self.panda.plugins['osi_linux'].osi_linux_fd_to_filename(cpu, proc, 0)
            fname_ptr_1 = self.panda.plugins['osi_linux'].osi_linux_fd_to_filename(cpu, proc, 1)
            fname_ptr_2 = self.panda.plugins['osi_linux'].osi_linux_fd_to_filename(cpu, proc, 2)
            fname_0 = ffi.string(fname_ptr_0).decode("utf8","ignore") if fname_ptr_0 != ffi.NULL else "<unknown>"
            fname_1 = ffi.string(fname_ptr_1).decode("utf8","ignore") if fname_ptr_1 != ffi.NULL else "<unknown>"
            fname_2 = ffi.string(fname_ptr_2).decode("utf8","ignore") if fname_ptr_2 != ffi.NULL else "<unknown>"
            self.db.update_fd(rr_instr, stackid, 'open', fname_0, 0, 0, 'initial fd')
            self.db.update_fd(rr_instr, stackid, 'open', fname_1, 1, 1, 'initial fd')
            self.db.update_fd(rr_instr, stackid, 'open', fname_2, 2, 2, 'initial fd')
            
    def load_callback(self):
        self.panda.disable_tb_chaining()
        self.panda.enable_precise_pc()
        @self.panda.cb_before_block_exec(name="before", procname = self.target_bin)
        def bbe(cpu, tb):
            pc = cpu.env_ptr.regs[15]
            if (self.should_record_block(pc,cpu) == True):
                for t in self.fixes.force_exec_tasks:
                    if t.matches(cpu.env_ptr.regs[15], cpu.env_ptr.regs[14], 'before'):
                        progress("performing force execution...")
                        t.do_fix(self.panda)    
                stackid = self.get_stackid(cpu)
                self.update_stackids(stackid, cpu)
                regs = cpu.env_ptr.regs
                size = tb.size
                pos = self.get_rr_instr()
                if pos >= self.last_checkpoint + 100000:
                    progress("executed %d instructions..." %pos)
                    self.last_checkpoint = pos
                bb_state = State('before',pos,stackid,tb.pc,regs,size,0)
                self.db.commit_state(bb_state)
                self.panda.enable_callback("after")
                self.do_ab = True
    
        @self.panda.cb_after_block_exec(name="after", enabled=False, procname = self.target_bin)
        def abe(cpu,tb,exitcode):
            if (self.do_ab):
                for t in self.fixes.force_exec_tasks:
                    if t.matches(cpu.env_ptr.regs[15], cpu.env_ptr.regs[14], 'after'):
                        progress("performing force execution...")
                        t.do_fix(self.panda)
                pc = cpu.env_ptr.regs[15]
                stackid = self.get_stackid(cpu)
                regs = cpu.env_ptr.regs
                ab_state = State('after',self.get_rr_instr(),stackid,pc,regs,tb.size,exitcode)
                self.db.commit_state(ab_state)
                self.panda.disable_callback("after")
                self.do_ab = False
            regs = cpu.env_ptr.regs
            if (regs[0] == 0xdeadbeef) and (regs[1] == 0xdeadbeef) and (regs[2] == 0xdeadbeef):
                stackid = self.get_stackid(cpu)
                self.db.commit_errno(self.get_rr_instr(), stackid, regs[3])

        # the followings are file descriptor related syscalls
        @self.panda.ppp("syscalls2", "on_sys_open_return")
        def sys_open(cpu, pc, filename, flags, mode):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            name = self.parse_val(cpu, filename)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'open', name, ret, ret, format((name, flags, mode)))
            else:
                self.db.update_fd(rr_instr, stackid, 'open', name, None, ret, format((name, flags, mode)))

        @self.panda.ppp("syscalls2", "on_sys_openat_return")
        def sys_openat(cpu, pc, dirfd, filename, flags, mode):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            name = self.parse_val(cpu, filename)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'openat', format((dirfd, name)), ret, ret, format((dirfd, name, flags, mode)))
            else:
                self.db.update_fd(rr_instr, stackid, 'openat', format((dirfd, name)), None, ret, format((dirfd, name, flags, mode)))
        
        @self.panda.ppp("syscalls2", "on_sys_dup_return")
        def sys_dup(cpu, pc, fd):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'dup', fd, ret, ret, '%d->%d' %(fd, ret))
            else:
                self.db.update_fd(rr_instr, stackid, 'dup', fd, None, ret, '')

        @self.panda.ppp("syscalls2", "on_sys_dup2_return")
        def sys_dup2(cpu, pc, oldfd, newfd):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'dup2', oldfd, newfd, ret, '%d->%d' %(oldfd, newfd))
            else:
                self.db.update_fd(rr_instr, stackid, 'dup2', oldfd, newfd, ret, '')

        @self.panda.ppp("syscalls2", "on_sys_dup3_return")
        def sys_dup3(cpu, pc, oldfd, newfd, flags):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'dup3', oldfd, newfd, ret, '%d->%d' %(oldfd, newfd))
            else:
                self.db.update_fd(rr_instr, stackid, 'dup3', oldfd, newfd, ret, '')

        @self.panda.ppp("syscalls2", "on_sys_creat_return")
        def sys_creat(cpu, pc, filename, mode):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            name = self.parse_val(cpu, filename)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'creat', name, ret, ret, format((name, mode)))
            else:
                self.db.update_fd(rr_instr, stackid, 'creat', name, None, ret, format((name, mode)))

        @self.panda.ppp("syscalls2", "on_sys_close_return")
        def sys_close(cpu, pc, fd):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'close', fd, None, ret, fd)
            else:
                self.db.update_fd(rr_instr, stackid, 'close', fd, None, ret, fd)
        
        @self.panda.ppp("syscalls2", "on_sys_socket_return")
        def sys_socket(cpu, pc, domain, type, protocol):
            # todo: detained new socket information
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'socket', '<new socket>', ret, ret, format((domain,type,protocol)))
            else:
                self.db.update_fd(rr_instr, stackid, 'socket', '<new socket>', None, ret, '')
        
        @self.panda.ppp("syscalls2", "on_sys_connect_return")
        def sys_connect(cpu, pc, fd, addr, addrlen):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            addr_arg = Arg('sockaddr', addr, panda=self.panda, cpu=cpu)
            addr_repr = repr(addr_arg)
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'connect', addr_repr, fd, ret, "%d->%s" %(fd, addr_repr))
            else:
                self.db.update_fd(rr_instr, stackid, 'connect', addr_repr, fd, ret, '')

        @self.panda.ppp("syscalls2", "on_sys_bind_return")
        def sys_bind(cpu, pc, fd, addr, addrlen):
            rr_instr = self.get_rr_instr()
            stackid = self.get_stackid(cpu)
            ret = hex_to_int(cpu.env_ptr.regs[0])
            addr_arg = Arg('sockaddr', addr, panda=self.panda, cpu=cpu)
            addr_repr = repr(addr_arg)
            if (ret >= 0):
                self.db.update_fd(rr_instr, stackid, 'bind', addr_repr, fd, ret, "%d->%s" %(fd, addr_repr))
            else:
                self.db.update_fd(rr_instr, stackid, 'bind', addr_repr, fd, ret, '')

    def run(self,replay=None):
        self.load_callback()
        if replay == None:
            self.panda.run_replay(self.target_bin)
        else:
            self.panda.run_replay(replay)


def main():
    p = TraceProject(target_path='/usr/sbin/httpds', workdir = '/share/rpi/342', load_fix = None)
    p.run()
    p.db.summary(pp=True)
    p.db.save()

if __name__ == "__main__":
    # main()
    target_path = None
    workdir = None
    fix = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], '-h-p:-w:-f:',['help','target_path','workdir','fix',])
    except:
        progress_err("usage: tracegen.py -p <target_path> -w <workdir> -f <fix_description_file>")
        progress_err("crucial arguments: -p, -w")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            progress_err("usage: tracegen.py -p <target_path> -w <workdir> -f <fix_description_file>")
            progress_err("crucial arguments: -p, -w")
            sys.exit(2)
        elif opt in ('-p','--target_path'):
            target_path = arg
        elif opt in ('-w','--workdir'):
            workdir = arg
        elif opt in ('-f','--fix'):
            fix = arg
    p = TraceProject(target_path = target_path, workdir = workdir, load_fix = fix)
    p.run()
    p.db.summary(pp=True)
    p.db.save()
