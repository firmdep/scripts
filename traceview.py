#!/usr/bin/env python3

import analysis as a
import os,sys,getopt
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from api_identity import parse_val_legacy

class ProjLoaderWorker(QThread):

    result = pyqtSignal(a.AnalysisProject)
    msg = pyqtSignal(str)

    def __init__(self, target_path = None, workdir = None, load_fix = None):
        super().__init__()
        self.target_path = target_path
        self.workdir = workdir
        self.load_fix = load_fix
    
    def run(self):
        self.msg.emit("Loading...")
        proj = a.AnalysisProject(target_path = self.target_path, workdir = self.workdir, load_fix = self.load_fix, init_panda = False)
        self.result.emit(proj)
        self.msg.emit("Done loading the project.")

class TraceLoaderWorker(QThread):

    result = pyqtSignal(list)
    msg = pyqtSignal(str)
    info = pyqtSignal(str)

    def __init__(self, proj):
        super().__init__()
        self.proj = proj
    
    def run(self):
        self.msg.emit("Extracting trace from database...")
        bbs = self.proj.generate_trace()
        self.msg.emit("Preprocessing(pass 1)...")
        bbs, callstack = self.proj.pass_1(bbs, return_callstack = True)
        callstack_repr = ""
        for c in callstack:
            if c in self.proj.symbol_dict:
                callstack_repr = callstack_repr + self.proj.symbol_dict[c].name + '\n'
            else:
                callstack_repr = callstack_repr + "sub_%x" %c + "\n"
        self.result.emit(bbs)
        if (len(bbs)<10000):
            self.info.emit("Done loading the trace (%d calls, %d blocks)\ncallstack:\n%s" %(len(self.proj.call_info_queue), len(bbs), callstack_repr))
        else:
            self.info.emit("The trace is very long (%d calls, %d blocks), try check loop!\ncallstack:\n%s" %(len(self.proj.call_info_queue), len(bbs), callstack_repr))
        self.msg.emit("Preprocessed (%d calls, %d blocks)" %(len(self.proj.call_info_queue), len(bbs)))

class CallInfo(object):
    
    def __init__(self, bbinfo, model_index):
        self.model_index = model_index
        self.begin_index = bbinfo.index
        self.end_index = None
        self.info = bbinfo.info
        self.before_state = bbinfo.call_state
        self.after_state = None
        self.retval = None

    def update(self, bbinfo, is_ret = False):
        self.end_index = bbinfo.index
        if (is_ret):
            self.after_state = bbinfo.ret_state
            self.retval = bbinfo.retval

    def get_range(self):
        if (self.end_index):
            return range(self.begin_index, self.end_index+1)
        else:
            return range(self.begin_index, self,begin_index+1)

class BBInfo(object):

    def __init__(self, index, depth, info = None, retval = None, call_state = None, ret_state = None):
        self.index = index
        self.depth = depth # depth of call stack
        self.info = info
        self.call_state = None
        self.ret_state = None
        self.retval = retval
    
class CheckLoopWorker(QThread):

    msg = pyqtSignal(str)

    def __init__(self, proj, bbs):
        super().__init__()
        self.proj = proj
        self.bbs = bbs
    
    def run(self):
        last_call_state = None
        last_call_bb_index = None
        last_repeat_index = None
        self.msg.emit("Begin scan...")
        for bb in self.bbs[-1:-500:-1]:
            if bb.state_bb.pos == self.proj.call_info_queue[-1]:
                self.msg.emit("Finding last loop...")
                last_call_state = bb.state_bb
                last_call_bb_index = self.bbs.index(bb)
        if (last_call_state):
            for bb in self.bbs[last_call_bb_index-1::-1]:
                if bb.state_bb.pos in self.proj.call_info_queue:
                    if (bb.state_bb == last_call_state):
                        self.msg.emit("Loop detected: at index %d/%d, now find the first loop..." %(self.bbs.index(bb),len(self.bbs)))
                        last_repeat_index = self.bbs.index(bb)
                        break
            if (last_repeat_index):
                for bb in self.bbs[:last_repeat_index]:
                    if (bb.state_bb == last_call_state):
                        self.msg.emit("First loop found, block index %d/%d" %(self.bbs.index(bb),len(self.bbs)))
                        break
            else:
                self.msg.emit("Loop not found")
        else:
                self.msg.emit("Loop not found")

class CallTraceGenWorker(QThread):

    info = pyqtSignal(BBInfo)
    msg = pyqtSignal(str)

    def __init__(self, proj, bbs):
        super().__init__()
        self.proj = proj
        self.bbs = bbs

    def run(self):
        depth = 0
        last_op = None
        total = len(self.bbs)
        call_info_queue = self.proj.call_info_queue
        ret_info_queue = self.proj.ret_info_queue
        for bb in self.bbs:
            self.msg.emit("Processing %d/%d" %(self.bbs.index(bb),total))
            if bb.state_bb.pos in ret_info_queue:
                ret = '%x' %bb.state_bb.regs[0]
                if last_op != 'call':
                    self.info.emit(BBInfo(self.bbs.index(bb), depth = depth-1, info = None, retval = ret, ret_state = bb.state_bb))
                else:
                    self.info.emit(BBInfo(self.bbs.index(bb), depth = depth, info = None, retval = ret, ret_state = bb.state_bb))
                depth -= 1
                last_op = 'ret'
                ret_info_queue.remove(bb.state_bb.pos)
            if bb.state_ab.pos in call_info_queue:
                last_op = 'call'
                state = bb.state_ab
                if state.pc in self.proj.symbol_dict:
                    name = self.proj.symbol_dict[state.pc].name
                else:
                    name = "sub_%x" %state.pc
                info = "%s(%x,%x,%x,%x)" %(name, state.regs[0],state.regs[1],state.regs[2],state.regs[3])
                self.info.emit(BBInfo(self.bbs.index(bb), depth = depth, info = info, call_state = bb.state_ab))
                depth += 1
                call_info_queue.remove(bb.state_ab.pos)
        self.msg.emit("Done.")

class TraceViewProject(QtWidgets.QDialog):
    
    def __init__(self, target_path = None, workdir = None, fix = None):
        super().__init__()
        
        self.target_path = target_path
        self.workdir = workdir
        self.fix = fix
        self.analysis = None
        self.bb_trace = None
        self.model_to_call = dict()
        self.callstack = []
        self.memory = None

        uic.loadUi('main.ui', self)
        self.model = QStandardItemModel()
        self.trace_view.setModel(self.model)
        self.model.setHorizontalHeaderLabels(["Call trace"])
        self.trace_view.clicked.connect(self.on_item_clicked)
        self.load_button.clicked.connect(self.load_proj)
        self.preprocess_button.clicked.connect(self.do_preprocess)
        self.gen_calltrace_button.clicked.connect(self.gen_calltrace)
        self.check_loop_button.clicked.connect(self.check_loop)
        self.show()

    def on_msg(self, result):
        self.status_bar.setText(result)

    def on_proj(self, result):
        self.analysis = result
        self.memory = self.analysis.get_entry_state()
    
    def on_bb_trace(self, result):
        self.bb_trace = result

    def load_proj(self):
        self.worker = ProjLoaderWorker(target_path = self.target_path, workdir = self.workdir, load_fix = self.fix)
        self.worker.result.connect(self.on_proj)
        self.worker.msg.connect(self.on_msg)
        self.worker.start()

    def do_preprocess(self):
        self.worker = TraceLoaderWorker(self.analysis)
        self.worker.result.connect(self.on_bb_trace)
        self.worker.msg.connect(self.on_msg)
        self.worker.info.connect(self.on_preprocess_result)
        self.worker.start()        

    def check_loop(self):
        self.worker = CheckLoopWorker(self.analysis, self.bb_trace)
        self.worker.msg.connect(self.on_msg)
        self.worker.start()

    def gen_calltrace(self):
        self.worker = CallTraceGenWorker(self.analysis, self.bb_trace)
        self.worker.msg.connect(self.on_msg)
        self.worker.info.connect(self.on_bbinfo)
        self.worker.start()

    def trace_view_add(self, item):
        root_item = self.model.invisibleRootItem()
        if (item.info):
            new_item = QStandardItem(item.info)
            if (len(self.callstack) > 0):
                parent_index = self.callstack[-1]
                parent_item = self.model.itemFromIndex(parent_index)
                parent_item.appendRow(new_item)
            else:
                root_item.appendRow(new_item)
            new_item_index = self.model.indexFromItem(new_item)
            new_call = CallInfo(item, new_item_index)
            for call_index in self.callstack:
                self.model_to_call[call_index].update(item)
            self.callstack.append(new_item_index)
            self.model_to_call[new_item_index] = new_call

        if (item.retval):
            if len(self.callstack)!=0:
                current_item_index = self.callstack[-1]
                current_item = self.model.itemFromIndex(current_item_index)
                orig_desc = current_item.text()
                self.model.setData(current_item_index, orig_desc + " = %s" %item.retval)
                self.model_to_call[current_item_index].update(item, is_ret = True)
                self.callstack.pop()
                for c in self.callstack:
                    self.model_to_call[c].update(item)

    def on_bbinfo(self, bbinfo):
        self.trace_view_add(bbinfo)

    def on_info(self, info):
        self.info_text.setText(info)

    def on_preprocess_result(self, result):
        dialog = QtWidgets.QDialog()
        dialog.setWindowTitle("Result")
        label = QtWidgets.QLabel(result)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(label)
        ok_button = QtWidgets.QPushButton("OK")
        ok_button.clicked.connect(dialog.accept)
        layout.addWidget(ok_button)
        dialog.setLayout(layout)
        dialog.exec_()
    
    def on_item_clicked(self, index):
        callinfo = self.model_to_call[index]
        if (callinfo.retval):
            self.info_text.setText("begin:%d\nend:%d" %(callinfo.begin_index, callinfo.end_index))
        else:
            if (callinfo.end_index):
                self.info_text.setText("begin:%d\nnot returned\nlast: >= %d" %(callinfo.begin_index, callinfo.end_index))
            else:
                self.info_text.setText("begin:%d\nnot returned" %(callinfo.begin_index))

if __name__ == '__main__':
    target_path = None
    workdir = None
    fix = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], '-h-p:-w:-f:',['help','target_path','workdir','fix',])
    except:
        progress_err("usage: traceview.py -p <target_path> -w <workdir> -f <fix_description_file>")
        progress_err("crucial arguments: -p, -w")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            progress_err("usage: traceview.py -p <target_path> -w <workdir> -f <fix_description_file>")
            progress_err("crucial arguments: -p, -w")
            sys.exit(2)
        elif opt in ('-p','--target_path'):
            target_path = arg
        elif opt in ('-w','--workdir'):
            workdir = arg
        elif opt in ('-f','--fix'):
            fix = arg
    app = QtWidgets.QApplication(sys.argv)
    ex = TraceViewProject(target_path = target_path, workdir = workdir, fix = fix)
    sys.exit(app.exec_())