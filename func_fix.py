#!/usr/bin/env python3
from base_class import *
import sys,getopt,os
import hashlib
import pymysql
from tqdm import tqdm
from fixes import force_exec_funcs
from api_identity import preprocess_skip_funcs


class FixGenProject(BaseProject):

    def __init__(self, target_path, workdir, target_bin=None, arch='armel', load_fix = None):
        BaseProject.__init__(self, target_path, workdir, target_bin = target_bin, arch = arch, init_panda = False, init_cle = True, init_db = True, load_symbol = True, load_fix = load_fix)
        self.db = Database(self.workdir, self.target_bin, db_source=(os.path.join(self.workdir, self.target_bin)+'.db'))

    def get_block(self, addr, size=None):
        if size:
            b = self.angr_proj.factory.block(addr, size)
        else:
            b = self.angr_proj.factory.block(addr)
        return b

    def generate_trace(self):

        def get_raw_trace():
            summaries = list(self.db.summary().values())
            progress("The database contains execution traces of the following processes/threads:")
            preferred = None
            max_dur = 0
            for task in summaries:
                print("%d: %s\t start:%d end:%d" %(summaries.index(task), task.stackid, task.start_pos, task.end_pos))
                overall_start, overall_end = task.get_whole_execution()
                dur = overall_end - overall_start
                if dur > max_dur:
                    preferred = summaries.index(task)
                    max_dur = dur
            # choice = input("task to analyze (suggested:%d) :" %preferred) or preferred
            return self.db.fetch_events_by_stackid(summaries[preferred].stackid, type='state')

        def parse_trace(raw_states):
            #  fetch vmstates from database 
            states = []
            for r in raw_states:
                new_s = State(('before' if r[2]==0 else 'after'), r[1], r[3],int(r[5]), r[8:-1], r[4], r[-1])
                if (states == []) and (new_s.type == 'after'):
                    pass
                else:
                    states.append(new_s)
                new_s = None
                del new_s
            if states[-1].type == 'before':
                states.pop()
            progress("done extracting states from database...")
            # fetch basic blocks according to vmstates
            bbs = []
            index = 0
            total = len(states) // 2
            while (index < total):
                state_bb = states[2 * index]
                state_ab = states[2 * index + 1]
                if (state_bb == state_ab):
                    index += 1
                    continue
                elif (state_ab.exitcode >1):
                    index += 1
                    continue
                block = self.get_block(state_bb.pc, size=state_bb.size)
                if state_ab.pc == 0xffff10a0: # the block did not done executing
                    progress_warn("%x: this block did not done executing..." %state_bb.pc)
                    index += 1
                    state_bb_middle = states[2 * index]
                    state_bb.size = state_bb.size - 4 + state_bb_middle.size
                    block = self.get_block(state_bb.pc, size = state_bb.size)
                    state_ab = states[2 * index + 1]
                new_block = BasicBlock(state_bb, state_ab, block)
                bbs.append(new_block)
                index += 1
            progress("done generating basic blocks according to vmstates...")
            return bbs

        raw = get_raw_trace()
        bbs = parse_trace(raw)
        progress("The trace contains %d blocks." %len(bbs))
        return bbs

    def pass_1(self, bbs, start_pos=None, main_func = None, skip_funcs = None):
        def prepare_skip_funcs(skip_funcs):
            skip_func_addrs = []
            skip_func_list = preprocess_skip_funcs
            for f in skip_func_list:
                sym = self.loader.find_symbol(f)
                if sym != None:
                    progress("The execution of %s() will be skipped." %sym.name)
                    skip_func_addrs.append(sym.rebased_addr)
            if skip_funcs != None:
                skip_func_addrs = skip_func_addrs + skip_funcs
            progress("skip funcs: %s" %repr(skip_func_addrs))
            return skip_func_addrs

        def find_start(bbs, start_pos):
            if (start_pos == None):
                return bbs
            else:
                for bb in bbs:
                    if (bb.pos >= start_pos):
                        new_bbs = bbs[bbs.index(bb):]
                        return new_bbs
                return []

        def remove_excess(bbs, main_func = None, skip_func_addrs = None):
            '''
            remove blocks of exported subroutines called by not-recorded functions.
            '''
            index = 0
            # begin_addr = 0x0
            new_bbs = []
            functions = []
            callstack = []
            last_plt_entry = None
            skip_end = None
            # while(index <= len(bbs) - 1):
            for index in tqdm(range(len(bbs))):
                bb = bbs[index]
                # if bb.exitcode > 1:
                #   index += 1
                #   continue
                if skip_end != None and bb.state_bb.pc != skip_end:
                    index += 1
                    continue
                else:
                    skip_end = None
                if len(callstack) == 0:
                    new_bbs.append(bb)
                elif (main_func != None) and (bb.state_bb.pc == main_func):
                    new_bbs.append(bb)
                elif (bb.state_bb.pc == new_bbs[-1].state_ab.pc):
                    new_bbs.append(bb)
                elif (new_bbs[-1].state_ab.pc == 0xffff10a0):
                    if bb.state_bb.pc in new_bbs[-1].block.instruction_addrs:
                        new_bbs[-1].state_ab = bb.state_ab
                        index += 1
                        continue
                    else:
                        new_bbs.append(bb)
                else:
                    if bb.state_bb.pc == callstack[-1]:
                        new_bbs.append(bb)
                    elif bb.state_bb.pc in self.symbol_dict:
                        if (last_plt_entry != None) and (self.symbol_dict[bb.state_bb.pc] == last_plt_entry):
                            new_bbs.append(bb)
                    else:
                        index += 1
                        continue
                if skip_func_addrs != None and bb.state_ab.pc in skip_func_addrs:
                    skip_end = bb.state_ab.lr
                # handle callstack
                if len(callstack) == 0:
                    pass
                elif bb.state_bb.pc == callstack[-1]:
                    functions.pop()
                    callstack.pop()
                # check vex
                vex = bbs[index].block.vex
                if vex.jumpkind == "Ijk_Call":
                    callstack.append(bbs[index].state_ab.lr)
                    functions.append(bbs[index].state_ab.pc)
                elif vex.jumpkind == "Ijk_Sys_syscall":
                    callstack.append(bbs[index].state_ab.lr)
                    functions.append(bbs[index].state_ab.pc)
                elif bbs[index].state_ab.pc > 0xff000000:
                    if bbs[index].state_ab.pc in kuser:
                        callstack.append(bbs[index].state_ab.lr)
                        functions.append(bbs[index].state_ab.pc)
                    elif bbs[index].state_ab.pc == 0xffff10a0:
                        progress_warn("bb %d: have a look at this!" %index)
                out = bbs[index].state_ab.pc
                if self.loader.find_section_containing(out) != None:
                    if (self.loader.find_section_containing(out).name == '.plt'):
                        if (out in self.symbol_dict) and (self.symbol_dict[out].name != '_dl_linux_resolve') and (self.symbol_dict[out].name != '_dl_runtime_resolve') :
                            last_plt_entry = self.symbol_dict[out]
                index += 1
            return new_bbs
        bbs = find_start(bbs, start_pos)
        skip_func_addrs = prepare_skip_funcs(skip_funcs)
        bbs = remove_excess(bbs, main_func=main_func, skip_func_addrs=skip_func_addrs)
        if bbs != None:
            progress("The trace now has %d blocks left." %len(bbs))
        else:
            os._exit(0)
        return bbs

    def find_force_exec_funcs(self, bbs):
        funcs = []
        for f in force_exec_funcs.keys():
            sym = self.loader.find_symbol(f)
            if sym != None:
                funcs.append(sym)
        if funcs == []:
            progress_err("Corresponding symbol of the function not found")
        else:
            progress("Found symbol, looking for the calls...")
        return funcs

    def fix_func(self,bbs,sym,max=1):
        solutions = []
        progress("function to be fixed: %s" %sym.name)
        addr = sym.rebased_addr
        ret_addrs = set()
        for bb in bbs:
            if bb.state_ab.pc == addr:
                lr = bb.state_ab.lr
                if lr in ret_addrs:
                    continue
                else:
                    ret_addrs.add(lr)
        old_ret, new_ret = force_exec_funcs[sym.name]
        for bb in bbs:
            if isinstance(old_ret, int):
                if (bb.state_bb.pc in ret_addrs) and (bb.state_bb.regs[0] == old_ret):
                    progress("force execution arguments: ('force_exec', %d, %d, 'before', %d, %d)" %(bb.state_bb.pc, bb.state_bb.lr, old_ret, new_ret))
                    solutions.append(ForceExecTask(bb.state_bb.pc, bb.state_bb.lr, 'before', old_ret, new_ret, None))
                    if len(solutions) >= max:
                        break
            elif isinstance(old_ret, list):
                if (bb.state_bb.pc in ret_addrs) and (bb.state_bb.regs[0] in old_ret):
                    progress("force execution arguments: ('force_exec', %d, %d, 'before', %d, %d)" %(bb.state_bb.pc, bb.state_bb.lr, bb.state_bb.regs[0], new_ret))
                    solutions.append(ForceExecTask(bb.state_bb.pc, bb.state_bb.lr, 'before', bb.state_bb.regs[0], new_ret, None))
                    if len(solutions) >= max:
                        break                
        return solutions

    def process(self, server_id):
        bbs = self.generate_trace()
        bbs = self.pass_1(bbs)
        candidate_funcs = self.find_force_exec_funcs(bbs)
        solutions = []
        if candidate_funcs != []:
            for f in candidate_funcs:
                new_solutions = self.fix_func(bbs, f)
                solutions = solutions + new_solutions
        if server_id != None:
            f_name = "/share/fix/fix_%d.txt" %server_id
            progress("outputting fix to %s" %f_name)
            f = open(f_name, 'a+')
            for s in solutions:
                f.write(format(s.repr_data()) + '\n')
            f.close()
            # commit(server_id, f_name, cmd)

def commit(server_id, fix_file, desc):
    db = pymysql.connect('192.168.3.48','root','firmware','firmdep')
    cursor = db.cursor()
    cursor.execute('select * from fixes order by id desc limit 1;')
    result = cursor.fetchall()
    if len(result) > 0:
        new_id = int(result[0][0]) + 1
    else:
        new_id = 1
    get_server = cursor.execute("select sign_call_20,sign_call_30,sign_call_50,sign_call_100,sign_20,sign_30,sign_50,sign_100 from signatures where server_id='%s'" %(repr(server_id)))
    if get_server == 0:
        progess_err("invaild server id")
        return None
    result = cursor.fetchall()
    sign_call_20,sign_call_30,sign_call_50,sign_call_100,sign_20,sign_30,sign_50,sign_100 = result[0]
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id, sign_call_20, 'sign_call_20', fix_file, desc))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+1, sign_call_30, 'sign_call_30', fix_file, desc))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+2, sign_call_50, 'sign_call_50', fix_file, desc))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+3, sign_call_100, 'sign_call_100', fix_file, desc))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+4, sign_20, 'sign_20', fix_file, desc))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+5, sign_30, 'sign_30', fix_file, desc))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+6, sign_50, 'sign_50', fix_file, desc))
    cmd = "INSERT INTO fixes VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(cmd,(new_id+7, sign_100, 'sign_100', fix_file, desc))
    db.commit()
    db.close()
    return new_id

def main():
    p = SignatureProject(target_path='/usr/sbin/httpds', workdir = '/share/rpi/342', load_fix = None)
    print(repr(p.gen_trace_signature()))

if __name__ == "__main__":
    target_path = None
    workdir = None
    fix = None
    server_id = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], '-h-p:-w:-i:-f',['help','target_path','workdir','server_id','fix'])
    except:
        progress_err("usage: func_fix.py -p <target_path> -w <workdir> -f <fix_description_file>")
        progress_err("crucial arguments: -p, -w")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            progress_err("usage: func_fix.py -p <target_path> -w <workdir> -f <fix_description_file>")
            progress_err("crucial arguments: -p, -w")  
            sys.exit(2)
        elif opt in ('-p','--target_path'):
            target_path = arg
        elif opt in ('-w','--workdir'):
            workdir = arg
        elif opt in ('-i','--server_id'):
            server_id = int(arg)
        elif opt in ('-f','--fix'):
            fix = arg
    p = FixGenProject(target_path=target_path, workdir = workdir, load_fix = fix)
    p.process(server_id)
