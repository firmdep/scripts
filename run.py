#!/usr/bin/env python3
import pexpect
import sys, getopt
import os,shutil
import time
import random
import string
from fixes import *
from colorama import Fore, Style
from pandare import *

def progress(msg):
    print(Fore.CYAN + '[+] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_warn(msg):
    print(Fore.YELLOW + '[!] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_err(msg):
    print(Fore.RED + '[-] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def check_failed():
	'''
	custom function for checking the program's condition.
	for example, you can try to access the emulated service using nmap or curl:
	'''
	return True

def play(target_bin, work_dir, cmd, fixdesc = None, snap_name = None):
	def ranstr():
		return ''.join(random.sample(string.ascii_letters + string.digits, 6))
	def generate_bootargs(target_bin, work_dir, monitor_socket, vmargs):
		boot_args = ['-M','versatilepb',
		'-cpu','cortex-a15',
		'-m', '256M',
		'-hda',work_dir + '/image.qcow2', 
		'-nographic',
		'-kernel','/share/zImage_v7_ipv6',
		'-dtb','/share/versatile-pb_v7.dtb',
		'-initrd','/share/rootfs.cpio',
		'-append','\"panic=1 nokaslr quiet console=ttyAMA0\"', 
		'-os', 'linux-32-debian', 
		'-serial', 'mon:stdio',
		'-no-reboot', 
		'-monitor', 'unix:%s,server,nowait' %monitor_socket,
		'-panda', 'osi:disable-autoload=true',
		'-panda', 'osi_linux:kconf_file=/share/gdb.conf,kconf_group=v7',
		'-panda', 'hooks2',
		'-panda', 'tracer:target=%s' %target_bin]
		default_net_args = ['-net','nic','-net','user,net=192.168.1.0/24,dhcpstart=192.168.1.1,hostfwd=tcp::10080-:80']
		if vmargs != []:
			boot_args = boot_args + ['-net', 'nic']
			for vmarg in vmargs:
				boot_args.append(vmarg.arg)
				boot_args.append(vmarg.value)
		else:
			boot_args = boot_args + default_net_args
		boot_args_str = ''
		for a in boot_args:
			boot_args_str = boot_args_str + ' ' + a
		return 'panda-system-arm ' + boot_args_str
	progress("Preparing VM...")
	monitor_socket = '/tmp/monitor_' + ranstr()
	fixes = FixDesc(fixdesc)
	boot_args = generate_bootargs(target_bin, work_dir, monitor_socket, fixes.custom_vm_args)
	progress("PANDA args: %s" %boot_args)
	child = pexpect.spawn(boot_args, encoding = 'ascii', logfile = sys.stdout)
	monitor = pexpect.spawn("nc -U %s" %monitor_socket, encoding = 'utf-8')
	while True:
		ret = child.expect(['#', pexpect.TIMEOUT, pexpect.EOF,], searchwindowsize=2000)
		if ret == 0:
			break
	if fixes.before_exec_cmds != []:
		str = '' 
		for c in fixes.before_exec_cmds:
			if str == '':
				str = c.cmd
			else:
				str = str + '\n' + c.cmd
		child.sendline(str)
		child.expect(['firmware-root', pexpect.TIMEOUT, pexpect.EOF,], searchwindowsize=2000)
	if fixes.force_exec_tasks != []:
		print("")
		for f in fixes.force_exec_tasks:
			progress("sending command to terminal: %s" %f.repr_data_qmp())
			# monitor.sendline(f.repr_data_qmp())
			child.sendline('\x01c%s\n\x01c' %f.repr_data_qmp())
	print("")
	progress("Now we're going to run the program, enjoy yourself!")
	progress("Port forward: localhost:10080 -> guest:port")
	child.sendline(cmd)
	child.logfile = None
	monitor.logfile = None
	child.interact()

def test():
	play('httpds','/share/342', 'httpds')

if __name__ == "__main__":
    target_bin = None
    target_path = None
    workdir = None
    cmd = None
    fix = None
    record_name = None
    timeout = 30
    try:
    	opts, args = getopt.getopt(sys.argv[1:], '-h-p:-w:-c:-f:',['help', 'target_path','workdir','cmd','fix'])
    except:
    	progress_err("usage: player.py -p <target_path> -w <workdir> -c <cmd> -f <fix_description_file> ")
    	progress_err("crucial arguments: -p, -w, -c")
    	sys.exit(2)
    for opt, arg in opts:
    	if opt in ('-h','--help'):
    		progress_err("usage: player.py -p <target_path> -w <workdir> -c <cmd> -f <fix_description_file>")
    		progress_err("crucial arguments: -p, -w, -c")
    		sys.exit(2)
    	elif opt in ('-p','--target_path'):
    		target_path = arg
    	elif opt in ('-w','--workdir'):
    		workdir = arg
    	elif opt in ('-c','--cmd'):
    		cmd = arg
    	elif opt in ('-f','--fix'):
    		fix = arg
    target_bin = os.path.basename(target_path)
    play(target_bin, workdir, cmd, fixdesc = fix)
