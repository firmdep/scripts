#!/usr/bin/env python3
import pexpect
import sys, getopt
import os,shutil
import time
import random
import string
from fixes import *
from colorama import Fore, Style
from pandare import *
import pymysql

def progress(msg):
    print(Fore.CYAN + '[+] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_warn(msg):
    print(Fore.YELLOW + '[!] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_err(msg):
    print(Fore.RED + '[-] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def get_args(id):
    db = pymysql.connect('192.168.3.48', 'root', 'firmware', 'firmdep')
    cursor = db.cursor()
    sql = "select iid,path,args from webservers where id = %d" %id
    cursor.execute(sql)
    result = cursor.fetchall()
    return result[0][0], result[0][1], result[0][2]

def play(target_bin, work_dir, cmd, fixdesc = None, snap_name = None):
	def ranstr():
		return ''.join(random.sample(string.ascii_letters + string.digits, 6))
	def generate_bootargs(target_bin, work_dir, monitor_socket, vmargs):
		boot_args = ['-M','versatilepb',
		'-cpu','cortex-a15',
		'-m', '256M',
		'-hda',work_dir + '/image.qcow2', 
		'-nographic',
		'-kernel','/share/zImage_v7_ipv6',
		'-dtb','/share/versatile-pb_v7.dtb',
		'-initrd','/share/rootfs.cpio',
		'-append','\"panic=1 nokaslr quiet console=ttyAMA0\"', 
		'-os', 'linux-32-debian', 
		'-serial', 'mon:stdio',
		'-no-reboot', 
		'-monitor', 'unix:%s,server,nowait' %monitor_socket]
		default_net_args = ['-net','nic','-net','user,net=192.168.1.0/24,dhcpstart=192.168.1.1,hostfwd=tcp::10080-:80']
		if vmargs != []:
			boot_args = boot_args + ['-net', 'nic']
			for vmarg in vmargs:
				boot_args.append(vmarg.arg)
				boot_args.append(vmarg.value)
		else:
			boot_args = boot_args + default_net_args
		boot_args_str = ''
		for a in boot_args:
			boot_args_str = boot_args_str + ' ' + a
		return 'panda-system-arm ' + boot_args_str
	progress("Preparing VM...")
	monitor_socket = '/tmp/monitor_' + ranstr()
	fixes = FixDesc(fixdesc)
	boot_args = generate_bootargs(target_bin, work_dir, monitor_socket, fixes.custom_vm_args)
	progress("PANDA args: %s" %boot_args)
	child = pexpect.spawn(boot_args, encoding = 'ascii', logfile = sys.stdout)
	monitor = pexpect.spawn("nc -U %s" %monitor_socket, encoding = 'utf-8')
	while True:
		ret = child.expect(['#', pexpect.TIMEOUT, pexpect.EOF,], searchwindowsize=2000)
		if ret == 0:
			break
	if fixes.force_exec_tasks != []:
		print("")
		for f in fixes.force_exec_tasks:
			progress("sending command to terminal: %s" %f.repr_data_qmp())
			child.sendline('\x01c%s\n\x01c' %f.repr_data_qmp())
	print("")
	progress("Now we're going to run the program, enjoy yourself!")
	progress("Port forward: localhost:10080 -> guest:port")
	child.sendline(cmd)
	child.logfile = None
	monitor.logfile = None
	child.interact()

def exec(id):
    iid, path, args = get_args(id)
    progress("id = %d, image_id = %d, path = %s, args = %s" %(id, iid, path, args))
    play(path, '/share/images/%d' %iid, 'uname')

if __name__ == "__main__":
    id = int(sys.argv[1])
    exec(id)
