#!/usr/bin/env python3
from pandare import *
from os import path
import os,sys
from api_identity import *
import angr
from query_sql import Database
from ctypes import c_uint
from fixes import *
from colorama import Fore, Style

def progress(msg):
    print(Fore.CYAN + '[+] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_warn(msg):
    print(Fore.YELLOW + '[!] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_err(msg):
    print(Fore.RED + '[-] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

class BaseProject(object):

    def __init__(self, target_path, workdir, target_bin=None, arch='armel', init_panda = True, init_cle = True, init_db = True, load_symbol = False, load_fix = None):
        self.target_path = target_path
        if target_bin == None:
            self.target_bin = path.basename(target_path)
        else:
            self.target_bin = target_bin
        self.workdir = workdir
        self.arch = arch
        self.extra_libs = []
        self.fixes = FixDesc(load_fix)
        for e in self.fixes.extra_libs:
            self.extra_libs.append(e.lib)
        # initialize angr
        if (init_cle):
            self.angr_proj, self.symbol_dict = self.load_angr(load_symbol=load_symbol)
            self.loader = self.angr_proj.loader
            self.addr_range = self.determine_range(self.extra_libs)
            self.included_elfs = [self.target_bin,] + self.extra_libs
        else:
            self.angr_proj = None
            self.loader = None
            self.symbol_dict = None
            self.addr_range = range(0x0, 0xc0000000)
            self.included_elfs = [self.target_bin,] + self.extra_libs
        # initialize PANDA
        if (init_panda):
            if self.arch == 'armel':
                target_image = self.workdir + "/image.qcow2"
                if (self.fixes.custom_vm_args == []):
                    boot_args = ['-M','versatilepb',
                        '-cpu','cortex-a15',
                        '-hda',target_image,
                        '-nographic',
                        '-kernel','/share/zImage_v7',
                        '-dtb','/share/versatile-pb_v7.dtb',
                        '-initrd','/share/rootfs.cpio',
                        '-append','panic=1 nokaslr console=ttyAMA0',
                        '-os', 'linux-32-debian','-no-reboot','-snapshot',
                        '-net','nic','-net','user,net=192.168.1.0/24,dhcpstart=192.168.1.1']
                else:
                    boot_args = ['-M','versatilepb',
                    '-cpu','cortex-a15',
                    '-hda',target_image,
                    '-nographic',
                    '-kernel','/share/zImage_v7_ipv6',
                    '-dtb','/share/versatile-pb_v7.dtb',
                    '-initrd','/share/rootfs.cpio',
                    '-append','panic=1 nokaslr console=ttyAMA0',
                    '-os', 'linux-32-debian','-no-reboot','-snapshot', '-net', 'nic']
                    for vmarg in self.fixes.custom_vm_args:
                        boot_args.append(vmarg.arg)
                        boot_args.append(vmarg.value)
                panda = Panda(generic=None, arch = 'arm', mem="256M", extra_args = boot_args, os= 'linux-arm', expect_prompt = rb'#>.*')
                panda.load_plugin("osi", args = {"disable-autoload":True})
                panda.load_plugin("osi_linux", args = {"kconf_file":"/share/gdb.conf","kconf_group":"v7"})
                panda.load_plugin("syscalls2")
                self.panda = panda
                progress("PANDA loaded")
            else:
                progress_err("Unsupported ISA")
                exit()
        else:
            self.panda = None
        if (init_db):
            self.db = Database(self.workdir, self.target_bin)
        else:
            self.db = None
        os.chdir(self.workdir)

    def get_stackid(self, cpu):
        osithread = self.panda.plugins['osi'].get_current_thread(cpu)
        str =  format((osithread.pid, osithread.tid))
        osithread = None
        return str

    def load_angr(self,load_symbol = False):
        def find_export(proj, name):
            for o in proj.all_elf_objects:
                s = o.get_symbol(name)
                if s != None:
                    if s.is_export == True:
                        return s
            return None
        os.chdir(self.workdir)
        try:
            conf = open("mapping_%s.json" %self.target_bin, "r")
        except:
            conf = None
        if conf != None:
            loaded = eval(conf.readline())
            opts = eval(conf.readline())
        else:
            loaded = []
            opts = {}
        # symbol_dict = eval(conf.readline())
        os.chdir(path.join(self.workdir+'/rootfs'))
        proj = angr.Project( '.' + self.target_path,
            use_system_libs = False,
            auto_load_libs = False,
            force_load_libs = loaded, 
            lib_opts = opts, 
            rebase_granularity=0x1000)
        loader = proj.loader
        symbol_dict = dict()
        if(load_symbol):
            for o in loader.all_elf_objects:
                for symbol in o.symbols:
                    if (symbol.name != ''):
                        real_sym = find_export(loader, symbol.name)
                        if (real_sym != None) and (symbol.rebased_addr != 0):
                            symbol_dict[symbol.rebased_addr] = real_sym
            progress("Done loading symbols.")
        return proj, symbol_dict

    def determine_range(self, extra_libs):
        addr_range = range(self.loader.main_object.min_addr, self.loader.main_object.max_addr)
        loaded = self.loader.all_elf_objects
        lib_dict = dict()
        for o in loaded:
            lib_dict[o.binary_basename] = o
        for e in extra_libs:
            if e in lib_dict.keys():
                addr_range = list(addr_range) + list(range(lib_dict[e].min_addr, lib_dict[e].max_addr))
        return addr_range

    def parse_val(self, cpu, val):
        for _ in range(5):
            if val == 0:
                return hex(val)
            try:
                str_data = self.panda.virtual_memory_read(cpu, val, 64)
            except ValueError:
                return hex(val)
            str_val = ""
            for d in str_data:
                if d >= 0x20 and d < 0x7F:
                    str_val += chr(d)
                else:
                    break
            if len(str_val) > 2:
                return format(str_val)
            return hex(val)

    def get_rr_instr(self):
        count_raw = c_uint(self.panda.rr_get_guest_instr_count()).value
        return count_raw

    def is_target(self, cpu = None, check_process = False):
        if (cpu == None):
            cpu = self.panda.get_cpu()
        if self.panda.in_kernel(cpu):
            return False
        pc = cpu.env_ptr.regs[15]
        # if pc not in self.addr_range:
        if pc not in range(0x0, 0xc0000000):
            return False
        if (check_process == True):
            '''
            this is now not necessary in before_block_exec and after_block_exec as the hooks plugin works
            '''
            proc = self.panda.plugins['osi'].get_current_process(cpu)
            name = str(ffi.string(proc.name),encoding = 'utf-8') if proc != ffi.NULL else "error"
            if name != self.target_bin:
                return False
        return True

    def load_callback(self):
        if (self.fixes.force_exec_tasks != []):
            self.panda.disable_tb_chaining()
            self.panda.enable_precise_pc()
            @self.panda.cb_before_block_exec(name = 'fix_before_exec')
            def fix_bbe(cpu, tb):
                if (self.is_target(cpu = cpu) == False):
                    return
                for t in self.fixes.force_exec_tasks:
                    if t.matches(cpu.env_ptr.regs[15], cpu.env_ptr.regs[14], 'before'):
                        progress("performing force execution...")
                        t.do_fix(self.panda)
            @self.panda.cb_after_block_exec(name = 'fix_after_exec')
            def fix_abe(cpu, tb, exitcode):
                if (self.is_target(cpu = cpu) == False):
                    return
                for t in self.fixes.force_exec_tasks:
                    if t.matches(cpu.env_ptr.regs[15], cpu.env_ptr.regs[14], 'after'):
                        progress("performing force execution...")
                        t.do_fix(self.panda)

    def run(self,replay=None):
        self.load_callback()
        if replay == None:
            self.panda.run_replay(self.target_bin)
        else:
            self.panda.run_replay(replay)

class Task(object):
    def __init__(self, panda, stackid):
        progress("New task: "%format(stackid))
        """
        stackid: As an identity for a process/thread.
        """
        self.stackid = stackid
        self.pid = pid_from_stackid(self.stackid)
        self.tid = tid_from_stackid(self.stackid)

class ProgramState(object):
    def __init__(self):
        self.callinfos = dict()
        self.retinfos = dict()
        self.events = dict()
    def new_call(self, proj, pos = None):
        if pos == None:
            pos = proj.get_rr_instr()
        cpu = proj.panda.get_cpu()
        addr = cpu.env_ptr.regs[15]
        retaddr = cpu.env_ptr.regs[14]
        stackid = proj.get_stackid(cpu)
        callinfo = new_callinfo(proj, addr, retaddr, stackid)
        self.callinfos[pos] = callinfo
        self.events[pos] = 'call'
    def new_return(self, proj, pos = None):
        if pos == None:
            pos = proj.get_rr_instr()
        cpu = proj.panda.get_cpu()
        callinfo = self.callinfos[list(self.callinfos.keys())[-1]]
        val = cpu.env_ptr.regs[0]
        stackid = proj.get_stackid(cpu)
        retinfo = new_retinfo(proj, callinfo, val, None, stackid)
        self.retinfos[pos] = retinfo
        self.events[pos] = 'ret'
    def new_call_from_state(self, proj, state):
        pos = state.pos
        callinfo = new_callinfo_from_state(proj, state)
        self.callinfos[pos] = callinfo
        self.events[pos] = 'call'
    def new_return_from_state(self, proj, state):
        pos = state.pos
        callinfo = self.callinfos[list(self.callinfos.keys())[-1]]
        val = state.regs[0]
        stackid = state.stackid
        retinfo = new_retinfo(proj, callinfo, val, None, stackid, pos = pos, parse_data = False)
        self.retinfos[pos] = retinfo
        self.events[pos] = 'ret'
    def get_callstack(self, pos):
        callstack = []
        for p in self.events.keys():
            if p <= pos:
                if self.events[p] == 'call':
                    callstack.append(self.callinfos[p].retaddr)
                elif self.events[p] == 'ret':
                    if callstack[-1] == self.retinfos[p].retaddr:
                        callstack.pop()
            else:
                break
        return callstack
    def get_functions(self, pos):
        functions = []
        for p in self.events.keys():
            if p <= pos:
                if self.events[p] == 'call':
                    functions.append(self.callinfos[p].func)
                elif self.events[p] == 'ret':
                    if functions[-1] == self.retinfos[p].func:
                        functions.pop()
            else:
                break
        return functions
    def gen_callgraph(self):
        last_op = None
        depth = 0
        last_op = None
        for k in self.events.keys():
            if self.events[k] == 'call':
                info = self.callinfos[k]
                if last_op == 'call':
                    print('')
                last_op = 'call'
                if depth != 0:
                    print("| "*depth + "+--%s" %repr(info), end='')
                    depth += 1
                else:
                    print(repr(info), end='')
                    depth += 1
            elif self.events[k] == 'ret':
                info = self.retinfos[k]
                if last_op != 'call':
                    print("| "*(depth-1) + "+-- = %s" %repr(info.data))
                    depth -= 1
                else:
                    print(" = %s" %repr(info.data))
                    depth -= 1
                last_op = 'ret'

class State(object):
    def __init__(self, type, pos, stackid, pc, regs, size, exitcode):
        self.type = type
        self.pos = pos
        self.stackid = stackid
        self.pc = pc
        self.regs = regs
        self.size = size
        self.lr = regs[14]
        self.exitcode = exitcode
    def __hash__(self):
        return hash(self)
    def __eq__(self, other):
        if other != None:
            return self.regs == other.regs
        else:
            return False
    def __repr__(self):
        return "instr:%d %s %s pc:%x lr:%x regs:%x,%x,%x,%x size:%d" %(self.pos, self.type, self.stackid, self.pc, self.lr, self.regs[0], self.regs[1], self.regs[2], self.regs[3], self.size)

class BasicBlock(object):
    def __init__(self, state_bb, state_ab, block):
        self.state_bb = state_bb
        self.state_ab = state_ab
        self.block = block
        self.size = self.state_bb.size
        self.pos = self.state_bb.pos
        self.stackid = self.state_bb.stackid
        self.mem_ops = []
        self.tmp_values = dict()
        self.exitcode = self.state_ab.exitcode
        self.simstate = None
    def pp(self, loader):
        vex = self.block.vex
        des = loader.describe_addr(self.state_bb.pc)
        print("Basic block at %s, size = %d" %(des, self.size))
        print(repr(self.state_bb))
        self.block.pp()
        if (vex.jumpkind == 'Ijk_Ret'):
            lr = loader.describe_addr(self.state_ab.regs[14])
            print("Returning to %s, return value = %x" %(lr, self.state_ab.regs[0]))
        elif (vex.jumpkind == 'Ijk_Call'):
            callee = loader.describe_addr(self.state_ab.pc)
            print("Calling %s, args = [%x,%x,%x,%x]" %(callee, self.state_ab.regs[0], self.state_ab.regs[1], self.state_ab.regs[2], self.state_ab.regs[3]))
        elif ('Ijk_Sys_syscall' in vex.jumpkind):
            call = syscalls[self.state_ab.regs[7]]
            print("Syscall %s, args = [%x,%x,%x,%x]" %(call, self.state_ab.regs[0], self.state_ab.regs[1], self.state_ab.regs[2], self.state_ab.regs[3]))
        else:
            pass
        print(repr(self.state_ab))
        extra_jump_targets = []
        ct = list(vex.constant_jump_targets)
        for c in ct:
            if (c != self.state_ab.pc):
                extra_jump_targets.append(hex(c))
        if(len(extra_jump_targets)>0):
            print("extra_targets:" + repr(extra_jump_targets))

class MemOp(object):
    def __init__(self,pc,op_type,addr,cond):
        self.pc = pc
        self.op_type = op_type
        self.addr = addr
        self.cond = cond
    def pp(self):
        print("pc= 0x%x " %self.pc + repr(self.op_type) + repr(self.addr) + repr(self.cond))
    def __eq__(self,other):
        return self.__dict__ == other.__dict__
    def __hash__(self):
        return hash((self.pc,self.op_type,self.addr))

class TmpOp(object):
    def __init__(self,index,irsb,value):
        self.index = index
        self.irsb = irsb
        self.value = value
    def __eq__(self,other):
        return self.__dict__ == other.__dict__
    def __hash__(self):
        return hash((self.index,self.irsb,self.value))

def main():
    p = BaseProject(target_path='/usr/sbin/lighttpd', workdir = '/share/rpi/20814')
    p.db.save()

if __name__ == "__main__":
    main()
