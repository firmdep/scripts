#!/usr/bin/env python3
import pexpect
import sys, getopt
import os,shutil
import time
import random
import string
from fixes import *
from colorama import Fore, Style
from pandare import *

def progress(msg):
    print(Fore.CYAN + '[+] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_warn(msg):
    print(Fore.YELLOW + '[!] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_err(msg):
    print(Fore.RED + '[-] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def acquire_mapping(target_bin, work_dir, target_path, record_name = None, fixdesc = None):
    os.chdir(work_dir)
    progress("Acquiring memory mapping information for target program...")
    boot_args = ['-M','versatilepb','-cpu','cortex-a15',
    '-hda', work_dir + '/image.qcow2', 
    '-nographic',
    '-kernel','/share/zImage_v7_ipv6',
    '-dtb','/share/versatile-pb_v7.dtb',
    '-initrd','/share/rootfs.cpio',
    '-append','panic=1 nokaslr quiet console=ttyAMA0',
    '-os', 'linux-32-debian',
    '-no-reboot','-snapshot']
    net_boot_args = ['-net','nic','-net','user,net=192.168.1.0/24,dhcpstart=192.168.1.1']
    fixes = FixDesc(fixdesc)
    if fixes.custom_vm_args == []:
        boot_args = boot_args + net_boot_args
    else:
        for vmarg in fixes.custom_vm_args:
            boot_args.append(vmarg.arg)
            boot_args.append(vmarg.value)
    panda = Panda(generic=None, arch = 'arm', mem="256M", extra_args = boot_args, expect_prompt = rb'firmware-root:#>.*')
    panda.load_plugin("osi", args = {"disable-autoload":True})
    panda.load_plugin("osi_linux", args = {"kconf_file":"/share/gdb.conf","kconf_group":"v7"})
    panda.load_plugin("syscalls2")
    loaded = []
    opts = {}

    @panda.ppp("syscalls2", "on_do_mmap2_return", name="load_lib")
    def on_load_lib_return(cpu, pc, addr, len, prot, flags, fd, pgoff):
        nonlocal target_bin
        nonlocal target_path
        nonlocal loaded
        nonlocal opts
        proc = panda.plugins['osi'].get_current_process(cpu)
        name = str(ffi.string(proc.name),encoding = 'utf-8')
        if (name == target_bin):
            loaded = []
            opts = {}
            mappings = panda.get_mappings(cpu)
            while (mappings.current_idx < mappings.garray_len):
                record = mappings.__next__()
                try:
                    bin_path = ffi.string(record.file).decode('utf8','ignore')
                except RuntimeError as e:
                    bin_path = '<NULL>'
                try:
                    name = ffi.string(record.name).decode('utf8','ignore')
                except RuntimeError as e:
                    name = '<NULL>'
                if bin_path in loaded:
                    continue
                elif bin_path == target_path:
                    continue
                elif bin_path == '<NULL>':
                    continue
                elif 'SYSV' in bin_path:
                    continue
                else:
                    loaded.append(os.path.join(work_dir + '/rootfs' + bin_path))
                    if name not in opts.keys():
                        opts[name] = {"base_addr":record.base}
    if (fixes.force_exec_tasks != []):
        @panda.cb_before_block_exec(name='fix_before', enabled = False, procname = cmd)
        def bbe(cpu, tb):
            if is_target(cpu):
                pc = cpu.env_ptr.regs[15]
                lr = cpu.env_ptr.regs[14]
                for t in fixes.force_exec_tasks:
                    if t.matches(pc, lr, 'before'):
                        print("[!] performing force execution...")
                        t.do_fix(panda)
        @panda.cb_after_block_exec(name='fix_after', enabled = False, procname = cmd)
        def abe(cpu, tb, exitcode):
            if is_target(cpu):
                pc = cpu.env_ptr.regs[15]
                lr = cpu.env_ptr.regs[14]
                for t in fixes.force_exec_tasks:
                    if t.matches(pc, lr, 'after'):
                        print("[!] performing force execution...")
                        t.do_fix(panda)
        @panda.ppp("syscalls2", 'on_sys_chroot_return', name = 'trigger')
        def trigger(cpu, pc, path):
            panda.enable_callback("fix_before")
            panda.enable_callback("fix_after")
    if record_name == None:
        record_name = target_bin
    panda.run_replay(record_name)
    # print(repr(loaded), repr(opts))
    out = "mapping_" + target_bin + '.json'
    out_fd = open(out, 'w')
    out_fd.write(repr(loaded)+'\n')
    out_fd.write(repr(opts)+'\n')
    progress("Successfully exported the program's memory mapping information")

if __name__ == "__main__":
    target_bin = None
    target_path = None
    workdir = None
    cmd = None
    fix = None
    record_name = None
    timeout = 30
    shell = 'firmware-root'
    try:
        opts, args = getopt.getopt(sys.argv[1:], '-h-p:-w:-f',['help', 'target_path','workdir','fix'])
    except:
        progress_err("usage: gen_record.py -p <target_path> -w <workdir> -c <cmd> -f <fix_description_file> -n <record_name> -t <timeout>")
        progress_err("crucial arguments: -p, -w, -c")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            progress_err("usage: gen_record.py -p <target_path> -w <workdir> -c <cmd> -f <fix_description_file> -n <record_name> -t <timeout>")
            progress_err("crucial arguments: -p, -w, -c")
            sys.exit(2)
        elif opt in ('-p','--target_path'):
            target_path = arg
        elif opt in ('-w','--workdir'):
            workdir = arg
        elif opt in ('-f','--fix'):
            fix = arg
    target_bin = os.path.basename(target_path)
    shutil.move(target_bin + '-rr-snp', workdir + '/' + target_bin + '-rr-snp')
    shutil.move(target_bin + '-rr-nondet.log', workdir + '/' + target_bin + '-rr-nondet.log')
    acquire_mapping(target_bin, workdir, target_path, record_name)
