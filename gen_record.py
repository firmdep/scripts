#!/usr/bin/env python3
import pexpect
import sys, getopt
import os,shutil
import time
import random
import string
from fixes import *
from colorama import Fore, Style
from pandare import *

def progress(msg):
    print(Fore.CYAN + '[+] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_warn(msg):
    print(Fore.YELLOW + '[!] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def progress_err(msg):
    print(Fore.RED + '[-] ' + Fore.RESET + Style.BRIGHT + msg + Style.RESET_ALL)

def check_failed():
	'''
	custom function for checking the program's condition.
	for example, you can try to access the emulated service using nmap or curl:
	'''
	return True

def do_record(target_bin, work_dir, cmd, snap_name = None, timeout = 30, fixdesc = None, shell = 'firmware-root'):
	def ranstr():
		return ''.join(random.sample(string.ascii_letters + string.digits, 6))
	def generate_bootargs(target_bin, work_dir, monitor_socket, vmargs):
		boot_args = ['-M','versatilepb',
		'-cpu','cortex-a15',
		'-m', '256M',
		'-hda',work_dir + '/image.qcow2', 
		'-nographic',
		'-kernel','/share/zImage_v7_ipv6',
		'-dtb','/share/versatile-pb_v7.dtb',
		'-initrd','/share/rootfs.cpio',
		'-append','\"panic=1 nokaslr quiet console=ttyAMA0\"', 
		'-os', 'linux-32-debian', 
		'-serial', 'mon:stdio',
		'-no-reboot', 
		'-snapshot',
		'-monitor', 'unix:%s,server,nowait' %monitor_socket,
		'-panda', 'osi:disable-autoload=true',
		'-panda', 'osi_linux:kconf_file=/share/gdb.conf,kconf_group=v7',
		 '-panda', 'hooks2',
		 '-panda', 'tracer:target=%s' %target_bin]
		default_net_args = ['-net','nic','-net','user,net=192.168.1.0/24,dhcpstart=192.168.1.1']
		if vmargs != []:
			boot_args = boot_args + ['-net', 'nic']
			for vmarg in vmargs:
				boot_args.append(vmarg.arg)
				boot_args.append(vmarg.value)
		else:
			boot_args = boot_args + default_net_args
		boot_args_str = ''
		for a in boot_args:
			boot_args_str = boot_args_str + ' ' + a
		return 'panda-system-arm ' + boot_args_str
	progress("Preparing VM...")
	if snap_name == None:
		snap_name = target_bin
	monitor_socket = '/tmp/monitor_' + ranstr()
	fixes = FixDesc(fixdesc)
	boot_args = generate_bootargs(target_bin, work_dir, monitor_socket, fixes.custom_vm_args)
	progress("PANDA args: %s" %boot_args)
	child = pexpect.spawn(boot_args, encoding = 'ascii', logfile = sys.stdout)
	monitor = pexpect.spawn("nc -U %s" %monitor_socket, encoding = 'utf-8', logfile = sys.stdout)
	while True:
		ret = child.expect([shell, pexpect.TIMEOUT, pexpect.EOF,], searchwindowsize=2000)
		if ret == 0:
			break
	if fixes.before_exec_cmds != []:
		str = '' 
		for c in fixes.before_exec_cmds:
			if str == '':
				str = c.cmd
			else:
				str = str + ';' + c.cmd
		child.sendline(str)
		child.expect([shell, pexpect.TIMEOUT, pexpect.EOF,], searchwindowsize=2000)
	if fixes.force_exec_tasks != []:
		for f in fixes.force_exec_tasks:
			progress("sending command to terminal: %s" %f.repr_data_qmp())
			monitor.sendline(f.repr_data_qmp())
			child.sendline('\x01c%s\n\x01c' %f.repr_data_qmp())
	print("")
	progress("Generating replay for %s, timeout = %d" %(target_bin, timeout))
	child.sendline('\x01c%s\n\x01c' %('begin_record ' + target_bin ))
	child.sendline(cmd)
	time.sleep(timeout)
	progress("Saving replay...")
	monitor.sendline("end_record")
	monitor.sendline("q")
	if check_failed():
		progress("The target didn't work properly.")
	else:
		progress("The target worked properly.")
	shutil.move(snap_name + '-rr-snp', work_dir + '/' + snap_name + '-rr-snp')
	shutil.move(snap_name + '-rr-nondet.log', work_dir + '/' + snap_name + '-rr-nondet.log')

def acquire_mapping(target_bin, work_dir, target_path, record_name = None, fixdesc = None):
	os.chdir(work_dir)
	progress("Current work_dir:%s" %work_dir)
	progress("Acquiring memory mapping information for target program...")
	boot_args = ['-M','versatilepb','-cpu','cortex-a15',
	'-hda', work_dir + '/image.qcow2', 
	'-nographic',
	'-kernel','/share/zImage_v7',
	'-dtb','/share/versatile-pb_v7.dtb',
	'-initrd','/share/rootfs.cpio',
	'-append','panic=1 nokaslr quiet console=ttyAMA0',
	'-os', 'linux-32-debian',
	'-no-reboot','-snapshot']
	net_boot_args = ['-net','nic','-net','user,net=192.168.1.0/24,dhcpstart=192.168.1.1']
	fixes = FixDesc(fixdesc)
	if fixes.custom_vm_args == []:
	    boot_args = boot_args + net_boot_args
	else:
	    for vmarg in fixes.custom_vm_args:
	        boot_args.append(vmarg.arg)
	        boot_args.append(vmarg.value)
	panda = Panda(generic=None, arch = 'arm', mem="256M", extra_args = boot_args, expect_prompt = rb'firmware-root:#>.*')
	panda.load_plugin("osi", args = {"disable-autoload":True})
	panda.load_plugin("osi_linux", args = {"kconf_file":"/share/gdb.conf","kconf_group":"v7"})
	panda.load_plugin("syscalls2")
	loaded = []
	opts = {}

	@panda.ppp("syscalls2", "on_do_mmap2_return", name="load_lib")
	def on_load_lib_return(cpu, pc, addr, len, prot, flags, fd, pgoff):
	    nonlocal target_bin
	    nonlocal target_path
	    nonlocal loaded
	    nonlocal opts
	    proc = panda.plugins['osi'].get_current_process(cpu)
	    name = str(ffi.string(proc.name),encoding = 'utf-8')
	    if (name == target_bin):
	        loaded = []
	        opts = {}
	        mappings = panda.get_mappings(cpu)
	        while (mappings.current_idx < mappings.garray_len):
	            record = mappings.__next__()
	            try:
	                bin_path = ffi.string(record.file).decode('utf8','ignore')
	            except RuntimeError as e:
	                bin_path = '<NULL>'
	            try:
	                name = ffi.string(record.name).decode('utf8','ignore')
	            except RuntimeError as e:
	                name = '<NULL>'
	            if bin_path in loaded:
	                continue
	            elif bin_path == target_path:
	                continue
	            elif bin_path == '<NULL>':
	                continue
	            else:
	                loaded.append(os.path.join(work_dir + '/rootfs' + bin_path))
	                if name not in opts.keys():
	                    opts[name] = {"base_addr":record.base}
	if (fixes.force_exec_tasks != []):
	    @panda.cb_before_block_exec(name='fix_before', enabled = False, procname = cmd)
	    def bbe(cpu, tb):
	        if is_target(cpu):
	            pc = cpu.env_ptr.regs[15]
	            lr = cpu.env_ptr.regs[14]
	            for t in fixes.force_exec_tasks:
	                if t.matches(pc, lr, 'before'):
	                    print("[!] performing force execution...")
	                    t.do_fix(panda)
	    @panda.cb_after_block_exec(name='fix_after', enabled = False, procname = cmd)
	    def abe(cpu, tb, exitcode):
	        if is_target(cpu):
	            pc = cpu.env_ptr.regs[15]
	            lr = cpu.env_ptr.regs[14]
	            for t in fixes.force_exec_tasks:
	                if t.matches(pc, lr, 'after'):
	                    print("[!] performing force execution...")
	                    t.do_fix(panda)
	    @panda.ppp("syscalls2", 'on_sys_chroot_return', name = 'trigger')
	    def trigger(cpu, pc, path):
	        panda.enable_callback("fix_before")
	        panda.enable_callback("fix_after")
	if record_name == None:
		record_name = target_bin
	panda.run_replay(record_name)
	# print(repr(loaded), repr(opts))
	out = "mapping_" + target_bin + '.json'
	out_fd = open(out, 'w')
	out_fd.write(repr(loaded)+'\n')
	out_fd.write(repr(opts)+'\n')
	progress("Successfully exported the program's memory mapping information")

def test():
	do_record('httpds','/share/rpi/342', 'httpds')
	acquire_mapping('httpds','/share/rpi/342','/usr/sbin/httpds','httpds')

if __name__ == "__main__":
    target_bin = None
    target_path = None
    workdir = None
    cmd = None
    fix = None
    record_name = None
    timeout = 30
    shell = 'firmware-root'
    try:
    	opts, args = getopt.getopt(sys.argv[1:], '-h-p:-w:-c:-f:-n:-t:-s',['help', 'target_path','workdir','cmd','fix','record_name','timeout','shell'])
    except:
    	progress_err("usage: gen_record.py -p <target_path> -w <workdir> -c <cmd> -f <fix_description_file> -n <record_name> -t <timeout>")
    	progress_err("crucial arguments: -p, -w, -c")
    	sys.exit(2)
    for opt, arg in opts:
    	if opt in ('-h','--help'):
    		progress_err("usage: gen_record.py -p <target_path> -w <workdir> -c <cmd> -f <fix_description_file> -n <record_name> -t <timeout>")
    		progress_err("crucial arguments: -p, -w, -c")
    		sys.exit(2)
    	elif opt in ('-p','--target_path'):
    		target_path = arg
    	elif opt in ('-w','--workdir'):
    		workdir = arg
    	elif opt in ('-c','--cmd'):
    		cmd = arg
    	elif opt in ('-f','--fix'):
    		fix = arg
    	elif opt in ('-n','--record_name'):
    		record_name = arg
    	elif opt in ('-t','--timeout'):
    		timeout = int(arg)
    	elif opt in ('-s','--shell'):
    		shell = arg
    target_bin = os.path.basename(target_path)
    do_record(target_bin, workdir, cmd, snap_name = record_name, timeout = timeout, fixdesc = fix, shell = shell)
    acquire_mapping(target_bin, workdir, target_path, record_name)
