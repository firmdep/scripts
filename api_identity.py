#!/usr/bin/env python3
from pandare import *
# from panda.arm.helper import *
import struct
from binascii import b2a_hex
import socket
import os
import angr

syscalls = {0: 'restart_syscall', 1: 'exit', 2: 'fork', 3: 'read', 4: 'write', 5: 'open', 6: 'close', 7: 'waitpid', 8: 'creat', 9: 'link', 10: 'unlink', 11: 'execve', 12: 'chdir', 13: 'time', 14: 'mknod', 15: 'chmod', 16: 'lchown', 17: 'break', 18: 'oldstat', 19: 'lseek', 20: 'getpid', 21: 'mount', 22: 'umount', 23: 'setuid', 24: 'getuid', 25: 'stime', 26: 'ptrace', 27: 'alarm', 28: 'oldfstat', 29: 'pause', 30: 'utime', 31: 'stty', 32: 'gtty', 33: 'access', 34: 'nice', 35: 'ftime', 36: 'sync', 37: 'kill', 38: 'rename', 39: 'mkdir', 40: 'rmdir', 41: 'dup', 42: 'pipe', 43: 'times', 44: 'prof', 45: 'brk', 46: 'setgid', 47: 'getgid', 48: 'signal', 49: 'geteuid', 50: 'getegid', 51: 'acct', 52: 'umount2', 53: 'lock', 54: 'ioctl', 55: 'fcntl', 56: 'mpx', 57: 'setpgid', 58: 'ulimit', 59: 'oldolduname', 60: 'umask', 61: 'chroot', 62: 'ustat', 63: 'dup2', 64: 'getppid', 65: 'getpgrp', 66: 'setsid', 67: 'sigaction', 68: 'sgetmask', 69: 'ssetmask', 70: 'setreuid', 71: 'setregid', 72: 'sigsuspend', 73: 'sigpending', 74: 'sethostname', 75: 'setrlimit', 76: 'getrlimit', 77: 'getrusage', 78: 'gettimeofday', 79: 'settimeofday', 80: 'getgroups', 81: 'setgroups', 82: 'select', 83: 'symlink', 84: 'oldlstat', 85: 'readlink', 86: 'uselib', 87: 'swapon', 88: 'reboot', 89: 'readdir', 90: 'mmap', 91: 'munmap', 92: 'truncate', 93: 'ftruncate', 94: 'fchmod', 95: 'fchown', 96: 'getpriority', 97: 'setpriority', 98: 'profil', 99: 'statfs', 100: 'fstatfs', 101: 'ioperm', 102: 'socketcall', 103: 'syslog', 104: 'setitimer', 105: 'getitimer', 106: 'stat', 107: 'lstat', 108: 'fstat', 109: 'olduname', 110: 'iopl', 111: 'vhangup', 112: 'idle', 113: 'syscall', 114: 'wait4', 115: 'swapoff', 116: 'sysinfo', 117: 'ipc', 118: 'fsync', 119: 'sigreturn', 120: 'clone', 121: 'setdomainname', 122: 'uname', 123: 'modify_ldt', 124: 'adjtimex', 125: 'mprotect', 126: 'sigprocmask', 127: 'create_module', 128: 'init_module', 129: 'delete_module', 130: 'get_kernel_syms', 131: 'quotactl', 132: 'getpgid', 133: 'fchdir', 134: 'bdflush', 135: 'sysfs', 136: 'personality', 137: 'afs_syscall', 138: 'setfsuid', 139: 'setfsgid', 140: '_llseek', 141: 'getdents', 142: '_newselect', 143: 'flock', 144: 'msync', 145: 'readv', 146: 'writev', 147: 'getsid', 148: 'fdatasync', 149: '_sysctl', 150: 'mlock', 151: 'munlock', 152: 'mlockall', 153: 'munlockall', 154: 'sched_setparam', 155: 'sched_getparam', 156: 'sched_setscheduler', 157: 'sched_getscheduler', 158: 'sched_yield', 159: 'EN(sched_get_priority_max)', 160: 'EN(sched_get_priority_min)', 161: '', 162: 'nanosleep', 163: 'mremap', 164: 'setresuid', 165: 'getresuid', 166: 'vm86', 167: 'query_module', 168: 'poll', 169: 'nfsservctl', 170: 'setresgid', 171: 'getresgid', 172: 'prctl', 173: 'rt_sigreturn', 174: 'rt_sigaction', 175: 'rt_sigprocmask', 176: 'rt_sigpending', 177: 'rt_sigtimedwait', 178: 'rt_sigqueueinfo', 179: 'rt_sigsuspend', 180: 'pread64', 181: 'pwrite64', 182: 'chown', 183: 'getcwd', 184: 'capget', 185: 'capset', 186: 'sigaltstack', 187: 'sendfile', 188: 'getpmsg', 189: 'putpmsg', 190: 'vfork', 191: 'ugetrlimit', 192: 'mmap2', 193: 'truncate64', 194: 'ftruncate64', 195: 'stat64', 196: 'lstat64', 197: 'fstat64', 198: 'lchown32', 199: 'getuid32', 200: 'getgid32', 201: 'geteuid32', 202: 'getegid32', 203: 'setreuid32', 204: 'setregid32', 205: 'getgroups32', 206: 'setgroups32', 207: 'fchown32', 208: 'setresuid32', 209: 'getresuid32', 210: 'setresgid32', 211: 'getresgid32', 212: 'chown32', 213: 'setuid32', 214: 'setgid32', 215: 'setfsuid32', 216: 'setfsgid32', 217: 'getdents64', 218: 'pivot_root', 219: 'mincore', 220: 'madvise', 221: 'fcntl64', 224: 'gettid', 225: 'readahead', 226: 'setxattr', 227: 'lsetxattr', 228: 'fsetxattr', 229: 'getxattr', 230: 'lgetxattr', 231: 'fgetxattr', 232: 'listxattr', 233: 'llistxattr', 234: 'flistxattr', 235: 'removexattr', 236: 'lremovexattr', 237: 'fremovexattr', 238: 'tkill', 239: 'sendfile64', 240: 'futex', 241: 'sched_setaffinity', 242: 'sched_getaffinity', 243: 'io_setup', 244: 'io_destroy', 245: 'io_getevents', 246: 'io_submit', 247: 'io_cancel', 248: 'exit_group', 249: 'lookup_dcookie', 250: 'epoll_create', 251: 'epoll_ctl', 252: 'epoll_wait', 253: 'remap_file_pages', 256: 'set_tid_address', 257: 'timer_create', 258: 'timer_settime', 259: 'timer_gettime', 260: 'timer_getoverrun', 261: 'timer_delete', 262: 'clock_settime', 263: 'clock_gettime', 264: 'clock_getres', 265: 'clock_nanosleep', 266: 'statfs64', 267: 'fstatfs64', 268: 'tgkill', 269: 'utimes', 270: 'fadvise64_64', 271: 'pciconfig_iobase', 272: 'pciconfig_read', 273: 'pciconfig_write', 274: 'mq_open', 275: 'mq_unlink', 276: 'mq_timedsend', 277: 'mq_timedreceive', 278: 'mq_notify', 279: 'mq_getsetattr', 280: 'waitid', 281: 'socket', 282: 'bind', 283: 'connect', 284: 'listen', 285: 'accept', 286: 'getsockname', 287: 'getpeername', 288: 'socketpair', 289: 'send', 290: 'sendto', 291: 'recv', 292: 'recvfrom', 293: 'shutdown', 294: 'setsockopt', 295: 'getsockopt', 296: 'sendmsg', 297: 'recvmsg', 298: 'semop', 299: 'semget', 300: 'semctl', 301: 'msgsnd', 302: 'msgrcv', 303: 'msgget', 304: 'msgctl', 305: 'shmat', 306: 'shmdt', 307: 'shmget', 308: 'shmctl', 309: 'add_key', 310: 'request_key', 311: 'keyctl', 312: 'semtimedop', 313: 'vserver', 314: 'ioprio_set', 315: 'ioprio_get', 316: 'inotify_init', 317: 'inotify_add_watch', 318: 'inotify_rm_watch', 319: 'mbind', 320: 'get_mempolicy', 321: 'set_mempolicy', 322: 'openat', 323: 'mkdirat', 324: 'mknodat', 325: 'fchownat', 326: 'futimesat', 327: 'fstatat64', 328: 'unlinkat', 329: 'renameat', 330: 'linkat', 331: 'symlinkat', 332: 'readlinkat', 333: 'fchmodat', 334: 'faccessat', 335: 'pselect6', 336: 'ppoll', 337: 'unshare', 338: 'set_robust_list', 339: 'get_robust_list', 340: 'splice', 341: 'sync_file_range2', 342: 'tee', 343: 'vmsplice', 344: 'move_pages', 345: 'getcpu', 346: 'epoll_pwait', 347: 'kexec_load', 348: 'utimensat', 349: 'signalfd', 350: 'timerfd_create', 351: 'eventfd', 352: 'fallocate', 353: 'timerfd_settime', 354: 'timerfd_gettime', 355: 'signalfd4', 356: 'eventfd2', 357: 'epoll_create1', 358: 'dup3', 359: 'pipe2', 360: 'inotify_init1', 361: 'preadv', 362: 'pwritev', 363: 'rt_tgsigqueueinfo', 364: 'perf_event_open', 365: 'recvmmsg', 366: 'accept4', 367: 'fanotify_init', 368: 'fanotify_mark', 369: 'prlimit64', 370: 'name_to_handle_at', 371: 'open_by_handle_at', 372: 'clock_adjtime', 373: 'syncfs', 374: 'sendmmsg', 375: 'setns', 376: 'process_vm_readv', 377: 'process_vm_writev', 378: 'kcmp', 379: 'finit_module', 380: 'sched_setattr', 381: 'sched_getattr', 382: 'renameat2', 383: 'seccomp', 384: 'getrandom', 385: 'memfd_create', 386: 'bpf', 387: 'execveat', 388: 'userfaultfd', 389: 'membarrier', 390: 'mlock2', 391: 'copy_file_range', 392: 'preadv2', 393: 'pwritev2', 394: 'pkey_mprotect', 395: 'pkey_alloc', 396: 'pkey_free', 397: 'statx', 398: 'rseq', 399: 'io_pgetevents', 400: 'migrate_pages', 401: 'kexec_file_load', 983045: 'UNKNOWN_SYSCALL_983045'}

# from The Open Group Base Specifications Issue 7, 2018 edition
# APIs related to math,memory are not counted.
err_returns = {
    'accept': [0xffffffff],
    'access': [0xffffffff],
    'faccessat': [0xffffffff],
    'aio_cancel': [0xffffffff],
    'aio_error': [0xffffffff],
    'aio_fsync': [0xffffffff],
    'aio_read': [0xffffffff],
    'aio_return': [0xffffffff],
    'aio_suspend': [0xffffffff],
    'aio_write': [0xffffffff],
    'asctime': [0x0],
    'asctime_r': [0x0],
    'bind': [0xffffffff],
    'bsearch': [0x0],
    'calloc': [0x0],
    'catclose': [0xffffffff],
    'catopen': [0xffffffff],
    'cfsetospeed': [0xffffffff],
    'cfsetispeed': [0xffffffff],
    'chdir': [0xffffffff],
    'chmod': [0xffffffff],
    'chown': [0xffffffff],
    'clock_getres': [0xffffffff],
    'clock_gettime': [0xffffffff],
    'clock_settime': [0xffffffff],
    'closedir': [0xffffffff],
    'close': [0xffffffff],
    'confstr': [0x0],
    'connect': [0xffffffff],
    'creat': [0xffffffff],
    'open': [0xffffffff],
    'crypt': [0x0],
    'ctime': [0xffffffff],
    'ctime_r': [0x0],
    'daemon': [0xffffffff],
    'dbm_store': range(0x7fffffff,0x100000000),
    'dbm_error': range(0x0,0xffffffff),
    'dbm_open': [0x0],
    'dbm_firstkey': [0x0],
    'dbm_nextkey': [0x0],
    'dbm_fetch': [0x0],
    'dirfd': [0xffffffff],
    'dlopen': [0x0],
    'dlsym': [0x0],
    'dprintf': range(0x7fffffff,0x100000000),
    'fprintf': range(0x7fffffff,0x100000000),
    'printf': range(0x7fffffff,0x100000000),
    'sprintf': range(0x7fffffff,0x100000000),
    'snprintf': range(0x7fffffff,0x100000000),
    'dup': [0xffffffff],
    'dup2': [0xffffffff],
    'duplocate': [0x0],
    'getgrent': [0x0],
    'gethostent': [0x0],
    'getnetbyaddr': [0x0],
    'getnetbyname': [0x0],
    'getnetent': [0x0],
    'getpwent': [0x0],
    'getservbyname': [0x0],
    'getservbyport': [0x0],
    'getservent': [0x0],
    'getutxent': [0x0],
    'getutxid': [0x0],
    'getutxline': [0x0],
    'puttuxline': [0x0],
    'execl': [0xffffffff],
    'execle': [0xffffffff],
    'execlp': [0xffffffff],
    'execv': [0xffffffff],
    'execve': [0xffffffff],
    'execvp': [0xffffffff],
    'fexecve': [0xffffffff],
    'fattach': [0xffffffff],
    'fchdir': [0xffffffff],
    'fchmodat': [0xffffffff],
    'fchmod': [0xffffffff],
    'fchownat': [0xffffffff],
    'fchown': [0xffffffff],
    'fclose': [0xffffffff],
    'fcntl': [0xffffffff],
    'fdatasync': [0xffffffff],
    'fdetach': [0xffffffff],
    'fdopendir': [0x0],
    'opendir': [0x0],
    'fdopen': [0x0],
    # fflush not implemented
    # fgets,fgetwc,fgetws not implemented
    'fileno': [0xffffffff],
    # flockfile,ftrylockfile,funlockfile not implemented
    # fmemopen
    # fnmatch not implemented yet but could be useful
    'fopen':[0x0],
    # we probably don't need fork
    'pathconf': [0xffffffff],
    'fpathconf': [0xffffffff],
    # fread
    'freopen': [0x0],
    'fscanf': [0,0xffffffff],
    'scanf': [0,0xffffffff],
    'sscanf': [0,0xffffffff],
    'fseek': [0xffffffff],
    'fseeko': [0xffffffff],
    'fstatat': [0xffffffff],
    'lstat': [0xffffffff],
    'stat': [0xffffffff],
    'stat64': [0xffffffff],
    'fstat': [0xffffffff],
    'fstatvfs': [0xffffffff],
    'statvfs': [0xffffffff],
    # fsync
    'ftell': [0xffffffff],
    'ftello': [0xffffffff],
    'ftok': [0xffffffff],
    'ftruncate': [0xffffffff],
    # ftw
    'futimens': [0xffffffff],
    'utimensat': [0xffffffff],
    'utimes': [0xffffffff],
    # fwide,fwprintf,swprintf,wprintf
    # fwrite,fputc
    'getcwd': [0x0],
    # getdate
    'getdelim': [0xffffffff],
    'getline': [0xffffffff],
    'getenv': [0x0],
    'getgroups': [0xffffffff],
    #endhostent,gethostent,sethostent
    'gethostname': [0xffffffff],
    'getitimer': [0xffffffff],
    'setitimer': [0xffffffff],
    #getdelim,getline
    'getlogin': [0x0],
    'getmsg': [0xffffffff],
    'getpmsg': [0xffffffff],
    # getopt
    'getpeername': [0xffffffff],
    'getpgid': [0xffffffff],
    #getpriority,setpriority
    'getprotobyname': [0x0],
    'getprotobynumber': [0x0],
    'getprotoent': [0x0],
    # getpwnam,getpwnam_r,getpwuid,getpwuid_r
    'getrlimit': [0xffffffff],
    'setrlimit': [0xffffffff],
    'getrusage' : [0xffffffff],
    # getc
    'getsid': [0xffffffff],
    'getsockname': [0xffffffff],
    'getsockopt': [0xffffffff],
    # getsubopt
    'pututxline': [0x0],
    # getwc,getwchar
    # glob,globfree
    # gmtime,gmtime_r
    # grantpt
    # hcreate,hdestroy,hsearch
    'iconv_close': [0xffffffff],
    'iconv': [0xffffffff],
    'iconv_open': [0xffffffff],
    'inet_addr': [0xffffffff],
    'inet_ntoa': [0x0],
    'inet_ntop': [0x0],
    'ioctl': [0xffffffff],
    # is* functions
    # kill,killpg
    # l64a,labs
    'lchown': [0xffffffff],
    # lsearch,lfind
    'linkat': [0xffffffff],
    'link': [0xffffffff],
    'lio_listio': [0xffffffff],
    'listen': [0xffffffff],
    # localeconv
    # localtime,localtime_r
    'lockf': [0xffffffff],
    # lseek
    'mblen': [0xffffffff],
    'mbrtowc': [0xffffffff,0xfffffffe],
    # mbsinit,mbsnrtowcs,mbsrtowcs,mbtowc
    'mkdir': [0xffffffff],
    'mkdirat': [0xffffffff],
    'mkdtemp': [0x0],
    'mkstemp': [0xffffffff],
    'mkfifo': [0xffffffff],
    'mkfifoat': [0xffffffff],
    'mknod': [0xffffffff],
    'mknodat': [0xffffffff],
    # mktime
    # mlockall,munlockall
    'mqclose': [0xffffffff],
    'mq_getattr': [0xffffffff],
    'mq_notify': [0xffffffff],
    'mq_open': [0xffffffff],
    'mq_receive': [0xffffffff],
    'mq_timedreceive': [0xffffffff],
    'mq_send': [0xffffffff],
    'mq_timedsend': [0xffffffff],
    'mq_unlink': [0xffffffff],
    'msgctl': [0xffffffff],
    'msgget': [0xffffffff],
    'msgrcv': [0xffffffff],
    'msgsnd': [0xffffffff],
    'msync': [0xffffffff],
    #nanosleep
    'newlocale': [0x0],
    # nftw
    # nice
    # nl_langinfo,nl_langinfo_l
    'openat': [0xffffffff],
    # optarg,opterr,optind,optopt
    # pause
    'pclose': [0xffffffff],
    'pipe': [0xffffffff],
    'poll': [0x0,0xffffffff],
    'popen': [0x0],
    'pread': [0xffffffff],
    'read': [0xffffffff],
    'pselect': [0xffffffff],
    'select': [0xffffffff],
    # ptsname
    # put*
    'pwrite': [0xffffffff],
    'readdir': [0x0],
    'readlink': [0xffffffff],
    'readlinkat': [0xffffffff],
    'readv': [0xffffffff],
    'realpath': [0x0],
    'recvfrom': [0xffffffff],
    'recv': [0xffffffff],
    'recvmsg': [0xffffffff],
    # regcomp,regerror,regexec,regfree
    'remove': [0xffffffff],
    'rename': [0xffffffff],
    'renameat': [0xffffffff],
    'rmdir': [0xffffffff],
    'scandir': [0xffffffff],
    # sched*
    'sem_close': [0xffffffff],
    'semctl': [0xffffffff],
    'sem_destroy': [0xffffffff],
    'semget': [0xffffffff],
    'sem_getvalue': [0xffffffff],
    'sem_init': [0xffffffff],
    'sem_open': [0xffffffff],
    'sem_failed': [0x0],
    'semop': [0xffffffff],
    'sem_post': [0xffffffff],
    'sem_timedwait': [0xffffffff],
    'sem_trywait': [0xffffffff],
    'sem_wait': [0xffffffff],
    'sem_unlink': [0xffffffff],
    'send': [0xffffffff],
    'sendmsg': [0xffffffff],
    'sendto': [0xffffffff],
    # setegid,seteuid,setgid,setpgid,setpgrp,setpriority,setregid,setreuid,setsid,setuid
    'setenv': [0xffffffff],
    # setjmp
    'setlocale': [0x0],
    'setsockopt': [0xffffffff],
    'shmat': [0xffffffff],
    'shmctl': [0xffffffff],
    'shmdt': [0xffffffff],
    'shmget': [0xffffffff],
    'shm_open': [0xffffffff],
    'shm_unlink': [0xffffffff],
    # sig*
    # sleep
    'sockatmark': [0xffffffff],
    'socket': [0xffffffff],
    'socketpair': [0xffffffff],
    # str*
    'symlink': [0xffffffff],
    'symlinkat': [0xffffffff],
    'sysconf': [0xffffffff],
    'system': [0xffffffff],
    'tcdrain': [0xffffffff],
    'tcflow': [0xffffffff],
    'tcflush': [0xffffffff],
    'tcgetattr': [0xffffffff],
    # tcgetpgrp,tcgetsid,tcsetpgrp
    'tcsendbreak': [0xffffffff],
    'tcsetattr': [0xffffffff],
    # tdelete,tfind,tsearch,twalk
    'telldir': [0xffffffff],
    'tempnam': [0x0],
    # time*
    'tmpfile': [0x0],
    'tmpnam': [0x0],
    # to*
    'truncate': [0xffffffff],
    'ttyname': [0xffffffff],
    # timezone,tzname,tzset
    #ulimit,umask
    'uname': [0xffffffff],
    # ungetc,ungetwc
    'unlink': [0xffffffff],
    'unlinkat': [0xffffffff],
    # unlockpt
    'unsetenv': [0xffffffff],
    # uselocale
    'utime': [0xffffffff],
    # va*
    'wait': [0xffffffff],
    'waitpid': [0xffffffff],
    # wcs*
    'write': [0xffffffff],
    # writev
    'fopen64': [0x0],

    # 12.13 added glob glob64
    'glob': [0x1,0x2,0x3],
    'glob64': [0x1,0x2,0x3],

    # 12.19 added shutdown,atoi
    'shutdown': [0xffffffff],
    'atoi':[0xffffffff]
}

# Kernel-provided User Helpers
kuser = {
    0xffff0ffc: '__kuser_helper_version',
    0xffff0fe0: '__kuser_get_tls',
    0xffff0fc0: '__kuser_cmpxchg',
    0xffff0fa0: '__kuser_memory_barrier',
    0xffff0f60: '__kuser_cmpxchg64'
    # 0xffff10a0: 'UNKNOWN_KERNEL_FUNC'
}

fine_returns = {
    'atexit': [0x0],
    'clock_getcpuclockid': [0x0],
    'clock_nanosleep': [0x0],
    'dlclose': [0x0],
    'dlerror': [0x0],
    'fgetpos': [0x0],
    'getaddrinfo': [0x0],
    'fsetpos': [0x0],
    'getgrgid': [0x0],
    'getgrgid_r': [0x0],
    'getgrnam': [0x0],
    'getgrnam_r': [0x0],
    'getlogin_r': [0x0],
    'getnameinfo': [0x0],
    'inet_pton': [0x1],
    'readdir_r': [0x0],
    'setvbuf': [0x0],
    'ttyname_r': [0x0],
    # 12.13: added glob
    'glob': [0x0],
    'glob64': [0x0],
    # huawei custom API
    'ATP_MSG_Init': [0x0],
}

err_funcs = [
    'abort',
    # erfcf,erfc,erfcl,erff,erf,erfl are not implemented yet.
    # and exit not exactly means error. could be a thread finished its job.
    # 'perror',
    'psiginfo',
    'psignal',
    'stderror',
    # '__errno_location'


]

ignore_funcs = [
    # 'strncmp',
    # 'strcmp',
    # 'strcasecmp',
    'strstr',
    'strchr',
    'strlen',
    'strrchr',
    'strpbrk',
    'strspn',
    '__aeabi_uldivmod'

]

# from errno.h
errno_dict = {
    1 :'EPERM',
    2 :'ENOENT',
    3 :'ESRCH',
    4 :'EINTR',
    5 :'EIO',
    6 :'ENXIO',
    7 :'E2BIG',
    8 :'ENOEXEC',
    9 :'EBADF',
    10 :'ECHILD',
    11 :'EAGAIN',
    12 :'ENOMEM',
    13 :'EACCES',
    14 :'EFAULT',
    15 :'ENOTBLK',
    16 :'EBUSY',
    17 :'EEXIST',
    18 :'EXDEV',
    19 :'ENODEV',
    20 :'ENOTDIR',
    21 :'EISDIR',
    22 :'EINVAL',
    23 :'ENFILE',
    24 :'EMFILE',
    25 :'ENOTTY',
    26 :'ETXTBSY',
    27 :'EFBIG',
    28 :'ENOSPC',
    29 :'ESPIPE',
    30 :'EROFS',
    31 :'EMLINK',
    32 :'EPIPE',
    33 :'EDOM',
    34 :'ERANGE',
    35 :'ENOMSG',
    36 :'EIDRM',
    37 :'ECHRNG',
    38 :'EL2NSYNC',
    39 :'EL3HLT',
    40 :'EL3RST',
    41 :'ELNRNG',
    42 :'EUNATCH',
    43 :'ENOCSI',
    44 :'EL2HLT',
    45 :'EDEADLK',
    46 :'ENOLCK',
    50 :'EBADE',
    51 :'EBADR',
    52 :'EXFULL',
    53 :'ENOANO',
    54 :'EBADRQC',
    55 :'EBADSLT',
    56 :'EDEADLOCK',
    57 :'EBFONT',
    60 :'ENOSTR',
    61 :'ENODATA',
    62 :'ETIME',
    63 :'ENOSR',
    64 :'ENONET',
    65 :'ENOPKG',
    66 :'EREMOTE',
    67 :'ENOLINK',
    68 :'EADV',
    69 :'ESRMNT',
    70 :'ECOMM',
    71 :'EPROTO',
    74 :'EMULTIHOP',
    75 :'ELBIN',
    76 :'EDOTDOT',
    77 :'EBADMSG',
    79 :'EFTYPE',
    80 :'ENOTUNIQ',
    81 :'EBADFD',
    82 :'EREMCHG',
    83 :'ELIBACC',
    84 :'ELIBBAD',
    85 :'ELIBSCN',
    86 :'ELIBMAX',
    87 :'ELIBEXEC',
    88 :'ENOSYS',
    89 :'ENMFILE',
    90 :'ENOTEMPTY',
    91 :'ENAMETOOLONG',
    92 :'ELOOP',
    95 :'EOPNOTSUPP',
    96 :'EPFNOSUPPORT',
    97 :'EAFNOSUPPORT',
    98 :'EADDRINUSE',
    99 :'EADDRNOTAVAIL',
    100 :'ENETDOWN',
    101 :'ENETUNREACH',
    102 :'ENETRESET',
    103 :'ECONNABORTED',
    104 :'ECONNRESET',
    105 :'ENOBUFS',
    106 :'EAFNOSUPPORT',
    107 :'EPROTOTYPE',
    108 :'ENOTSOCK',
    109 :'ENOPROTOOPT',
    110 :'ESHUTDOWN',
    111 :'ECONNREFUSED',
    112 :'EADDRINUSE',
    113 :'ECONNABORTED',
    114 :'ENETUNREACH',
    115 :'ENETDOWN',
    116 :'ETIMEDOUT',
    117 :'EHOSTDOWN',
    118 :'EHOSTUNREACH',
    119 :'EINPROGRESS',
    120 :'EALREADY',
    121 :'EDESTADDRREQ',
    122 :'EMSGSIZE',
    123 :'EPROTONOSUPPORT',
    124 :'ESOCKTNOSUPPORT',
    125 :'EADDRNOTAVAIL',
    126 :'ENETRESET',
    127 :'EISCONN',
    128 :'ENOTCONN',
    129 :'ETOOMANYREFS',
    130 :'EPROCLIM',
    131 :'EUSERS',
    132 :'EDQUOT',
    133 :'ESTALE',
    134 :'ENOTSUP',
    135 :'ENOMEDIUM',
    136 :'ENOSHARE',
    137 :'ECASECLASH',
    138 :'EILSEQ',
    139 :'EOVERFLOW',
}

def query_err(value):
    errno = 0x100000000 - value
    return errno_dict[errno]

def query_syscall(num):
    return syscalls[num]


# 0: normal return,1: error return,2: not sure
def check_call(callee,value):
    if callee in ignore_funcs:
        return 0
    if callee in err_funcs:
        return 1
    if callee in fine_returns:
        if value in fine_returns[callee]:
            return 0
        # else:
        #     return 0
    if callee in err_returns:
        if value in err_returns[callee]:
            return 1
        elif value in range(0xffffff74,0xffffffff):
            errno = 0x100000000 - value
            print("errno " + str(errno) + " " + errno_dict[errno])
            return 1
        else:
            return 0
        # else:
        #     return 0
    else:
        if value in range(0x7fffffff,0x100000000):
            return 1
        elif value > 0x0:
            return 0
        else:
            return 2
        


ffi.cdef("""
struct sockaddr {
    uint16_t sa_family;
    char     sa_data[14];
    ...;
};
struct in_addr {
    uint32_t s_addr;
    ...;
};
struct sockaddr_in {
    short   sin_family;
    unsigned short  sin_port;
    struct in_addr sin_addr;
    ...;
};
struct sockaddr_un {
    uint16_t sun_family;
    char    sun_path[108];
    ...;
};
struct sockaddr_in6 {
    uint16_t     sin6_family;
    unsigned short      sin6_port;
    uint32_t        sin6_flowinfo;
    struct in6_addr sin6_addr;
    uint32_t        sin6_scope_id;
    ...;
};
struct in6_addr {
    unsigned char   s6_addr[16];
    ...;
};
""")
lib = ffi.verify("""
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
""")



common_apis = {
    'access' : ['string','concrete'],
    'fclose' : ['ptr'],
    'fflush' : ['file'],
    'fgetc' : ['file'],
    'fgets' : ['string','concrete','file'],
    'fopen' : ['string','string'],
    'fputc' : ['concrete','file'],
    'fputs' : ['string','file'],
    'fread' : ['ptr','concrete','concrete','file'],
    'fscanf' : ['file','string'],
    'fseek' : ['file','concrete','concrete'],
    'ftell' : ['file'],
    'fprintf' : ['file','string'],
    'fwrite' : ['ptr','concrete','concrete','file'],
    'getchar' : [],
    'malloc' : ['concrete'],
    'memcmp' : ['ptr','ptr','concrete'],
    'memcpy' : ['ptr','ptr','concrete'],
    'memset' : ['ptr','concrete','concrete'],
    'openlog' : ['string','concrete','concrete'],
    'perror' : ['string'],
    'printf' : ['string'],
    'putc' : ['concrete','file'],
    'putchar' : ['concrete'],
    'rand' : [],
    'realloc' : ['ptr','concrete'],
    'rewind' : ['file'],
    'scanf' : ['string'],
    'setvbuf' : ['fd','string','concrete','concrete'],
    'snprintf' : ['string','concrete','string'],
    'srand' : ['concrete'],
    'sscanf' : ['string','string'],
    'strcat' : ['string','string'],
    'strncat' : ['string','string','concrete'],
    'strchr' : ['string','concrete'],
    'strrchr' : ['string','concrete'],
    'strcmp' : ['string','string'],
    'strncmp' : ['string','string','concrete'],
    'strcpy' : ['ptr','string'],
    'strncpy' : ['ptr','string','concrete'],
    'strstr' : ['string','string'],
    'strcasestr' : ['string','string'],
    'strtol' : ['string','ptr','concrete'],
    'system' : ['string'],
    'tmpnam' : ['string'],
    'tolower' : ['concrete'],
    'toupper' : ['concrete'],
    'ungetc' : ['concrete','file'],
    'vsprintf' : ['string','string','concrete'],
    'brk' : ['ptr'],
    'cwd' : [],
    'fstat' : ['fd','ptr'],
     # 'futex' : ['ptr','concrete','concrete','ptr','ptr','concrete'],
    'getpid' : [],
    'gettid' : [],
    'getrlimit' : ['concrete','ptr'],
    'readv' : ['fd','ptr','concrete'],
    'writev' : ['fd','ptr','concrete'],
    'lseek' : ['fd','concrete','concrete'],
    # mmap
    'mprotect' : ['ptr','concrete','concrete'],
    'munmap' : ['ptr','concrete'],
    'set_tid_address' : ['ptr'],
    'sigaction' : ['concrete','ptr','ptr'],
    'sigprocmask' : ['concrete','ptr','ptr'],
    'stat' : ['string','ptr'],
    'lstat' : ['string','ptr'],
    'fstatat' : ['fd','string','ptr','concrete'],
    '__xstat' : ['concrete','string','ptr'],
    '__lxstat' : ['concrete','string','ptr'],
    '__fxstat' : ['concrete','fd','ptr'],
    '__xstat64' : ['concrete','string','ptr'],
    '__lxstat64' : ['concrete','string','ptr'],
    '__fxstat64' : ['concrete','fd','ptr'],
    'sysinfo' : ['ptr'],
    'tgkill' : ['concrete','concrete','concrete'],
    'time' : ['ptr'],
    'getuid' : [],
    'getgid' : [],
    'uname' : ['ptr'],
    'unlink' : ['string'],
    'accept' : ['fd','ptr','ptr'],
    'bind' : ['fd','sockaddr','concrete'],
    'close' : ['fd'],
    'closedir' : ['fd'],
    'dup' : ['fd'],
    'dup2' : ['fd','concrete'],
    'dup3' : ['fd','concrete','concrete'],
    'fcntl' : ['fd','concrete'],
    'fdopen' : ['fd','string'],
    'fileno' : ['fd'],
    'fork' : [],
    'gethostbyname' : ['string'],
    'gethostbyaddr' : ['ptr','concrete','concrete'],
    'getpass' : ['string'],
    'getsockopt' : ['fd','concrete','concrete','ptr','ptr'],
    'htonl' : ['concrete'],
    'htons' : ['concrete'],
    'inet_ntoa' : ['ptr'],
    'inet_aton' : ['string','ptr'],
    'listen' : ['fd','concrete'],
    'open' : ['string','concrete'],
    'open64' : ['string','concrete'],
    'opendir' : ['string'],
    'pread' : ['fd','ptr','concrete','concrete'],
    'pread64' : ['fd','ptr','concrete','concrete'],
    'pwrite' : ['fd','ptr','concrete','concrete'],
    'pwrite64' : ['fd','ptr','concrete','concrete'],
    'read' : ['fd','ptr','concrete'],
    'readdir' : ['fd'],
    'recv': ['fd','ptr','concrete','concrete'],
    'recvfrom' : ['fd','ptr','concrete','concrete','ptr','ptr'],
    'select' : ['concrete','ptr','ptr','ptr','ptr'],
    'poll' : ['ptr','concrete','concrete'],
    'send' : ['fd','ptr','concrete','concrete'],
    'setsockopt' : ['fd','concrete','concrete','ptr','concrete'],
    'gettimeofday' : ['ptr','ptr'],
    'sleep' : ['concrete'],
    'socket' : ['concrete','concrete','concrete'],
    'strdup' : ['string'],
    'write' : ['fd','ptr','concrete'],
    # 12.13 added glob
    'glob' : ['string','concrete','ptr','ptr'],
    'glob64' : ['string','concrete','ptr','ptr'],
    'atoi': ['string'],
    'connect': ['concrete', 'sockaddr', 'concrete'],
    # custom HUAWEI API
    'ATP_MSG_Init': ['concrete']
}

fd_op_funcs = ['open','dup','dup2','dup3','socket','connect','bind','setsockopt','creat','close']

err_returns = {
    'accept': [0xffffffff],
    'access': [0xffffffff],
    'faccessat': [0xffffffff],
    'aio_cancel': [0xffffffff],
    'aio_error': [0xffffffff],
    'aio_fsync': [0xffffffff],
    'aio_read': [0xffffffff],
    'aio_return': [0xffffffff],
    'aio_suspend': [0xffffffff],
    'aio_write': [0xffffffff],
    'asctime': [0x0],
    'asctime_r': [0x0],
    'bind': [0xffffffff],
    'bsearch': [0x0],
    'calloc': [0x0],
    'catclose': [0xffffffff],
    'catopen': [0xffffffff],
    'cfsetospeed': [0xffffffff],
    'cfsetispeed': [0xffffffff],
    'chdir': [0xffffffff],
    'chmod': [0xffffffff],
    'chown': [0xffffffff],
    'clock_getres': [0xffffffff],
    'clock_gettime': [0xffffffff],
    'clock_settime': [0xffffffff],
    'closedir': [0xffffffff],
    'close': [0xffffffff],
    'confstr': [0x0],
    'connect': [0xffffffff],
    'creat': [0xffffffff],
    'open': [0xffffffff],
    'crypt': [0x0],
    'ctime': [0xffffffff],
    'ctime_r': [0x0],
    'dbm_store': range(0x7fffffff,0x100000000),
    'dbm_error': range(0x0,0xffffffff),
    'dbm_open': [0x0],
    'dbm_firstkey': [0x0],
    'dbm_nextkey': [0x0],
    'dbm_fetch': [0x0],
    'dirfd': [0xffffffff],
    'dlopen': [0x0],
    'dlsym': [0x0],
    'dprintf': range(0x7fffffff,0x100000000),
    'fprintf': range(0x7fffffff,0x100000000),
    'printf': range(0x7fffffff,0x100000000),
    'sprintf': range(0x7fffffff,0x100000000),
    'snprintf': range(0x7fffffff,0x100000000),
    'dup': [0xffffffff],
    'dup2': [0xffffffff],
    'duplocate': [0x0],
    'getgrent': [0x0],
    'gethostent': [0x0],
    'getnetbyaddr': [0x0],
    'getnetbyname': [0x0],
    'getnetent': [0x0],
    'getpwent': [0x0],
    'getservbyname': [0x0],
    'getservbyport': [0x0],
    'getservent': [0x0],
    'getutxent': [0x0],
    'getutxid': [0x0],
    'getutxline': [0x0],
    'puttuxline': [0x0],
    'execl': [0xffffffff],
    'execle': [0xffffffff],
    'execlp': [0xffffffff],
    'execv': [0xffffffff],
    'execve': [0xffffffff],
    'execvp': [0xffffffff],
    'fexecve': [0xffffffff],
    'fattach': [0xffffffff],
    'fchdir': [0xffffffff],
    'fchmodat': [0xffffffff],
    'fchmod': [0xffffffff],
    'fchownat': [0xffffffff],
    'fchown': [0xffffffff],
    'fclose': [0xffffffff],
    'fcntl': [0xffffffff],
    'fdatasync': [0xffffffff],
    'fdetach': [0xffffffff],
    'fdopendir': [0x0],
    'opendir': [0x0],
    'fdopen': [0x0],
    # fflush not implemented
    # fgets,fgetwc,fgetws not implemented
    'fileno': [0xffffffff],
    # flockfile,ftrylockfile,funlockfile not implemented
    # fmemopen
    # fnmatch not implemented yet but could be useful
    'fopen':[0x0],
    # we probably don't need fork
    'pathconf': [0xffffffff],
    'fpathconf': [0xffffffff],
    # fread
    'freopen': [0x0],
    'fscanf': [0,0xffffffff],
    'scanf': [0,0xffffffff],
    'sscanf': [0,0xffffffff],
    'fseek': [0xffffffff],
    'fseeko': [0xffffffff],
    'fstatat': [0xffffffff],
    'lstat': [0xffffffff],
    'stat': [0xffffffff],
    'stat64': [0xffffffff],
    'fstat': [0xffffffff],
    'fstatvfs': [0xffffffff],
    'statvfs': [0xffffffff],
    # fsync
    'ftell': [0xffffffff],
    'ftello': [0xffffffff],
    'ftok': [0xffffffff],
    'ftruncate': [0xffffffff],
    # ftw
    'futimens': [0xffffffff],
    'utimensat': [0xffffffff],
    'utimes': [0xffffffff],
    # fwide,fwprintf,swprintf,wprintf
    # fwrite,fputc
    'getcwd': [0x0],
    # getdate
    'getdelim': [0xffffffff],
    'getline': [0xffffffff],
    'getenv': [0x0],
    'getgroups': [0xffffffff],
    #endhostent,gethostent,sethostent
    'gethostname': [0xffffffff],
    'getitimer': [0xffffffff],
    'setitimer': [0xffffffff],
    #getdelim,getline
    'getlogin': [0x0],
    'getmsg': [0xffffffff],
    'getpmsg': [0xffffffff],
    # getopt
    'getpeername': [0xffffffff],
    'getpgid': [0xffffffff],
    #getpriority,setpriority
    'getprotobyname': [0x0],
    'getprotobynumber': [0x0],
    'getprotoent': [0x0],
    # getpwnam,getpwnam_r,getpwuid,getpwuid_r
    'getrlimit': [0xffffffff],
    'setrlimit': [0xffffffff],
    'getrusage' : [0xffffffff],
    # getc
    'getsid': [0xffffffff],
    'getsockname': [0xffffffff],
    'getsockopt': [0xffffffff],
    # getsubopt
    'pututxline': [0x0],
    # getwc,getwchar
    # glob,globfree
    # gmtime,gmtime_r
    # grantpt
    # hcreate,hdestroy,hsearch
    'iconv_close': [0xffffffff],
    'iconv': [0xffffffff],
    'iconv_open': [0xffffffff],
    'inet_addr': [0xffffffff],
    'inet_ntoa': [0x0],
    'inet_ntop': [0x0],
    'ioctl': [0xffffffff],
    # is* functions
    # kill,killpg
    # l64a,labs
    'lchown': [0xffffffff],
    # lsearch,lfind
    'linkat': [0xffffffff],
    'link': [0xffffffff],
    'lio_listio': [0xffffffff],
    'listen': [0xffffffff],
    # localeconv
    # localtime,localtime_r
    'lockf': [0xffffffff],
    # lseek
    'mblen': [0xffffffff],
    'mbrtowc': [0xffffffff,0xfffffffe],
    # mbsinit,mbsnrtowcs,mbsrtowcs,mbtowc
    'mkdir': [0xffffffff],
    'mkdirat': [0xffffffff],
    'mkdtemp': [0x0],
    'mkstemp': [0xffffffff],
    'mkfifo': [0xffffffff],
    'mkfifoat': [0xffffffff],
    'mknod': [0xffffffff],
    'mknodat': [0xffffffff],
    # mktime
    # mlockall,munlockall
    'mqclose': [0xffffffff],
    'mq_getattr': [0xffffffff],
    'mq_notify': [0xffffffff],
    'mq_open': [0xffffffff],
    'mq_receive': [0xffffffff],
    'mq_timedreceive': [0xffffffff],
    'mq_send': [0xffffffff],
    'mq_timedsend': [0xffffffff],
    'mq_unlink': [0xffffffff],
    'msgctl': [0xffffffff],
    'msgget': [0xffffffff],
    'msgrcv': [0xffffffff],
    'msgsnd': [0xffffffff],
    'msync': [0xffffffff],
    #nanosleep
    'newlocale': [0x0],
    # nftw
    # nice
    # nl_langinfo,nl_langinfo_l
    'openat': [0xffffffff],
    # optarg,opterr,optind,optopt
    # pause
    'pclose': [0xffffffff],
    'pipe': [0xffffffff],
    'poll': [0x0,0xffffffff],
    'popen': [0x0],
    'pread': [0xffffffff],
    'read': [0xffffffff],
    'pselect': [0xffffffff],
    'select': [0xffffffff],
    # ptsname
    # put*
    'pwrite': [0xffffffff],
    'readdir': [0x0],
    'readlink': [0xffffffff],
    'readlinkat': [0xffffffff],
    'readv': [0xffffffff],
    'realpath': [0x0],
    'recvfrom': [0xffffffff],
    'recv': [0xffffffff],
    'recvmsg': [0xffffffff],
    # regcomp,regerror,regexec,regfree
    'remove': [0xffffffff],
    'rename': [0xffffffff],
    'renameat': [0xffffffff],
    'rmdir': [0xffffffff],
    'scandir': [0xffffffff],
    # sched*
    'sem_close': [0xffffffff],
    'semctl': [0xffffffff],
    'sem_destroy': [0xffffffff],
    'semget': [0xffffffff],
    'sem_getvalue': [0xffffffff],
    'sem_init': [0xffffffff],
    'sem_open': [0xffffffff],
    'sem_failed': [0x0],
    'semop': [0xffffffff],
    'sem_post': [0xffffffff],
    'sem_timedwait': [0xffffffff],
    'sem_trywait': [0xffffffff],
    'sem_wait': [0xffffffff],
    'sem_unlink': [0xffffffff],
    'send': [0xffffffff],
    'sendmsg': [0xffffffff],
    'sendto': [0xffffffff],
    # setegid,seteuid,setgid,setpgid,setpgrp,setpriority,setregid,setreuid,setsid,setuid
    'setenv': [0xffffffff],
    # setjmp
    'setlocale': [0x0],
    'setsockopt': [0xffffffff],
    'shmat': [0xffffffff],
    'shmctl': [0xffffffff],
    'shmdt': [0xffffffff],
    'shmget': [0xffffffff],
    'shm_open': [0xffffffff],
    'shm_unlink': [0xffffffff],
    # sig*
    # sleep
    'sockatmark': [0xffffffff],
    'socket': [0xffffffff],
    'socketpair': [0xffffffff],
    # str*
    'symlink': [0xffffffff],
    'symlinkat': [0xffffffff],
    'sysconf': [0xffffffff],
    'system': [0xffffffff],
    'tcdrain': [0xffffffff],
    'tcflow': [0xffffffff],
    'tcflush': [0xffffffff],
    'tcgetattr': [0xffffffff],
    # tcgetpgrp,tcgetsid,tcsetpgrp
    'tcsendbreak': [0xffffffff],
    'tcsetattr': [0xffffffff],
    # tdelete,tfind,tsearch,twalk
    'telldir': [0xffffffff],
    'tempnam': [0x0],
    # time*
    'tmpfile': [0x0],
    'tmpnam': [0x0],
    # to*
    'truncate': [0xffffffff],
    'ttyname': [0xffffffff],
    # timezone,tzname,tzset
    #ulimit,umask
    'uname': [0xffffffff],
    # ungetc,ungetwc
    'unlink': [0xffffffff],
    'unlinkat': [0xffffffff],
    # unlockpt
    'unsetenv': [0xffffffff],
    # uselocale
    'utime': [0xffffffff],
    # va*
    'wait': [0xffffffff],
    'waitpid': [0xffffffff],
    # wcs*
    'write': [0xffffffff],
    # writev
    'fopen64': [0x0],

    # 12.13 added glob glob64
    'glob': [0x1,0x2,0x3],
    'glob64': [0x1,0x2,0x3],

    # 12.19 added shutdown,atoi
    'shutdown': [0xffffffff],
    'atoi':[0xffffffff],
}

# Kernel-provided User Helpers
kuser = {
    0xffff0ffc: '__kuser_helper_version',
    0xffff0fe0: '__kuser_get_tls',
    0xffff0fc0: '__kuser_cmpxchg',
    0xffff0fa0: '__kuser_memory_barrier',
    0xffff0f60: '__kuser_cmpxchg64'
    # 0xffff10a0: 'UNKNOWN_KERNEL_FUNC'
}

fine_returns = {
    'atexit': [0x0],
    'clock_getcpuclockid': [0x0],
    'clock_nanosleep': [0x0],
    'dlclose': [0x0],
    'dlerror': [0x0],
    'fgetpos': [0x0],
    'getaddrinfo': [0x0],
    'fsetpos': [0x0],
    'getgrgid': [0x0],
    'getgrgid_r': [0x0],
    'getgrnam': [0x0],
    'getgrnam_r': [0x0],
    'getlogin_r': [0x0],
    'getnameinfo': [0x0],
    'inet_pton': [0x1],
    'readdir_r': [0x0],
    'setvbuf': [0x0],
    'ttyname_r': [0x0],
    # 12.13: added glob
    'glob': [0x0],
    'glob64': [0x0],
    'ATP_MSG_Init': [0x0]
    
}

err_funcs = [
    'abort',
    # erfcf,erfc,erfcl,erff,erf,erfl are not implemented yet.
    # and exit not exactly means error. could be a thread finished its job.
    # 'perror',
    'psiginfo',
    'psignal',
    'stderror',
    # '__errno_location'


]

ignore_funcs = [
    # 'strncmp',
    # 'strcmp',
    # 'strcasecmp',
    'strstr',
    'strchr',
    'strlen',
    'strrchr',
    'strpbrk',
    'strspn',
    '__aeabi_uldivmod'

]

# from errno.h
errno_dict = {
    1 :'EPERM',
    2 :'ENOENT',
    3 :'ESRCH',
    4 :'EINTR',
    5 :'EIO',
    6 :'ENXIO',
    7 :'E2BIG',
    8 :'ENOEXEC',
    9 :'EBADF',
    10 :'ECHILD',
    11 :'EAGAIN',
    12 :'ENOMEM',
    13 :'EACCES',
    14 :'EFAULT',
    15 :'ENOTBLK',
    16 :'EBUSY',
    17 :'EEXIST',
    18 :'EXDEV',
    19 :'ENODEV',
    20 :'ENOTDIR',
    21 :'EISDIR',
    22 :'EINVAL',
    23 :'ENFILE',
    24 :'EMFILE',
    25 :'ENOTTY',
    26 :'ETXTBSY',
    27 :'EFBIG',
    28 :'ENOSPC',
    29 :'ESPIPE',
    30 :'EROFS',
    31 :'EMLINK',
    32 :'EPIPE',
    33 :'EDOM',
    34 :'ERANGE',
    35 :'ENOMSG',
    36 :'EIDRM',
    37 :'ECHRNG',
    38 :'EL2NSYNC',
    39 :'EL3HLT',
    40 :'EL3RST',
    41 :'ELNRNG',
    42 :'EUNATCH',
    43 :'ENOCSI',
    44 :'EL2HLT',
    45 :'EDEADLK',
    46 :'ENOLCK',
    50 :'EBADE',
    51 :'EBADR',
    52 :'EXFULL',
    53 :'ENOANO',
    54 :'EBADRQC',
    55 :'EBADSLT',
    56 :'EDEADLOCK',
    57 :'EBFONT',
    60 :'ENOSTR',
    61 :'ENODATA',
    62 :'ETIME',
    63 :'ENOSR',
    64 :'ENONET',
    65 :'ENOPKG',
    66 :'EREMOTE',
    67 :'ENOLINK',
    68 :'EADV',
    69 :'ESRMNT',
    70 :'ECOMM',
    71 :'EPROTO',
    74 :'EMULTIHOP',
    75 :'ELBIN',
    76 :'EDOTDOT',
    77 :'EBADMSG',
    79 :'EFTYPE',
    80 :'ENOTUNIQ',
    81 :'EBADFD',
    82 :'EREMCHG',
    83 :'ELIBACC',
    84 :'ELIBBAD',
    85 :'ELIBSCN',
    86 :'ELIBMAX',
    87 :'ELIBEXEC',
    88 :'ENOSYS',
    89 :'ENMFILE',
    90 :'ENOTEMPTY',
    91 :'ENAMETOOLONG',
    92 :'ELOOP',
    95 :'EOPNOTSUPP',
    96 :'EPFNOSUPPORT',
    97 :'EAFNOSUPPORT',
    98 :'EADDRINUSE',
    99 :'EADDRNOTAVAIL',
    100 :'ENETDOWN',
    101 :'ENETUNREACH',
    102 :'ENETRESET',
    103 :'ECONNABORTED',
    104 :'ECONNRESET',
    105 :'ENOBUFS',
    106 :'EAFNOSUPPORT',
    107 :'EPROTOTYPE',
    108 :'ENOTSOCK',
    109 :'ENOPROTOOPT',
    110 :'ESHUTDOWN',
    111 :'ECONNREFUSED',
    112 :'EADDRINUSE',
    113 :'ECONNABORTED',
    114 :'ENETUNREACH',
    115 :'ENETDOWN',
    116 :'ETIMEDOUT',
    117 :'EHOSTDOWN',
    118 :'EHOSTUNREACH',
    119 :'EINPROGRESS',
    120 :'EALREADY',
    121 :'EDESTADDRREQ',
    122 :'EMSGSIZE',
    123 :'EPROTONOSUPPORT',
    124 :'ESOCKTNOSUPPORT',
    125 :'EADDRNOTAVAIL',
    126 :'ENETRESET',
    127 :'EISCONN',
    128 :'ENOTCONN',
    129 :'ETOOMANYREFS',
    130 :'EPROCLIM',
    131 :'EUSERS',
    132 :'EDQUOT',
    133 :'ESTALE',
    134 :'ENOTSUP',
    135 :'ENOMEDIUM',
    136 :'ENOSHARE',
    137 :'ECASECLASH',
    138 :'EILSEQ',
    139 :'EOVERFLOW',
}

# Usually, the configuration-parsing process is long and useless for us. 
# If we can identity these functions, we can just purge the related bbs to reduce the trace. 
preprocess_skip_funcs = [
    'config_read', # lighttpd
    'buffer_init', # lighttpd
    'ngx_log_error_core', # nginx
    'log_error_write' #lighttpd
]

def parse_sockaddr(data_raw):
    data = ffi.from_buffer("struct sockaddr *", data_raw)
    if data.sa_family == 2: # AF_INET
        sockaddr_in = ffi.from_buffer("struct sockaddr_in *", data_raw)
        addr = socket.inet_ntoa(struct.pack('=I',sockaddr_in.sin_addr.s_addr))
        port = socket.ntohs(sockaddr_in.sin_port)
        return ("AF_INET",addr,port)
    elif data.sa_family == 1: # AF_UNIX
        # this requires test!
        sockaddr_un = ffi.from_buffer("struct sockaddr_un *", data_raw)
        path = ffi.string(sockaddr_un.sun_path)
        return ("AF_UNIX",path)
    elif data.sa_family == 10: # AF_INET6
        sockaddr_in6 = ffi.from_buffer("struct sockaddr_in6 *", data_raw)
        addr = sockaddr_in6.sin6_addr
        port = socket.ntohs(sockaddr_in6.sin6_port)
        return ("AF_INET6",addr,port)
    else:
        print("\033[33m[!]\033[0m parse_sockaddr: unsupported socket type: %d" %data.sa_family)
        return None

structs = {'sockaddr':'struct sockaddr_un *'}

struct_parsers = {'sockaddr':parse_sockaddr}

def parse_val(panda, cpu, val):
    if (panda == None) or (cpu == None):
        # print("\033[33m[!]\033[0m empty panda/cpu, unable to parse this value.")
        return val
    for _ in range(5):
        if val == 0:
            return val
        try:
            str_data = panda.virtual_memory_read(cpu, val, 64)
        except ValueError:
            return val
        str_val = ""
        for d in str_data:
            if d >= 0x20 and d < 0x7F:
                str_val += chr(d)
            else:
                break
        if len(str_val) > 2:
            return format(str_val)
        return val

def parse_val_legacy(proj, simstate, addr):
    '''
    infer data from angr's memory model. use this when panda isn't available
    '''
    def is_code(proj,addr):
        return True if (proj.get_block(addr).size > 0) else False
    def is_vaild_ptr(addr):
        return True if (addr in range(0x8000,0xc0000000)) else False
    def is_vaild_ptr_in_program(addr):
        return True if proj.loader.find_object_containing(addr) != None else False
    if proj.loader.main_object.sections_map != {}:
        got_offset = proj.loader.main_object.sections_map['.got'].vaddr
    else:
        print("[!] this program has no section map, cannot get information about this...")
        return None
    if simstate == None:
        simstate = proj.angr_proj.factory.entry_state(add_options={angr.options.ZERO_FILL_UNCONSTRAINED_MEMORY,angr.options.ZERO_FILL_UNCONSTRAINED_REGISTERS})
    if addr < 0x8000:
        return addr # regard this as a concrete value.
    elif proj.loader.find_section_containing(addr) == None:
        if is_vaild_ptr(addr):
            val = simstate.mem[addr].long.concrete
            if val != 0:
                return val
            else:
                return None
        else:
            return None      
    elif proj.loader.find_section_containing(addr).name == '.plt':
        if addr in proj.symbol_dict:
            name = proj.symbol_dict[addr].name
        return '<.plt: %s>' %name
    elif proj.loader.find_section_containing(addr).name == '.text':
        if is_code(proj,addr) :
            return addr
        elif is_vaild_ptr(addr):
            target = simstate.mem[addr].long.concrete
            if (target == 0) or (target == None):
                return addr
            else:
                parse_val_legacy(proj,simstate,target)
        else:
            return None
    elif proj.loader.find_section_containing(addr).name == '.got':
        if is_vaild_ptr_in_program(addr):
            ptr = simstate.mem[addr].long
            if ptr.concrete > 0xc0000000:
                target = ptr.concrete + got_offset
            else:
                target = got_offset - ptr.concrete
            parse_val_legacy(proj,simstate,target)
        else:
            return None
    elif proj.loader.find_section_containing(addr).name == '.rodata':
        con = simstate.mem[addr].string.concrete
        return con
    elif proj.loader.find_section_containing(addr).name == '.bss':
        return addr
    elif proj.loader.find_section_containing(addr).name == '.data':
        if simstate.mem[addr].long.concrete == 0 or simstate.mem[addr].long.concrete == None:
            return None
        else:
            # char_var = state.mem[addr].char.concrete
            # string_var = state.mem[addr].string.concrete
            hex_var = simstate.mem[addr].uint32_t.concrete
            return hex_var
    else:
        if is_vaild_ptr(addr):
            val = simstate.mem[addr].long.concrete
            return val
        else:
            return None

class Arg(object):
    def __init__(self, typ, val, data = None, panda = None, cpu = None, fds = None):
        self.type = typ
        self.val = val # the corresponding register value
        self.unsolved = None
        self.data = None
        if data != None:
            self.data == data
        elif self.type == 'concrete':
            self.data = val
        elif self.type == 'ptr':
            self.data = val
        elif self.type == 'file':
            self.data = val
        elif self.type == 'string':
            if panda != None:
                self.data = parse_val(panda,cpu,val)
                if self.data == self.val: # failed to infer string
                    self.data = None
            else:
                self.data = None
        elif self.type == 'fd':
            if (fds != None) and (val in fds):
                fname = fds[val]
                self.data = (val,fname)
            elif panda != None:
                print("pc: 0x%x" % cpu.env_ptr.regs[15])
                proc = panda.plugins['osi'].get_current_process(cpu)
                fname_ptr = panda.plugins['osi_linux'].osi_linux_fd_to_filename(cpu, proc, val)
                fname = ffi.string(fname_ptr).decode("utf8","ignore") if fname_ptr != ffi.NULL else "<unknown>"
                self.data = (val,fname)
        elif self.type in structs:
            if panda != None:
                data_raw = panda.virtual_memory_read(cpu, val, ffi.sizeof(structs[self.type]))
                self.data = struct_parsers[self.type](data_raw)
                self.unsolved = ffi.from_buffer("struct sockaddr *", data_raw)
            else:
                self.data = None
        else:
            if panda != None:
                self.data = parse_val(panda, cpu, val)
            else:
                self.data = None
    def __repr__(self):
        if self.data == None:
            return '<%x: unknown>' %self.val
        else:
            if isinstance(self.data,str): # string
                return format(self.data)
            elif isinstance(self.data,bytes): # also string
                return str(self.data, encoding='utf-8')
            elif (self.type == 'concrete'): # concrete
                return format(self.data)
            elif isinstance(self.data, tuple): # fd or struct
                return format(self.data)
            else:
                return hex(self.data)

class CallInfo(object):
    def __init__(self, addr, retaddr, pos, name, stackid, args, callstack, functions, incomplete = False):
        self.func = addr
        self.retaddr = retaddr
        self.pos = pos
        self.stackid = stackid
        self.name = name
        self.args = args
        self.callstack = callstack
        self.functions = functions
        self.depth = len(self.callstack)
        self.incomplete = incomplete
    def __repr__(self):
        args_str = ""
        if (self.args != None):
            if (self.incomplete == False):
                for i in range(0, len(self.args)):
                    args_str = args_str + repr(self.args[i])
                    if (i != len(self.args) - 1):
                        args_str = args_str + ','
            else:
                for i in range(0, len(self.args)):
                    args_str = args_str + repr(self.args[i])
                    args_str = args_str + ','
                args_str = args_str + '...'           
        return self.name + '(' + args_str + ')'

def new_callinfo(proj, addr, retaddr, stackid, pos = None):
    if pos == None:
        pos = proj.get_rr_instr()
    if addr in proj.symbol_dict:
        name = proj.symbol_dict[addr].name
    else:
        name = "sub_%x" %addr
    args = parse_args(proj.panda, proj.panda.get_cpu(), name, proj.db.get_fds(pos, stackid))
    callstack = proj.program_state.get_callstack(pos)
    functions = proj.program_state.get_functions(pos)
    return CallInfo(addr, retaddr, pos, name, stackid, args, callstack, functions)

def new_callinfo_from_state(proj, state):
    addr = state.pc
    retaddr = state.lr
    pos = state.pos
    stackid = state.stackid
    if addr in proj.symbol_dict:
        name = proj.symbol_dict[addr].name
    else:
        name = "sub_%x" %addr
    if name in common_apis:
        args = []
        arg_format = common_apis[name]
        for i in range(max(len(arg_format),4)):
            val = state.regs[i]
            try:
                new_arg = Arg(arg_format[i], val, proj.db.get_fds(pos, stackid))
                args.append(new_arg)
            except IndexError:
                pass            
        if len(arg_format) > 4:
            for i in range(4, len(arg_format)):
                new_arg = Arg('unknown', None)
                args.append(new_arg)
    else:
        args = []
        for i in range(0, 4):
            val = state.regs[i] 
            new_arg = Arg('unknown', val)
            args.append(new_arg)
    callstack = proj.program_state.get_callstack(pos)
    functions = proj.program_state.get_functions(pos)
    return CallInfo(addr, retaddr, pos, name, stackid, args, callstack, functions)

class RetInfo(object):
    def __init__(self, callinfo, val, ret_type, stackid, pos, data, callstack, functions):
        self.callinfo = callinfo
        self.func = self.callinfo.func
        self.retaddr = self.callinfo.retaddr
        self.val = val
        self.type = ret_type # currently not used
        self.stackid = stackid
        self.pos = pos
        self.data = data
        self.callstack = callstack
        self.functions = functions
        self.depth = len(self.callstack)
        self.name = self.callinfo.name
    def __repr__(self):
        return repr(self.callinfo) + " = " + hex(self.val)

def new_retinfo(proj, callinfo, val, ret_type, stackid, pos = None, parse_data = True):
    if pos == None:
        pos = proj.get_rr_instr()
    if parse_data == True:
        data = parse_val(proj.panda, proj.panda.get_cpu(), val)
    else:
        data = val
    callstack = proj.program_state.get_callstack(pos)
    callstack.pop()
    functions = proj.program_state.get_functions(pos)
    functions.pop()
    return RetInfo(callinfo, val, ret_type, stackid, pos, data, callstack, functions)

def parse_args(panda, cpu, name, fds = None):
    def get_arg(panda, cpu, index):
        if (index <= 3):
            val = cpu.env_ptr.regs[i]
        else:
            raw = panda.virtual_memory_read(cpu, panda.current_sp(cpu) + 4 * (i - 4), 4)
            val = struct.unpack('<L', bytes(raw))[0]
        return val
    if name in common_apis:
        args = []
        arg_format = common_apis[name]
        for i in range(0,len(arg_format)):
            val = get_arg(panda, cpu, i)
            new_arg = Arg(arg_format[i], val, panda = panda, cpu = cpu, fds = fds)
            args.append(new_arg)
    else:
        args = []
        for i in range(0,4):
            val = get_arg(panda, cpu, i)
            new_arg = Arg('unknown', val, panda = panda, cpu = cpu, fds = fds)
            args.append(new_arg)
    return args

def parse_args_legacy(state, name, proj, fds = None, simstate = None):
    def get_arg(state, index):
        if (index <= 3):
            val = state.regs[i]
        else:
            if (simstate):
                val = simstate.mem[addr].long.concrete
            else:
                return 0
        return val
    if name in common_apis:
        args = []
        arg_format = common_apis[name]
        for i in range(0, len(arg_format)):
            val = get_arg(state, i)
            new_arg = Arg(arg_format[i], val, proj = proj, state = state, fds = fds, simstate = simstate)
            args.append(new_arg)
    else:
        args = []
        for i in range(0, 4):
            val = get_arg(state, i)
            new_arg = Arg('unknown', val, proj = proj, state = state, fds = fds, simstate = simstate)
            args.append(new_arg)
    return args

# 0: normal return,1: error return,2: not sure
def check_call(callee,value):
    if callee in ignore_funcs:
        return 0
    if callee in err_funcs:
        return 1
    if callee in fine_returns:
        if value in fine_returns[callee]:
            return 0
    if callee in err_returns:
        if value in err_returns[callee]:
            return 1
        elif value in range(0xffffff74,0xffffffff):
            errno = 0x100000000 - value
            print("errno " + str(errno) + " " + errno_dict[errno])
            return 1
        else:
            return 0
        # else:
        #     return 0
    else:
        if value in range(0xbfffffff,0x100000000):
            return 1
        elif value > 0x0:
            return 0
        else:
            return 2

def hex_to_int(num):
    width = 32
    if num > 2 ** (width - 1) - 1:
        num = 2 ** width - num
        num = 0 - num
    return num

def pid_from_stackid(stackid_str):
    stackid = eval(stackid_str)
    return int(stackid[0])

def tid_from_stackid(stackid_str):
    stackid = eval(stackid_str)
    return int(stackid[0])
