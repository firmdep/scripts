#!/usr/bin/env python3
import os, sys, getopt
import pymysql
import pexpect

def get_args(id):
    db = pymysql.connect(host='192.168.3.48', user='root', password='firmware', database='firmdep')
    cursor = db.cursor()
    sql = "select iid,path,args from webservers where id = %d" %id
    cursor.execute(sql)
    result = cursor.fetchall()
    return result[0][0], result[0][1], result[0][2]

def get_iid(id):
    iid, path, args = get_args(id)
    print("[+] image id:%d" %iid)
    return iid

def main(iid):
    print("[+] starting VM")
    args = "panda-system-arm -M versatilepb -cpu cortex-a15 -m 256M -hda /share/images/%d/image.qcow2 -nographic -kernel /share/zImage_v7_ipv6 -dtb /share/versatile-pb_v7.dtb -initrd /share/rootfs.cpio -append \"panic=1 nokaslr quiet console=ttyAMA0\" -os linux-32-debian -serial mon:stdio -no-reboot -loadvm ok -net nic -net user,net=192.168.1.0/24,dhcpstart=192.168.1.1,hostfwd=tcp::80-:80,hostfwd=tcp::443-:443,hostfwd=tcp::8888-:8888" %iid
    print(args)
    vm = pexpect.spawn(args)
    vm.logfile = sys.stdout.buffer
    err = vm.expect(['does not have the requested snapshot', ],timeout=None)
    if err == 0:
    	print("[-] the snapshot was not loaded.")
    	vm.kill(0)
    	
if __name__ == "__main__":
    id = int(sys.argv[1])
    iid = get_iid(id)
    main(iid)
